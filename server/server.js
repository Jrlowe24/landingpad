const Express = require('express');
const path = require('path');
const logger = require('morgan');
// const bodyParser = require('body-parser');
require('./util/mongodb');

const app = new Express();
const firebase = require('firebase');
const propertyRouter = require('./api/property');
const ownerRouter = require('./api/owner');
const bookingRouter = require('./api/booking');
const sRuleRouter = require('./api/seasonalRules');
const chargeRouter = require('./api/charges');
const couponRouter = require('./api/coupon');
const stripeRouter = require('./api/stripe');
const authRouter = require('./api/auth');
const imageRouter = require('./api/images');
const icalRouter = require('./api/ical');


// Set Development modes checks
const isDevMode = process.env.NODE_ENV === 'development' || false;
let port = 8180;
if (isDevMode) {
  port = 3000;
}


const firebaseConfig = {
  apiKey: 'AIzaSyBT0B75DXa0yuaS0xCH2Q8Atge6heOvblU',
  authDomain: 'appily-50cdf.firebaseapp.com',
  databaseURL: 'https://appily-50cdf.firebaseio.com',
  projectId: 'appily-50cdf',
  storageBucket: 'appily-50cdf.appspot.com',
  messagingSenderId: '830817027349',
  appId: '1:830817027349:web:8189a1b6f5589d3e'
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
if (firebaseApp != null) {
  console.log('firebase app started successfully');
} else {
  console.log('firebase app failed to start');
}

app.use(logger('dev'));
app.use(Express.json());
app.use(propertyRouter);
app.use(ownerRouter);
app.use(bookingRouter);
app.use(stripeRouter);
app.use(authRouter);
app.use(imageRouter);
app.use(icalRouter);
app.use(sRuleRouter);
app.use(chargeRouter);
app.use(couponRouter);

app.use(Express.static(path.resolve(__dirname, '../public')));
// app.use(Express.static(path.resolve(__dirname, '../public')));
// start app
app.listen(port, () => {
  console.log(`Server started on port: ${port}`);
});

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/app.html'));
});
