const mongoose = require('mongoose');

const Charges = new mongoose.Schema({
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Owner'
  },
  property: [{
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: 'Property'
  }],
  name: {
    type: String,
    required: false,
    trim: true
  },
  chargeType: {
    type: String,
    required: false
  },
  chargePer: {
    type: String,
    required: false
  },
  charge: {
    type: Number,
    required: false
  },
  taxable: {
    type: Boolean,
    required: false
  },
  selectedProperties: {
    type: [Object],
    required: false
  }
});

const charges = mongoose.model('Charges', Charges);

module.exports = charges;
