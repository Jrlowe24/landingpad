const mongoose = require('mongoose');

const Booking = mongoose.model('Booking', {
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Owner'
  },
  property: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Property'
  },
  bookerInfo: {
    name: String,
    email: String,
    phoneNumber: String,
    message: String
  },
  dates: {
    starting: Date,
    ending: Date
  },
  source: {
    type: String
  },
  confirmed: {
    type: String
  },
  numOfGuests: {
    type: Number
  },
  price: {
    type: Number
  },
  type: {
    type: String
  },
  selectedCharges: {
    type: [Object]
  },
  chargesInfoMap: {
    type: Map
  }

});


module.exports = Booking;
