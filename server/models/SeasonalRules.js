const mongoose = require('mongoose');

const SeasonalRules = new mongoose.Schema({
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Owner'
  },
  property: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: 'Property'
  },
  name: {
    type: String,
    required: false,
    trim: true
  },
  startDate: {
    type: Date,
    required: false
  },
  endDate: {
    type: Date,
    required: false
  },
  minNights: {
    type: Number,
    required: false
  },
  maxNights: {
    type: Number,
    required: false
  },
  propertyInfoMap: Map,
  selectedProperties: {
    type: [Object],
    required: false
  }
});

const seasonalRules = mongoose.model('SeasonalRules', SeasonalRules);

module.exports = seasonalRules;
