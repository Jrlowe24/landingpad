const mongoose = require('mongoose');

const Coupon = new mongoose.Schema({
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Owner'
  },
  property: [{
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: 'Property'
  }],
  name: {
    type: String,
    required: false,
    trim: true
  },
  type: {
    type: String,
    required: false
  },
  validFrom: {
    type: Date,
    required: false
  },
  validTo: {
    type: Date,
    required: false
  },
  paymentDiscount: {
    type: Number,
    required: false
  },
  couponLimit: {
    type: Number,
    required: false
  },
  minNightStay: {
    type: Number,
    required: false
  },
  code: {
    type: String,
    required: false
  },
  timesRedeemed: {
    type: Number,
    required: false
  },
  selectedProperties: {
    type: [Object],
    required: false
  }
});

const coupon = mongoose.model('Coupon', Coupon);

module.exports = coupon;
