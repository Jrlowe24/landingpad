const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const Property = require('./property');

const ownerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('Email is invalid');
      }
    }
  },
  password: {
    type: String,
    required: true,
    minlength: 7,
    trim: true,
    validate(value) {
      if (value.toLowerCase().includes('password')) {
        throw new Error('Password cannot contain "password"');
      }
    }
  },
  about: {
    type: String,
    required: true
  },
  websiteId: {
    type: String,
    required: true
  },
  calenders: {
    type: [String],
    required: false
  },
  logo: String,
  titleOfProperties: String,
  showCaseImages: [String],
  aboutYourHome: String,
  aboutYourHosts: String,
  stripeToken: {
    type: String,
    required: false
  },
  businessInfo: {
    type: [Object],
    required: false
  },
  emailInfo: {
    type: [Object],
    required: false
  },
  policyInfo: {
    type: [Object],
    required: false
  }
});

ownerSchema.virtual('properties', {
  ref: 'Property',
  localField: '_id',
  foreignField: 'owner'
});

ownerSchema.virtual('bookings', {
  ref: 'Booking',
  localField: '_id',
  foreignField: 'owner'
});

ownerSchema.virtual('seasonalRules', {
  ref: 'SeasonalRules',
  localField: '_id',
  foreignField: 'owner'
});

ownerSchema.virtual('charges', {
  ref: 'Charges',
  localField: '_id',
  foreignField: 'owner'
});

ownerSchema.virtual('coupons', {
  ref: 'Coupon',
  localField: '_id',
  foreignField: 'owner'
});

ownerSchema.statics.findByCredentials = async (email, password) => {
  const owner = await Owner.findOne({ email }); // eslint-disable-line
  if (!owner) {
    throw new Error('Unable to login');
  }
  const isMatch = await bcrypt.compare(password, owner.password);
  if (!isMatch) {
    throw new Error('Unable to login');
  }
  return owner;
};

// Hash the plain text password before saving
ownerSchema.pre('save', async function (next) {
  const user = this;

  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }

  next();
});

// Delete owner properties when user is removed
ownerSchema.pre('remove', async function (next) {
  const owner = this;
  await Property.deleteMany({ owner: owner._id });
  next();
});

const Owner = mongoose.model('Owner', ownerSchema);

module.exports = Owner;
