const mongoose = require('mongoose');
const Booking = require('./booking');

const propertySchema = new mongoose.Schema({
  websiteID: { // ID associating this property to a persons booking website
    type: String,
    required: false
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Owner'
  },
  images: {
    type: [String], // primary image should be [0]
    required: false
  },
  numGuests: {
    type: Number,
    required: false
  },
  numBedrooms: {
    type: Number,
    required: false
  },
  numBathrooms: {
    type: Number,
    required: false
  },
  name: {
    type: String,
    default: 'No name',
    required: false
  },
  location: {
    lat: Number,
    lng: Number
  },
  bookedDays: {
    type: [String],
    required: false
  },
  times: {
    checkIn: String,
    checkOut: String
  },
  price: {
    rateType: String,
    weekdayPrice: Number,
    weekendPrice: Number,
    weeklyPrice: Number,
    monthlyPrice: Number,
    forGuestsAbove: Number,
    chargeGuestsAbove: Number,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  amenities: Map,
  minNights: {
    type: Number,
    required: false
  },
  calenders: [String],
  address: {
    street: String,
    city: String,
    state: String,
    zipcode: String,
    country: String,
    formattedAddress: String
  },
  externalBookings: [Object],
  landingPadBookings: [Object],
  homeType: {
    type: String,
    required: false
  },
  rentalOption: {
    type: String,
    required: false
  },
  bedroomInfo: [{
    title: String,
    numOfKings: Number,
    numOfQueens: Number,
    numOfTwins: Number,
    numOfFulls: Number,
    numOfCouches: Number
  }],
  neighborhoodInfo: {
    type: String,
    required: false
  },
  gettingAround: {
    type: String,
    required: false
  },
  parking: {
    type: String,
    required: false
  },
  accesibility: {
    type: String,
    required: false
  },
  thingsToKnow: {
    type: String,
    required: false
  },
  houseRules: {
    type: String,
    required: false
  },
  icalLink: {
    type: String,
    required: false
  },
  seasonalRule: {
    type: [Object],
    required: false
  }
});

propertySchema.virtual('bookings', {
  ref: 'Booking',
  localField: '_id',
  foreignField: 'property'
});

propertySchema.virtual('seasonalRules', {
  ref: 'SeasonalRules',
  localField: '_id',
  foreignField: 'property'
});

propertySchema.virtual('charges', {
  ref: 'Charges',
  localField: '_id',
  foreignField: 'property'
});

propertySchema.virtual('coupons', {
  ref: 'Coupon',
  localField: '_id',
  foreignField: 'property'
});

// Delete user tasks when user is removed
propertySchema.pre('remove', async function (next) {
  const property = this;
  await Booking.deleteMany({ owner: property._id });
  next();
});

const Property = mongoose.model('Property', propertySchema);

module.exports = Property;
