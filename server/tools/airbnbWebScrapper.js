const currHost = {
  name: '',
  personCapacity: 0,
  starRating: 0,
  description: '',
  space: '',
  location: {
    lat: 0,
    lng: 0,
  },
  hostName: '',
  aboutHost: '',
  propertyLocation: '',
  bedLabel: '',
  bathLabel: '',
  guestLabel: '',
  numBedrooms: 0,
  numGuests: 0,
  numBathrooms: 0,
  images: [],
  amenities: new Map(),
  reviews: [],
  owner: '',
  minNights: 0,
  neighborhoodInfo: '',
  houseRules: '',
  gettingAround: '',
  times: {
    checkIn: '',
    checkOut: ''
  }
  
};
const webScrapper = async (url, id) => {
  await fetch(url)
    .then(res => res.text())
    .then(body => {
      const numOfStart = body.search('behavioralUid');
      let newString = body.substring(numOfStart - 2, body.length);
      const numOfEnd = newString.search('</script>');
      newString = newString.substring(0, numOfEnd - 3);
      const jsonString = JSON.parse(newString);
      const str = jsonString.bootstrapData.reduxData.homePDP.listingInfo.listing;
      console.log(str);
      currHost.name = str.name || 'It appears that your Airbnb title is blank. This is where you can add any pertinent information.';
      currHost.personCapacity = str.person_capacity || 0;
      currHost.starRating = str.star_rating || 0;
      currHost.description = str.sectioned_description.description || 'It appears that your Airbnb property description is blank. This is where you can add any pertinent information.';
      currHost.space = str.sectioned_description.space || 'It appears that your Airbnb space description is blank. This is where you can add any pertinent information.';
      currHost.neighborhoodInfo = str.sectioned_description.neighborhood_overview || '';
      currHost.houseRules = str.sectioned_description.house_rules || '';
      currHost.gettingAround = str.sectioned_description.transit || '';
      currHost.location.lat = str.lat || 0;
      currHost.location.lng = str.lng || 0;
      currHost.hostName = str.primary_host.host_name || '';
      currHost.minNights = str.min_nights || 0;
      currHost.aboutHost = str.primary_host.about || 'It appears that your Airbnb host information is blank. This is where you can add any pertinent information.';
      currHost.propertyLocation = str.p3_summary_address || '';
      currHost.bathLabel = str.bathroom_label || 'X Bathrooms';
      currHost.bedLabel = str.bedroom_label || 'X Bedrooms';
      currHost.numBathrooms = parseInt(str.bathroom_label.charAt(0), 10) || 0;
      currHost.numBedrooms = parseInt(str.bedroom_label.charAt(0), 10) || 0;
      currHost.numGuests = parseInt(str.guest_label.charAt(0), 10) || 0;
      currHost.guestLabel = str.guest_label || 'X People';
      currHost.owner = id;
  
      const amenities = str.listing_amenities;
      for (let j = 0; j < amenities.length; j += 1) {
        currHost.amenities.set(amenities[j].name, true);
      }
  
      const reviews = str.sorted_reviews;
  
      for (let j = 0; j < reviews.length; j += 1) {
        const currReview = {
          comments: reviews[j].comments,
          name: reviews[j].reviewer.first_name,
          profilePic: reviews[j].reviewer.picture_url,
          date: reviews[j].localized_date,
          rating: reviews[j].rating
        };
        currHost.reviews.push(currReview);
      }
    
      // eslint-disable-next-line prefer-destructuring
      const photos = str.photos;
      for (let i = 0; i < photos.length; i += 1) {
        currHost.images.push(photos[i].large);
      }
      //   console.log(currHost);
    }).catch(error => {
      console.log(error);
    });


  return currHost;
};

module.exports = webScrapper;
