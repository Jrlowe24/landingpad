// const jwt = require('jsonwebtoken');

const Owner = require('../models/owner');

const auth = async (req, res, next) => {
  console.log(`looking for firebase ID: ${req.params.id}`);
  try {
    await Owner.findOne({ firebaseID: req.params.id }, (err, obj) => { // eslint-disable-line
      if (err) {
        console.log(err);
      } else {
        req.owner = obj;
      }
    });

    next();
  } catch (e) {
    res.status(401).send({ error: 'Please authenticate.' });
  }
};

module.exports = auth;
