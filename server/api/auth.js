const firebase = require('firebase');
const express = require('express');
const bcrypt = require('bcryptjs');
const Owner = require('../models/owner');
const db = require('../util/mongodb');

const router = new express.Router();

router.post('/api/auth/login', async (req, res) => {
  console.log('attempting log in');

  await firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
    .then(() => {
      console.log('singned in');
      return res.json({
        success: true
      });
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
});


module.exports.email_signUp = (req, res) => {
  console.log('attempting sign up');
  const crypt = bcrypt.hashSync(req.body.password, 10);
  firebase.auth().createUserWithEmailAndPassword(req.body.email, req.body.password)
    .then(_user => {
      console.log('fb id: ', _user.user.uid); // => f9b327e70bbcf42494ccb28b2d98e00e
      const owner = new Owner({
        authId: _user.user.uid,
        email: req.body.email,
        password: crypt,
      });
      owner.save(err => {
        if (err) {
          firebase.auth().currentUser.delete();
          return res.json({
            success: false,
            msg: err
          });
        }
        return res.json({
          success: true,
          msg: `ADDED user ${req.body}`
        });
      });
    })
    .catch(err => {
      console.error(err);
      return res.json({
        success: false,
        msg: err
      });
    });
};

module.exports.delete_account = (req, res) => { // eslint-disable-line
  console.log(`deleting account ${req.body.authId}`);
  const user = firebase.auth().currentUser;
  if (user == null) {
    return res.json({
      success: false,
      msg: 'user is not logged in'
    });
  }
  if (req.body.authId !== user.uid) {
    // check to make sure the requested account is correct
    return res.json({
      success: false,
      msg: 'requested account does not match current account'
    });
  }
  user.delete().then(() => {
    // remove user from our database
    db.collection('users').deleteOne({ authId: user.uid });
    return res.json({
      success: true,
      msg: `Deleted User: ${user.uid}`
    });
  }).catch(err => {
    return res.json({
      success: false,
      msg: err,
    });
  });
};

module.exports.current_user = (req, res) => {
  console.log('attempting log in');
  const currUser = firebase.auth().currentUser;
  if (currUser == null) {
    return res.json({
      success: false,
      msg: 'user not logged in'
    });
  }
  return res.json({
    success: true,
    uid: currUser.uid
  });
};
module.exports = router;
