const express = require('express');
const aws = require('aws-sdk');
const multerS3 = require('multer-s3');
const multer = require('multer');
const path = require('path');


const router = new express.Router();

/**
 * PROFILE IMAGE STORING STARTS
 */
const s3 = new aws.S3({
  accessKeyId: 'AKIAVIXAS47POKW6O74K',
  secretAccessKey: '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',
  Bucket: 'imagebucketfirst'
});

/**
 * Check File Type
 * @param file
 * @param cb
 * @return {*}
 */
function checkFileType(file, cb) {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);
  if (mimetype && extname) {
    return cb(null, true);
  }
  return cb('Error: Images Only!');
}

// Delete image
router.post('/deleteImage/:id/:fileName', (req, res) => {
  const _id = req.params.id;
  console.log(_id);
  const _fileName = req.params.fileName;
  console.log(_fileName);
  const params = {
    Bucket: 'imagebucketfirst',
    Delete: {
      Objects: [ // required
        {
          Key: _fileName // required
        }
      ],
    },
  };
  s3.deleteObjects(params, (err, data) => {
    if (err) console.log(err, err.stack); // an error occurred
    else res.json(data); // successful response
  });
});

/**
 * BUSINESS GALLERY IMAGES
 * MULTIPLE FILE UPLOADS
 */
// Multiple File Uploads ( max 4 )
const uploadsBusinessGallery = multer({
  storage: multerS3({
    s3,
    bucket: 'imagebucketfirst',
    acl: 'public-read',
    key(req, file, cb) {
      cb(null, `${path.basename(file.originalname, path.extname(file.originalname))}-${Date.now()}${path.extname(file.originalname)}`);
    }
  }),
  limits: { fileSize: 2000000 }, // In bytes: 2000000 bytes = 2 MB
  fileFilter(req, file, cb) {
    checkFileType(file, cb);
  }
}).array('imageFiles', 4);
/**
    * @route POST /api/profile/business-gallery-upload/:id
    * @param id The propeprty id to add the images to
    * @desc Upload business Gallery images
    * @access public
    */
router.post('/multiple-file-upload/:id', (req, res) => {
  uploadsBusinessGallery(req, res, error => {
    console.log(req.query);
    //   console.log(res);
    console.log('files', req.files);
    if (error) {
      console.log('errors', error);
      res.json({ error });
    } else {
      // If File not found
      // eslint-disable-next-line no-lonely-if
      if (req.files === undefined) {
        console.log('Error: No File Selected!');
        res.json('Error: No File Selected');
      } else {
        // If Success
        const fileArray = req.files;
        let fileLocation;
        const galleryImgLocationArray = [];
        for (let i = 0; i < fileArray.length; i += 1) {
          fileLocation = fileArray[i].location;
          console.log('filenm', fileLocation);
          galleryImgLocationArray.push(fileLocation);
        }
        // Save the file name into database
        // res.json({
        //   filesArray: fileArray,
        //   locationArray: galleryImgLocationArray
        
        // });
        // const fileName = 'architecture-bed-bedroom-1454806-1566933267278.jpg';
        // deleteImage(fileName);
        res.json(galleryImgLocationArray);
        const _id = req.params.id;
        console.log(_id);
      }
    }
  });
});


// delete image

module.exports = router;
