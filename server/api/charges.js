const express = require('express');
const Owner = require('../models/owner');
const Charges = require('../models/charges');
const Property = require('../models/property');
// const auth = require('../middleware/auth');

const router = new express.Router();

// create rule
router.post('/charge', async (req, res) => {
  console.log('creating charge');
  console.log(req.body);

  const charge = new Charges({
    ...req.body,
  });

  try {
    await charge.save();
    res.status(201).send(charge._id);
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

// get all charges by owner
router.get('/charge/owner/:id', async (req, res) => {
  try {
    const owner = await Owner.findById(req.params.id);
    console.log(owner);
    await owner.populate('charges').execPopulate();
    res.status(200).send(owner.charges);
  } catch (e) {
    res.status(500).send();
  }
});

// get all charges by property
router.get('/charge/prop/:id', async (req, res) => {
  try {
    const property = await Property.findById(req.params.id);
    console.log(property);
    await property.populate('charges').execPopulate();
    res.send(property.charges);
  } catch (e) {
    res.status(500).send();
  }
});

// get charge by coupon Id
router.get('/charge/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const charge = await Charges.findOne({ _id });

    if (!charge) {
      return res.status(404).send();
    }
    return res.send(charge);
  } catch (e) {
    return res.status(500).send();
  }
});

// update rules
router.patch('/charge/:id', async (req, res) => { // eslint-disable-line
  const updates = Object.keys(req.body);
  try {
    const charges = await Charges.findById(req.params.id);
    console.log(charges);
    if (!charges) {
      return res.status(404).send();
    }
    updates.forEach(update => charges[update] = req.body[update]); // eslint-disable-line
    await charges.update(charges);
    res.send(charges);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete('/charge/:id', async (req, res) => {
  try {
    const charges = await Charges.findById(req.params.id).remove();
    res.send(charges);
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
