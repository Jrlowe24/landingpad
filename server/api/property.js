const express = require('express');
const Property = require('../models/property');
const Owner = require('../models/owner');
const auth = require('../middleware/auth');
const webScrapper = require('../tools/airbnbWebScrapper');

const router = new express.Router();

// create property with owner id
router.post('/property/:id', async (req, res) => {
  console.log('creating property');
  const _id = req.params.id;
  console.log(req.body);
  const property = new Property({
    ...{ owner: _id },
  });

  try {
    await property.save();
    res.status(201).send(property._id);
  } catch (e) {
    res.status(400).send(e);
  }
});

// Create propperty with owner id and airbnbUrl
router.post('/property/:id/:url', async (req, res) => {
  console.log('creating property');
  const _id = req.params.id;
  console.log(_id);
  const _url = req.params.url;
  // console.log(decodeURIComponent(_url));
  const currProperty = await webScrapper(_url, _id);
  const property = new Property({
    ...currProperty,
  });
  try {
    console.log('here');
    await property.save();
    res.status(201).send(property._id);
  } catch (e) {
    res.status(400).send(e);
  }
});

// get property
router.get('/property/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const property = await Property.findOne({ _id });

    if (!property) {
      return res.status(404).send();
    }
    return res.send(property);
  } catch (e) {
    return res.status(500).send();
  }
});

// get all properties (based on owner)
router.get('/properties/:id', auth, async (req, res) => {
  const _id = req.params.id;
  console.log(`auth owner ${req.owner}`);
  try {
    const owner = await Owner.findOne({ _id });
    console.log(`owner ${owner}`);
    await owner.populate('properties').execPopulate();
    // console.log(owner.properties);
    res.send(owner.properties);
  } catch (e) {
    res.status(500).send();
  }
});

router.patch('/property/:id', async (req, res) => { // eslint-disable-line
  const updates = Object.keys(req.body);
  // const allowedUpdates = ['allowed fields go here'];
  // const isValidOperation = updates.every(update => allowedUpdates.includes(update));

  // if (!isValidOperation) {
  //   return res.status(400).send({ error: 'Invalid updates!' });
  // }

  try {
    const property = await Property.findById(req.params.id);
    console.log(property);
    if (!property) {
      return res.status(404).send();
    }

    updates.forEach(update => property[update] = req.body[update]); // eslint-disable-line
    await property.update(property);
    res.send(property);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete('/property/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const property = await Property.findById(_id).remove();
    res.send(property);
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
