const ical = require('ical-generator');
const express = require('express');
const Property = require('../models/property');


const router = new express.Router();

const icalCreater = (bookings, id) => {
  const cal = ical();
  const landingPadEvents = [];
  bookings.forEach(element => {
    landingPadEvents.push({
      start: element.dates.starting,
      end: element.dates.ending,
      summary: element.source,
      description: element.bookerInfo.name,
      status: element.confirmed ? 'confirmed' : 'requested'
    });
    cal.createEvent({
      start: element.dates.starting,
      end: element.dates.ending,
      summary: element.source,
      description: element.bookerInfo.name
    });
  });
  cal.prodId({
    company: 'LandingPad',
    product: 'Ical',
    language: 'EN' // optional, defaults to EN
  });
  cal.name('testName');
  cal.domain('localhost:3000');
  cal.url(`https://localhost:3000/ical/${id}calendar.ical`);
  return cal;
};


router.get('/calendar/ical/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const property = await Property.findOne({ _id });
    await property.populate('bookings').execPopulate();
    // eslint-disable-next-line prefer-destructuring
    const bookings = property.bookings;
    const cal = icalCreater(bookings, property._id);
    console.log(cal);
    cal.serve(res, [`landingPad${_id}.ics`]);
    // if this doesn't work we need to host the file on aws and update it everytime
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
