const express = require('express');
const ical = require('ical');
// const mailjet = require('node-mailjet').connect('97463b518a25b12469af16d83b4b26e4', '62fc4f1f83621360e91a1fb63a264a80');
const Booking = require('../models/booking');
const Owner = require('../models/owner');
const Property = require('../models/property');
// const auth = require('../middleware/auth');
const router = new express.Router();

// const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const icalCall = url => new Promise(resolve => {
  ical.fromURL(url, {}, (err, data) => {
    const res = [];
  for (let k in data) { // eslint-disable-line
  if (data.hasOwnProperty(k)) { // eslint-disable-line
        const ev = data[k];
        if (data[k].type === 'VEVENT') {
          const info = {
            source: ev.summary,
            startDate: ev.start,
            endDate: ev.end
          };
          res.push(info);
        }
      }
    }
    // console.log('info undefined');
    resolve(res);
  });
});

const externalBookings = async calenders => {
  // eslint-disable-next-line no-return-await
  return Promise.all(calenders.map(async url => await icalCall(url)));
};

// email set up


// update booking
router.patch('/booking/:id', async (req, res) => { // eslint-disable-line
  const updates = Object.keys(req.body);

  try {
    const booking = await Booking.findById(req.params.id);
    console.log(booking);
    if (!booking) {
      return res.status(404).send();
    }

    updates.forEach(update => booking[update] = req.body[update]); // eslint-disable-line
    await booking.update(booking);
    // const request = mailjet
    //   .post('send', { version: 'v3.1' })
    //   .request({
    //     Messages: [
    //       {
    //         From: {
    //           Email: 'hello@trylandingpad.com',
    //           Name: 'Hemanth'
    //         },
    //         To: [
    //           {
    //             Email: 'hemanth1bellala@gmail.com',
    //             Name: 'Hemanth'
    //           }
    //         ],
    //         TemplateID: 999660,
    //         TemplateLanguage: true,
    //         Subject: 'Your Reservation has been declined',
    //         Variables: { name: 'Jose' }
    //       }
    //     ]
    //   });
    // request.then(result => {
    //   console.log(result.body);
    //   }).catch(err => {
    //     console.log(err.statusCode);
    //   });
    res.send(booking);
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

// create booking
router.post('/booking', async (req, res) => {
  console.log('creating booking');
  console.log(req.body);
  const booking = new Booking({
    ...req.body,
  });

  try {
    await booking.save();
    res.status(201).send(booking._id);
  } catch (e) {
    res.status(400).send(e);
  }
});

// find booking based on booking ID
router.get('/booking/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const booking = await Booking.findOne({ _id });

    if (!booking) {
      return res.status(404).send();
    }
    return res.send(booking);
  } catch (e) {
    return res.status(500).send();
  }
});

// get only external bookings for property
router.get('/externalBookings/property/:id', async (req, res) => { // eslint-disable-line
  const _id = req.params.id;
  try {
    const property = await Property.findOne({ _id });
    // console.log(allBookings);
    res.send(property.externalBookings);
  } catch (e) {
    return res.status(500).send();
  }
});

// all landingPad bookings for a property
router.get('/bookings/property/:id', async (req, res) => { // eslint-disable-line
  const _id = req.params.id;
  try {
    const property = await Property.findOne({ _id });
    // console.log(property);
    await property.populate('bookings').execPopulate();
    console.log(property.bookings);
    res.send(property.bookings);
  } catch (e) {
    return res.status(500).send();
  }
});

// all bookings for an owner
router.get('/bookings/owner/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const owner = await Owner.findOne({ _id });
    console.log(`owner ${owner}`);
    await owner.populate('bookings').execPopulate();
    res.send(owner.bookings);
  } catch (e) {
    res.status(500).send();
  }
});

// get external bookings from the calendars
router.get('/booking/external/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const property = await Property.findOne({ _id });
    const externalBooking = await externalBookings(property.calenders);
    property.externalBookings = externalBooking;
    await property.update(property);
    res.send(externalBooking);
  } catch (e) {
    res.status(500).send();
  }
});

router.delete('/booking/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const booking = await Booking.findById(_id).remove();
    res.send(booking);
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
