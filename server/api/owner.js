const express = require('express');
const firebase = require('firebase');
const Owner = require('../models/owner');
const auth = require('../middleware/auth');

const router = new express.Router();

// create owner
router.post('/owner', async (req, res) => {
  console.log('creating owner');
  console.log(req.body);

  const owner = await new Owner({
    ...req.body,
  });

  try {
    await owner.save();
  } catch (e) {
    res.status(400).send(e);
  }
  await firebase.auth().createUserWithEmailAndPassword(req.body.email, req.body.password)
    .then(user => {
      try {
        Owner.updateOne({ _id: owner._id }, {
          firebaseID: user.user.uid
        }, (err, affected) => {
          console.log(affected);
          res.status(201).send(owner);
        });
      } catch (e) {
        console.log(`Could not update owner: ${e}`);
      }
    });
});

router.get('/owner/:id', auth, async (req, res) => {
  console.log(`auth owner ${req.owner}`);
  try {
    res.send(req.owner);
  } catch (e) {
    res.status(500).send();
  }
});

updateOwner = async (req) => { // eslint-disable-line
  const updates = Object.keys(req.body);
  const allowedUpdates = [
    'name',
    'email',
    'password',
    'about',
    'logo',
    'titleOfProperties',
    'showCaseImages',
    'aboutYourHome',
    'aboutYourHosts',
    'calenders',
    'stripeToken'
  ];
  const isValidOperation = await updates.every(update => allowedUpdates.includes(update));

  if (!isValidOperation) {
    console.log('invalid update');
  }

  try {
    const owner = await Owner.findById(req.params.id);
      updates.forEach(update => owner[update] = req.body[update]); // eslint-disable-line
    await owner.save();
    return owner;
  } catch (e) {
    console.log(`Could not update owner: ${e}`);
  }
};


router.patch('/owner/:id', async (req, res) => { // eslint-disable-line

  const owner = await updateOwner(req);
  if (owner) {
    res.send(owner);
  } else {
    res.status(400);
  }
});

router.delete('/owner/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const owner = await Owner.findById(_id).remove();
    res.send(owner);
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
