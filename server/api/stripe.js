const cors = require('cors');
const express = require('express');
const stripe = require('stripe')('sk_test_6RTuREW92A1JwZExFTXpStZg00dblzy8Zy');
const request = require('request-promise-native');
const firebase = require('firebase');
const auth = require('../middleware/auth');
const Owner = require('../models/owner');
const OwnerAPI = require('../api/owner');

stripe.setApiVersion('2019-08-14');
// const uuid = require('uuid/v4');

const router = new express.Router();

router.use(cors());
router.use(express.json());

router.post('/stripe/create', async (req, res) => {
  await stripe.accounts.create({
    type: 'custom',
    country: 'US',
    email: 'bob@example.com',
    requested_capabilities: ['card_payments', 'transfers']
  }, (err, account) => {
    res.send(account);
  });
});

router.get('/payments', async (req, res) => {
  // res.send("Add your Stripe Secret Key to the .require('stripe') statement!");
  const stripeTok = req.query.code;
  console.log(`stripe token: ${req.query.code}`);

  const options = {
    method: 'POST',
    uri: 'https://connect.stripe.com/oauth/token',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: {
      client_secret: 'sk_test_6RTuREW92A1JwZExFTXpStZg00dblzy8Zy',
      code: req.query.code,
      grant_type: 'authorization_code'
    },
    json: true
  };
  await request(options) // registers the owners stripe account
    .then(json => {
    })
    .catch(err => {
      console.log(err);
    });

  console.log(`confirming stripe account for user: ${req.query.state}`);

  try {
    await Owner.updateOne({ _id: req.query.state }, {
      stripeToken: stripeTok
    }, (err, affected, resp) => {
      console.log(affected);
    });
  } catch (e) {
    console.log(`Could not update owner: ${e}`);
  }

  res.send('OK');
});


router.get('/property/checkout', (req, res) => {
  res.send("Add your Stripe Secret Key to the .require('stripe') statement!");
  console.log('testshit');
});

router.post('/property/:id/checkout', async (req, res) => {
  console.log('Request:', req.body);

  let error;
  let status;
  try {
    const { product, token } = req.body;

    const customer = await stripe.customers.create({
      email: token.email,
      source: token.id
    });

    // const idempotencyKey = uuid();
    const charge = await stripe.charges.create(
      {
        amount: product.price * 100,
        currency: 'usd',
        customer: customer.id,
        receipt_email: token.email,
        description: `Purchased the ${product.name}`,
        shipping: {
          name: token.card.name,
          address: {
            line1: token.card.address_line1,
            line2: token.card.address_line2,
            city: token.card.address_city,
            country: token.card.address_country,
            postal_code: token.card.address_zip
          }
        }
      }
    );
    console.log('Charge:', { charge });
    status = 'success';
  } catch (newerror) {
    console.error('Error:', newerror);
    status = 'failure';
  }

  res.json({ error, status });
});

// router.listen(8080);


module.exports = router;
