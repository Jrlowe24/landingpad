const express = require('express');
const Owner = require('../models/owner');
const SeasonalRule = require('../models/SeasonalRules');
const Property = require('../models/property');
const auth = require('../middleware/auth');

const router = new express.Router();

// create rule
router.post('/seasonalRule', async (req, res) => {
  console.log('creating seasonal rule');
  console.log(req.body);

  const rule = new SeasonalRule({
    ...req.body,
  });

  try {
    await rule.save();
    res.status(201).send(rule._id);
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

// get rule by id
router.get('/seasonalRules/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const seasonalRule = await SeasonalRule.findOne({ _id });

    if (!seasonalRule) {
      return res.status(404).send();
    }
    return res.send(seasonalRule);
  } catch (e) {
    return res.status(500).send();
  }
});

router.get('/seasonalRules/owner/:id', async (req, res) => {
  const _id = req.params.id;
  console.log(_id);
  try {
    const owner = await Owner.findOne({ _id });
    console.log(owner);
    await owner.populate('seasonalRules').execPopulate();
    res.status(200).send(owner.seasonalRules);
  } catch (e) {
    res.status(500).send();
  }
});

// get all seasonal rules by property
router.get('/seasonalRules/prop/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const property = await Property.findOne({ _id });
    console.log(property);
    await property.populate('seasonalRules').execPopulate();
    res.send(property.seasonalRules);
  } catch (e) {
    res.status(500).send();
  }
});

// update rules
router.patch('/seasonalRules/prop/:id', async (req, res) => { // eslint-disable-line
  const updates = Object.keys(req.body);
  try {
    const rule = await SeasonalRule.findById(req.params.id);
    console.log(rule);
    if (!rule) {
      return res.status(404).send();
    }
    updates.forEach(update => rule[update] = req.body[update]); // eslint-disable-line
    await rule.update(rule);
    res.send(rule);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete('/seasonalRules/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const rule = await SeasonalRule.findById(_id).remove();
    res.send(rule);
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
