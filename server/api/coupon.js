const express = require('express');
const Owner = require('../models/owner');
const Coupon = require('../models/coupon');
const Property = require('../models/property');
// const auth = require('../middleware/auth');

const router = new express.Router();

// create coupon
router.post('/coupon', async (req, res) => {
  console.log('creating coupon');
  console.log(req.body);

  const coupon = new Coupon({
    ...req.body,
  });

  try {
    await coupon.save();
    res.status(201).send(coupon._id);
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

// get all coupons by owner
router.get('/coupon/owner/:id', async (req, res) => {
  try {
    const owner = await Owner.findById(req.params.id);
    console.log(owner);
    await owner.populate('coupons').execPopulate();
    res.status(200).send(owner.coupons);
  } catch (e) {
    res.status(500).send();
  }
});

// get coupon by coupon Id
router.get('/coupon/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    const coupon = await Coupon.findOne({ _id });

    if (!coupon) {
      return res.status(404).send();
    }
    return res.send(coupon);
  } catch (e) {
    return res.status(500).send();
  }
});

// get all coupons by property
router.get('/coupon/prop/:id', async (req, res) => {
  try {
    const property = await Property.findById(req.params.id);
    console.log(property);
    await property.populate('coupons').execPopulate();
    res.send(property.coupons);
  } catch (e) {
    res.status(500).send();
  }
});

// update rules
router.patch('/coupon/:id', async (req, res) => { // eslint-disable-line
  const updates = Object.keys(req.body);
  try {
    const coupon = await Coupon.findById(req.params.id);
    console.log(coupon);
    if (!coupon) {
      return res.status(404).send();
    }
    updates.forEach(update => coupon[update] = req.body[update]); // eslint-disable-line
    await coupon.update(coupon);
    res.send(coupon);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete('/coupon/:id', async (req, res) => {
  try {
    const coupon = await Coupon.findById(req.params.id).remove();
    res.send(coupon);
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
