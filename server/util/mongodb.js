const mongoose = require('mongoose');

// Set native promises as mongoose promise
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);

const mongoDB = 'mongodb://localhost:27017/landingPad';
// const mongoDB = 'mongodb://ec2-18-221-72-194.us-east-2.compute.amazonaws.com:27017/landingPad';
mongoose.connect(mongoDB, { useNewUrlParser: true });

// Get the default connection
const db = mongoose.connection;

// Bind connection to error event (to get notification of connection errors)
db.on('error', err => {
  console.error('A MongoDB connection error has occurred.');
  console.error(err);
  console.error('Application server is exiting with non-zero exit code to ensure server is removed from cluster');
  return process.exit(1);
});

module.exports = db;
