const classifyDays = (startDate, endDate) => {
  let numOfWeekends = 0;
  let numOfWeekDays = 0;
  let totalDays = 0;
  let loop = new Date(startDate);
  while (loop <= endDate) {
    if (loop.getDay() === 6 || loop.getDay() === 0 || loop.getDay() === 5) {
      numOfWeekends += 1;
    } else {
      numOfWeekDays += 1;
    }
    totalDays += 1;
    const newDate = loop.setDate(loop.getDate() + 1);
    loop = new Date(newDate);
  }
  return { numOfWeekDays, numOfWeekends, totalDays };
};

export default function PriceHelper(price, startDate, endDate) {
  const classifiedDays = classifyDays(startDate, endDate);
  const { numOfWeekDays, numOfWeekends, totalDays } = classifiedDays;
  if (totalDays >= 30 && price.monthlyPrice) {
    return price.monthlyPrice * (totalDays / 30);
  }
  if (totalDays >= 6 && price.weeklyPrice) {
    return price.weeklyPrice * (totalDays / 6);
  }

  if (price.weekendPrice) {
    return price.weekdayPrice * numOfWeekDays + price.weekendPrice * numOfWeekends;
  }
  return price.weekdayPrice * totalDays;
}
