// import React, { Component } from 'react';
// import { Route, HashRouter as Router, Switch } from 'react-router-dom';
// import styles from './App.css';
// import Home from './containers/Home/Home';
// import Navigation from './containers/App/components/Navigation';
// import Properties from './containers/Properties/Properties';
// import PropertyDetailView from './containers/PropertyDetailView/PropertyDetailView';
//
//
// export default class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//     };
//   }
//
//
//   render() {
//     const properties = [{
//       amenities: ['wifi', 'ac', 'water', 'wifi', 'ac', 'water', 'wifi', 'ac', 'water', 'wifi', 'ac', 'water', 'wifi', 'ac', 'water'],
//       img: ['https://a0.muscache.com/4ea/air/v2/pictures/6d892ba1-d291-4716-8edb-d72c60d22e96.jpg?t=r:w1200-h720-sfit,e:fjpg-c90', 'https://a0.muscache.com/4ea/air/v2/pictures/6d892ba1-d291-4716-8edb-d72c60d22e96.jpg?t=r:w1200-h720-sfit,e:fjpg-c90'],
//       propertyName: 'Lounge in a wonderful',
//       id: 234243,
//       description: 'White walls and wooden furniture intertwine to create a natural aesthetic that feels open and inviting. Walk past the elegant kitchen with granite counter tops and through the gorgeous French doors leading to a private garden patio.',
//       price: 100,
//       personCapacity: 4,
//       beds: '1 Queen Bed',
//       checkIn: '4:00 PM - 11:00 PM',
//       checkOut: '11:00 AM',
//       minNights: 3,
//       bathrooms: 2,
//       bedrooms: 1
//     },
//     {
//       amenities: ['wifi', 'ac', 'water', 'wifi', 'ac', 'water', 'wifi', 'ac', 'water', 'wifi', 'ac', 'water', 'wifi', 'ac', 'water'],
//       img: ['https://a0.muscache.com/4ea/air/v2/pictures/6d892ba1-d291-4716-8edb-d72c60d22e96.jpg?t=r:w1200-h720-sfit,e:fjpg-c90', 'https://a0.muscache.com/4ea/air/v2/pictures/6d892ba1-d291-4716-8edb-d72c60d22e96.jpg?t=r:w1200-h720-sfit,e:fjpg-c90', 'https://a0.muscache.com/4ea/air/v2/pictures/1473e587-f24e-4267-a919-a6cac9f983ec.jpg?t=r:w1200-h720-sfit,e:fjpg-c90'],
//       propertyName: 'Stay in a shitty Place',
//       id: 124231,
//       description: 'White walls and wooden furniture intertwine to create a natural aesthetic that feels open and inviting. Walk past the elegant kitchen with granite counter tops and through the gorgeous French doors leading to a private garden patio.',
//       price: 100,
//       personCapacity: 4,
//       beds: '1 Queen Bed',
//       checkIn: '4:00 PM - 11:00 PM',
//       checkOut: '11:00 AM',
//       minNights: 3,
//       bathrooms: 1,
//       bedrooms: 2
//     }];
//
//     const hostInfo = {
//       hostName: 'Ron',
//       titleOfProperties: 'Luxury Retreats on the Park',
//       showCaseImages: ['https://a0.muscache.com/4ea/air/v2/pictures/1473e587-f24e-4267-a919-a6cac9f983ec.jpg?t=r:w1200-h720-sfit,e:fjpg-c90', 'https://a0.muscache.com/4ea/air/v2/pictures/6d892ba1-d291-4716-8edb-d72c60d22e96.jpg?t=r:w1200-h720-sfit,e:fjpg-c90', 'https://a0.muscache.com/4ea/air/v2/pictures/6d892ba1-d291-4716-8edb-d72c60d22e96.jpg?t=r:w1200-h720-sfit,e:fjpg-c90'],
//       logo: 'https://image.flaticon.com/icons/png/512/506/506668.png',
//       locations: [{ lat: 30, long: 60 }, { lat: 72, long: 103 }],
//       aboutYourHome: 'Style, comfort, and convenience. 2 bed/1 bath apt in a renovated Victorian. Centrally located in Atlanta\'s most popular Midtown neighborhood - one block from the Park and a short walk to food, coffee, and entertainment. Friendly hosts, great reviews! Please submit inquiry before booking :)',
//       aboutYourHosts: 'We are a mother/two daughter/wife team and we take our hosting responsibilities seriously! Jane started Airbnbing in 2009 (back when people asked how to spell that and her friends thought it was "kind of weird.") Look at it now! If you\'ve never stayed in an Airbnb before, we\'d love to host you! We enjoy making first-timers feel comfortable and welcome! If you are a seasoned pro or business traveler, we are confident you will find our listings are some of best around. If you\'re in the film/tv industry rest assured we have extensive experience renting short term furnished rentals for your industry both through and outside of Airbnb. All of our linens are updated annually, all of our mattresses were purchased in the last 4 years, and we frequently change out the decor to keep it looking fresh! With four of us at the helm, you are always getting a quick response to any inquiry. We look forward to hosting you!!'
//     };
//     return (
//       <Router>
//         <div className={styles.App}>
//           <Navigation />
//           <Switch>
//             <Route
//               exact
//               path="/"
//               render={props => <Home {...props} properties={properties} hostInfo={hostInfo} />}
//             />
//             <Route
//               exact
//               path="/properties"
//               render={props => <Properties {...props} properties={properties} />}
//             />
//             <Route
//               path="/properties/:id"
//               render={props => (
//                 <PropertyDetailView
//                   {...props}
//                   property={
//                           properties.filter(
//                             property => property.id === parseInt(props.match.params.id, 10)
//                           )[0]
//                         }
//                 />
//               )}
//             />
//           </Switch>
//         </div>
//       </Router>
//     );
//   }
// }

import React, { Component } from 'react';
// import Booking from './containers/Booking/Booking';

import CircularProgress from '@material-ui/core/CircularProgress';
import OwnerDashBoard from './containers/OwnerDashBoard/OwnerDashBoard';
import Login from './containers/OwnerDashBoard/Login/Login';
import styles from './App.css';
// import OwnerDashBoard from './containers/OwnerDashBoard/OwnerDashBoard';

const firebase = require('firebase');


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      owner: null,
      loading: true
    };
  }

  componentWillMount() {
    const { owner } = this.state;
    if (owner == null) {
      // this.setState({
      //   loading: false
      // });
    }
    this.fetchOwnerInfo();
  }


  fetchOwnerInfo = async () => {
    console.log('fetching info');
    await firebase.auth().onAuthStateChanged(user => {
      if (user) {
        fetch(`/owner/${user.uid}`)
          .then(res => res.json())
          .then(data => {
            console.log(data);
            this.setState({ owner: data, loading: false });
          })
          .catch(err => {
            console.log(err);
          });
      } else {
        this.setState({ owner: null, loading: false });
      }
    });
  };


  render() { //eslint-disable-line
    const { owner, loading } = this.state;
    if (owner) {
      return (
        <div>
          <div className={styles.header} />
          <OwnerDashBoard owner={owner} className={styles} />
        </div>
      );
    }
    if (loading) {
      return (
        <div className={styles.loading}>
          <CircularProgress className={styles.loadAnim} />
        </div>
      // <Loading />
      );
    }
    if (!owner) {
      return (
        <Login />
      );
    }
  }
}
