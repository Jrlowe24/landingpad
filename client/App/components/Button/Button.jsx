import React from 'react';
import styles from './Button.css';
import deleteLogo from './deleteLogo.png';
import editLogo from './editLogo.png';
import duplicateLogo from './duplicateLogo.png';


const Button = props => {
  const {
    text, onClick, disabled, edit, del, duplicate, cancel, className, noOutline
  } = props;
  const body = [];
  if (edit) {
    body.push(<img src={editLogo} alt="edit" className="editLogo" key="edit" />);
  } else if (del) {
    body.push(<img src={deleteLogo} alt="delete" className="deleteLogo" key="delete" />);
  } else if (duplicate) {
    body.push(<img src={duplicateLogo} alt="duplicate" className="duplicateLogo" key="duplicate" />);
  } else {
    body.push(text);
  }
  
  const style = [];
  if (edit || del || duplicate) {
    style.push(styles.actionButtons);
  } else if (cancel) {
    style.push(styles.cancelButton);
  } else if (noOutline) {
    style.push(styles.noOutline);
  } else {
    style.push(styles.customButton);
  }
  return (
    <button
      type="submit"
      onClick={onClick}
      disabled={disabled}
      className={`${style} ${className}`}
    >
      {body}
    </button>
  );
};

export default Button;
