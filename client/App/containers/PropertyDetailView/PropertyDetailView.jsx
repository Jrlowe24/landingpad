import React, { Component } from 'react';
import './PropertyDetailView.less';
import PropTypes from 'prop-types';
import './image-gallery.less';
import ImageGallery from 'react-image-gallery';
import SearchContainer from './SearchContainer/SearchContainer';


class PropertyDetailView extends Component {
  static formatPrice(num) {
    return `$${num}`;
  }

  static mapAmenity(amenity) {
    const amenities = {
      ac: 'https://image.flaticon.com/icons/svg/63/63341.svg',
      wifi: 'https://image.flaticon.com/icons/svg/159/159599.svg',
      tv: 'https://image.flaticon.com/icons/svg/130/130281.svg',
      shower: 'https://image.flaticon.com/icons/svg/1401/1401492.svg',
      kitchen: 'https://image.flaticon.com/icons/svg/45/45266.svg',
      towel: 'https://image.flaticon.com/icons/svg/1948/1948498.svg',
    };
    return amenities[`${amenity}`] || 'https://image.flaticon.com/icons/svg/907/907830.svg';
  }

  constructor(props) {
    super(props);
    this.state = {};
  }


  render() {
    const { property } = this.props;
    // loading images into the gallery
    const images = [];
    for (let i = 0; i < property.img.length; i += 1) {
      images.push({
        original: property.img[i],
        thumbnail: property.img[i]
      });
    }
    const icons = [];
    for (let i = 0; i < property.amenities.length; i += 1) {
      icons.push(
        <div key={i} className="bulletImageText">
          <img
            className="bulletImages"
            alt="icon"
            src={PropertyDetailView.mapAmenity(property.amenities[i])}
          />
          <li>
            {property.amenities[i]}
          </li>
        </div>
      );
    }
    return (
      <div className="detailsPageLayout">
        <div className="propertyDetails">
          <div className="detailRow">
            <button
              type="submit"
              className="spacingHeaderButton"
            >
                Back
            </button>
            <div className="vl1">
              <h2>
                {property.propertyName}
              </h2>
            </div>
          </div>
          <ImageGallery
            items={images}
            className="imageGallery"
          />
          <div className="detailRow">
            <h3 className="headerText">
              {'Properties:'}
            </h3>
            <ul>
              <li>
                  Accommodates:
                {' '}
                {property.personCapacity}
              </li>
              <li>
                  Beds:
                {' '}
                {property.beds}
              </li>
            </ul>
          </div>
          <div className="bar" />
          <div className="detailRow">
            <h3 className="headerText">
              {'More Info:'}
            </h3>
            <ul>
              <li className="moreInfo">
                {property.description}
              </li>
            </ul>
          </div>
          <div className="bar" />
          <div className="detailRow">
            <h3 className="headerText">
              {'Amenities:'}
            </h3>
            <ul>
              {icons}
            </ul>
          </div>
          <div className="bar" />
          <div className="detailRow">
            <h3 className="headerText">
              {'Check in Check out:'}
            </h3>
            <ul>
              <li>
                  Check in:
                {' '}
                {property.checkIn}
              </li>
              <li>
                  Check out:
                {' '}
                {property.checkOut}
              </li>
            </ul>
          </div>
          <div className="bar" />
          <div className="detailRow">
            <h3 className="headerText">
              {'Terms:'}
            </h3>
            <ul>
              <li>
                Minimum Nights:
                {' '}
                {property.minNights}
              </li>
              <li>
                Read our policies
              </li>
            </ul>
          </div>
        </div>
        <div>
          <SearchContainer property={property} />
        </div>
      </div>
    );
  }
}

export default PropertyDetailView;

PropertyDetailView.Proptype = {
  property: PropTypes.object.isRequired
};
