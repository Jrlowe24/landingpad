import React, { Component } from 'react';
import { DatePicker, Button } from 'antd';
import NumericInput from 'react-numeric-input';
import 'antd/es/date-picker/style';
import moment from 'moment';
import './SearchContainer.less';
import PropTypes from 'prop-types';


const { RangePicker } = DatePicker;

class SearchContainer extends Component {
  static formatPrice(num) {
    return `$${num}`;
  }

  static range(start, end) {
    const result = [];
    for (let i = start; i < end; i += 1) {
      result.push(i);
    }
    return result;
  }

  static myFormat(num) {
    return `${num} Guests`;
  }

  static disabledDate(current) {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  constructor(props) {
    super(props);
    this.state = { };
  }


  render() {
    const { property } = this.props;
    return (
      <div className="searchContainer">
        <div className="searchContainerContent">
          <div className="vl">
            <span>
                From
              <br />
              {SearchContainer.formatPrice(property.price)}
              <br />
              Per Night
            </span>
          </div>
          <br />
          <div>
            <RangePicker
              placeholder={['Check in', 'Check out']}
              disabledDate={SearchContainer.disabledDate}
              format="DD-MM-YYYY"
              className="rangePicker"
            />
          </div>
          <div className="spacing">
            <NumericInput
              min={1}
              max={property.personCapacity}
              value="1 Guests"
              className="numOfGuests"
              format={SearchContainer.myFormat}
            />
          </div>
          <div>
            <Button className="buttonSearch"> Request Booking </Button>
          </div>
        </div>
      </div>

    );
  }
}

export default SearchContainer;

SearchContainer.Proptype = {
  property: PropTypes.object.isRequired
};
