import React, { Component } from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { formatDate, parseDate } from 'react-day-picker/moment';
import '../../Properties/SearchBar/ReactDayPicker.less';
import NumericInput from 'react-numeric-input';
import moment from 'moment';
import StripeCheckout from 'react-stripe-checkout';

let blockedDays = [];
let blockedDaysCheckOut = [];
export default class OrderSummary extends Component {
  static myFormat(num) {
    return `${num} Guests`;
  }

  constructor(props) {
    super(props);
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.getNumOfDays = this.getNumOfDays.bind(this);
    this.state = {
      from: props.checkIn,
      to: props.checkOut,
      numOfGuests: 0,
      price: 0
    };
    this.showFromMonth = this.showFromMonth.bind(this);
    this.setPrice = this.setPrice.bind(this);
    this.handleNumOfGuests = this.handleNumOfGuests.bind(this);
    this.firstBlocked = this.firstBlocked.bind(this);
    this.blockedDay = this.blockedDay.bind(this);
    this.blockedDaysForCheckOut = this.blockedDaysForCheckOut.bind(this);
  }

  componentDidMount() {
    this.setPrice();
  }


  setPrice() {
    const { from, to } = this.state;
    if (to !== '' && from !== '') {
      console.log('here');
      const { property } = this.props;
      const priceNow = this.getNumOfDays() * property.price;
      this.setState({ price: priceNow }, () => {
        const { updatePrice } = this.props;
        updatePrice(priceNow);
      });
    }
  }

  getNumOfDays() {
    const { from, to } = this.state;
    const differenceInTime = to.getTime() - from.getTime();

    // To calculate the no. of days between two dates
    const numOfDays = Math.round((differenceInTime / (1000 * 3600 * 24)));
    console.log(`numOfDays:${numOfDays}`);
    return numOfDays;
  }

  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState({ from }, () => {
      const { updateFrom } = this.props;
      updateFrom(from);
      this.handleToChange('');
      // this.setPrice();
    });
  }

  handleToChange(to) {
    this.setState({ to }, () => {
      const { updateTo } = this.props;
      updateTo(to);
      this.showFromMonth();
      this.setPrice();
    });
  }

  handleNumOfGuests(e) {
    this.setState({ numOfGuests: e }, () => {
      const { updateNumOfGuests } = this.props;
      updateNumOfGuests(e);
    });
  }

  firstBlocked() {
    const { from } = this.state;
    if (from !== '') {
      const now = from;
      let closest = Infinity;
      blockedDays.forEach(d => {
        const date = new Date(d.from);

        if (date >= now && (date < new Date(closest) || date < closest)) {
          closest = d;
        }
      });
      return closest.from;
    }
    return null;
  }

  //   https://react-day-picker.js.org/docs/matching-days/
  // use this to figure how to block dates
  blockedDay() {
    const { externalBookings } = this.props;
    const blockedDates = [];
    externalBookings.forEach(bookings => {
      bookings.forEach(booking => {
        blockedDates.push({ from: new Date(booking.startDate), to: new Date(booking.endDate) });
      });
    });
    console.log(blockedDates);
    // blockedDates.push({ from: new Date(2019, 8, 1), to: new Date(2019, 8, 19) });]
    blockedDates.push({ before: moment().toDate() });
    blockedDays = blockedDates;
    blockedDaysCheckOut = [...blockedDates];
    return blockedDates;
  }

  blockedDaysForCheckOut() {
    const blockedDates = blockedDaysCheckOut;
    const { from } = this.state;
    blockedDates.push({ before: from });
    blockedDates.push({ after: this.firstBlocked() });
    return blockedDates;
  }

  render() {
    const {
      from, to, numOfGuests, price
    } = this.state;
    const modifiers = { start: from, end: to };
    const blockDays = this.blockedDay();
    const { property, handleToken } = this.props;
    return (
      <div>
        <h1>Order Summary</h1>
        <img
          alt="Main pic for Property"
          src={property === null ? 'nothin' : property.images[0]}
          className="propertiesImage"
        />
        <p>
          {property === null ? 'Loading' : property.name}
        </p>
        <div className="InputFromTo">
          <DayPickerInput
            value={from}
            placeholder="Check in"
            format="LL"
            formatDate={formatDate}
            parseDate={parseDate}
            dayPickerProps={{
              selectedDays: [from, { from, to }],
              disabledDays: blockDays,
              modifiers,
              numberOfMonths: 1,
              onDayClick: () => this.to.getInput().focus(),
            }}
            onDayChange={this.handleFromChange}
          />
          <span className="InputFromTo-to">
            <DayPickerInput
              ref={el => { this.to = el; }}
              value={to}
              placeholder="Check out"
              format="LL"
              formatDate={formatDate}
              parseDate={parseDate}
              dayPickerProps={{
                selectedDays: [from, { from, to }],
                disabledDays: this.blockedDaysForCheckOut(),
                modifiers,
                month: from,
                numberOfMonths: 1,
              }}
              onDayChange={this.handleToChange}
            />
          </span>
        </div>
        <NumericInput
          min={1}
          max={property === null ? 6 : property.numGuests}
          value={numOfGuests}
          className="propertiesNumOfGuests"
          format={this.myFormat}
          onChange={this.handleNumOfGuests}
        />
        <h3>Subtotal </h3>
        <p>{price}</p>
        <StripeCheckout
          stripeKey="pk_test_YKy8kWYuKrjA9JSkD9iy3uz8"
          token={handleToken}
          amount={price * 100}
          name={property === null ? 'Loading' : property.name}
          image={property === null ? 'nothin' : property.images[0]}
        />
      </div>
    );
  }
}
