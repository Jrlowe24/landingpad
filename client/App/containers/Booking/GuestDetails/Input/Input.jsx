import React from 'react';

const Input = props => {
  const {
    type, title, name, value, placeHolder, handleChange, pattern,
  } = props;
  return (
    <div className="form-group">
      <label htmlFor={name} className="form-label" id={name}>
        {title}
        <input
          className="form-input"
          id={name}
          name={name}
          type={type}
          value={value}
          onChange={handleChange}
          placeholder={placeHolder}
          pattern={pattern}
        />
      </label>
    </div>
  );
};

export default Input;
