import React, { Component } from 'react';
import { CountryDropdown } from 'react-country-region-selector';
import Input from './Input/Input';


export default class GuestDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newGuest: props.newGuest
    };

    this.handleFirstName = this.handleFirstName.bind(this);
    this.handleLastName = this.handleLastName.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePhoneNumber = this.handlePhoneNumber.bind(this);
    this.handleCountry = this.handleCountry.bind(this);
    this.handleSpecialRequests = this.handleSpecialRequests.bind(this);
  }


  handleFirstName(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      newGuest:
          { ...prevState.newGuest, firstName: value }
    }), () => {
      console.log('handlefirstName');
      const { updateGuest } = this.props;
      const { newGuest } = this.state;
      updateGuest(newGuest);
    });
  }

  handleLastName(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      newGuest:
          { ...prevState.newGuest, lastName: value }
    }), () => {
      const { updateGuest } = this.props;
      const { newGuest } = this.state;
      updateGuest(newGuest);
    });
  }

  handleEmail(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      newGuest:
          { ...prevState.newGuest, email: value }
    }), () => {
      const { updateGuest } = this.props;
      const { newGuest } = this.state;
      updateGuest(newGuest);
    });
  }

  handlePhoneNumber(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      newGuest:
          { ...prevState.newGuest, phoneNumber: value }
    }));
  }

  handleCountry(e) {
    this.setState(prevState => ({
      newGuest:
          { ...prevState.newGuest, country: e }
    }), () => {
      const { updateGuest } = this.props;
      const { newGuest } = this.state;
      updateGuest(newGuest);
    });
  }

  handleSpecialRequests(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      newGuest:
          { ...prevState.newGuest, specialRequests: value }
    }), () => {
      const { updateGuest } = this.props;
      const { newGuest } = this.state;
      updateGuest(newGuest);
    });
  }

  render() {
    const { newGuest } = this.state;
    return (
      <div>
        <Input
          type="text"
          title="First Name"
          name="firstName"
          value={newGuest.firstName}
          placeholder="Enter your name"
          handleChange={this.handleFirstName}
        />
        <Input
          type="text"
          title="Last Name"
          name="lastName"
          value={newGuest.lastName}
          placeholder="Enter your last name"
          handleChange={this.handleLastName}
        />
        <Input
          type="email"
          title="Email:"
          name="email"
          value={newGuest.email}
          placeholder="Enter your email"
          handleChange={this.handleEmail}
        />
        <Input
          type="tel"
          title="Phone: "
          name="phone"
          value={newGuest.phoneNumber}
          placeholder="Enter your number"
          handleChange={this.handlePhoneNumber}
          pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
        />
        <CountryDropdown
          value={newGuest.country}
          onChange={this.handleCountry}
        />
        <textarea id="specialRequests" name="specialRequests" value={newGuest.specialRequests} onChange={this.handleSpecialRequests} />
      </div>


    );
  }
}
