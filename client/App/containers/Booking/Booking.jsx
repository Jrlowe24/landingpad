import React, { Component } from 'react';
import { toast } from 'react-toastify';
import GuestDetails from './GuestDetails/GuestDetails';
import OrderSummary from './OrderSummary/OrderSummary';
import './toast.less';

const today = new Date();
const tomorrow = new Date();
tomorrow.setDate(today.getDate() + 1);
toast.configure();

export default class Booking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      from: '',
      to: '',
      numOfGuests: 0,
      price: 0,
      externalBookings: [],
      property: null,
      newGuest: {
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        country: '',
        specialRequests: ''
      }
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.updateFrom = this.updateFrom.bind(this);
    this.updateNumOfGuests = this.updateNumOfGuests.bind(this);
    this.updateTo = this.updateTo.bind(this);
    this.updatePrice = this.updatePrice.bind(this);
    this.updateNewGuest = this.updateNewGuest.bind(this);
    this.handleToken = this.handleToken.bind(this);
  }


  componentWillMount() {
    // Syncing with external and other bookings
    fetch('http://localhost:3000/bookingExternal/5d51990dd7c269140df931de').then(response => {
      response.json().then(data => {
        this.setState({ externalBookings: data });
      });
    });

    // getting property info
    fetch('http://localhost:3000/properties/5d51bb27b997a815bbd01e85').then(response => {
      response.json().then(data => {
        this.setState({ property: data });
      });
    });
  }

  // stripe api call
  async handleToken(token) {
    const {
      price, property
    } = this.state;
    await fetch('http://localhost:3000/property/5d51990dd7c269140df931de/checkout', {
      method: 'POST',
      body: JSON.stringify({
        product: {
          name: property === null ? 'Loading' : property.name,
          price: price * 100
        },
        token
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        const { status } = data;
        if (status === 'success') {
          toast('Success! Check email for details', { type: 'success' });
        } else {
          toast('Something went wrong', { type: 'error' });
        }
      });
    });
    this.handleFormSubmit();
  }

  // create a booking call
  handleFormSubmit() {
    const {
      from, to, numOfGuests, price, newGuest
    } = this.state;
    console.log(this.state);
    console.log(numOfGuests);
    console.log(price);
    const currGuest = newGuest;
    console.log(currGuest);
    // creating a booking request
    fetch('http://localhost:3000/booking', {
      method: 'POST',
      body: JSON.stringify({
        owner: '5d4c4e8d3a41cdd284013454',
        property: '5d492b196b4d93e8707d791f',
        bookerInfo: {
          name: currGuest.firstName,
          email: currGuest.email,
          phoneNumber: currGuest.phoneNumber
        },
        dates: {
          starting: from,
          ending: to
        }
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        console.log(data);
      });
    });
  }
  
  updateFrom(from) {
    this.setState({ from });
  }

  updateTo(to) {
    this.setState({ to });
  }

  updateNumOfGuests(numOfGuests) {
    this.setState({ numOfGuests });
  }

  updatePrice(price) {
    this.setState({ price });
  }

  updateNewGuest(newGuest) {
    this.setState({ newGuest });
  }

  render() {
    const {
      from, to, externalBookings, property, newGuest
    } = this.state;
    return (
      <div>
        <GuestDetails
          newGuest={newGuest}
          updateGuest={this.updateNewGuest}
        />
        <OrderSummary
          updateFrom={this.updateFrom}
          updateTo={this.updateTo}
          updateNumOfGuests={this.updateNumOfGuests}
          updatePrice={this.updatePrice}
          checkIn={from}
          checkOut={to}
          externalBookings={externalBookings}
          property={property}
          handleToken={this.handleToken}
        />
      </div>

    );
  }
}
