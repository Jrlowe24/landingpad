import React, { Component } from 'react';
import { DatePicker, Button } from 'antd/lib/index';
import NumericInput from 'react-numeric-input';
import 'antd/es/date-picker/style/index';
import moment from 'moment';
import './propertyAvailability.less';


const { RangePicker } = DatePicker;

class PropertyAvailability extends Component {
  static range(start, end) {
    const result = [];
    for (let i = start; i < end; i += 1) {
      result.push(i);
    }
    return result;
  }

  static disabledDate(current) {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    return (
      <div>
        <div className="rowC">
          <div>
            Check in Check out
            <br />
            <RangePicker
              placeholder={['DD MM YYYY', 'DD MM YYYY']}
              disabledDate={PropertyAvailability.disabledDate}
              format="DD-MM-YYYY"
              className="rangePicker"
            />
          </div>
          <div className="spacing">
            Guests
            <br />
            <NumericInput
              min={0}
              max={100}
              value={1}
              className="numOfGuests"
            />
          </div>
          <div>
            <br />
            <Button> Request Booking </Button>
          </div>
        </div>
      </div>

    );
  }
}

export default PropertyAvailability;
