import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './Listing.css';

class Listing extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

  }

  render() {
    const { data } = this.props;
    return (
      <div className={styles.main}>
        <Link to="/properties">
          <img src={data.image} className={styles.image} alt="property" />
        </Link>
        <span className={styles.info}>
          { `${data.numPeople} People | ${data.numBedrooms} Bedrooms | ${data.numBathrooms} Bathrooms`}
        </span>
        <span className={styles.name}>
          { data.name }
        </span>
      </div>
    );
  }
}

export default Listing;
