import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './ListingView.css';
import Listing from './Listing/Listing';
import sampleImage from './Listing/SampleImage.jpg';
import sampleImage2 from './Listing/sampleImage2.jpg';
import sampleImage3 from './Listing/sampleImage3.jpg';
import sampleImage4 from './Listing/sampleImage4.jpg';
import sampleImage5 from './Listing/sampleImage5.jpg';


const properties = [ // just for testing
  {
    image: sampleImage,
    numPeople: 5,
    numBedrooms: 2,
    numBathrooms: 1,
    name: 'Luxury 2 bdrm retreat on the Park'
  },
  {
    image: sampleImage2,
    numPeople: 7,
    numBedrooms: 4,
    numBathrooms: 2,
    name: 'Penthouse suite in the City'
  },
  {
    image: sampleImage3,
    numPeople: 6,
    numBedrooms: 3,
    numBathrooms: 2,
    name: 'Nice ass apartment'
  },
  {
    image: sampleImage4,
    numPeople: 2,
    numBedrooms: 1,
    numBathrooms: 1,
    name: 'Luxury Apartment'
  },
  {
    image: sampleImage5,
    numPeople: 3,
    numBedrooms: 1,
    numBathrooms: 2,
    name: 'Midtown Apartment'
  }

];

class ListingView extends Component {
  isMounted = false;

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const listings = [];
    const createDivs = i => {
      if (properties[i]) {
        return (
          <Listing key={i} data={properties[i]} />
        );
      }
      return (
        <p key={i}>
          Loading.
        </p>
      );
    };
    for (let i = 0; i < properties.length; i += 1) {
      listings.push(createDivs(i));
    }
    return (
      <div className={styles.main}>
        {listings}
      </div>
    );
  }
}

export default ListingView;

ListingView.Proptype = {
  properties: PropTypes.object.isRequired
};
