import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './AboutYourHomes.less';


class AboutYourHomes extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { aboutYourHome } = this.props;
    return (
      <div className="aboutYourHomeContainer">
        <header className="headerStyle">
          <h1>
            {'About your homes'}
          </h1>
        </header>
        <p className="textStyle">
          {aboutYourHome}
        </p>
      </div>
    );
  }
}

export default AboutYourHomes;

AboutYourHomes.Proptype = {
  aboutYourHome: PropTypes.string.isRequired
};
