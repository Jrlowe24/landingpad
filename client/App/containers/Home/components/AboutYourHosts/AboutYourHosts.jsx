import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './AboutYourHosts.less';

class AboutYourHosts extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { aboutYourHosts } = this.props;
    return (
      <div className="container">
        <header className="headerStyle">
          <h1>
            {'About your hosts'}
          </h1>
        </header>
        <p className="textStyle">
          {aboutYourHosts}
        </p>
      </div>
    );
  }
}

export default AboutYourHosts;
AboutYourHosts.Proptype = {
  aboutYourHosts: PropTypes.string.isRequired
};
