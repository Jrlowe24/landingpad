import React, { Component } from 'react';
import PropTypes from 'prop-types';


class BodyPictureLayout extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { showCaseImages } = this.props;
    return (
      <div>
        <img
          style={{
            width: '35%',
            height: '440px',
            paddingRight: '30px',
            objectFit: 'cover'
          }}
          alt="1st display"
          src={showCaseImages[0]}
        />
        <img
          style={{
            width: '30%',
            height: '440px',
            paddingRight: '30px',
            objectFit: 'cover'
          }}
          alt="2nd display"
          src={showCaseImages[1]}
        />
        <img
          style={{
            width: '35%',
            height: '440px',
            objectFit: 'cover',
          }}

          src={showCaseImages[2]}
          alt="3rd Display"
        />
      </div>
    );
  }
}

export default BodyPictureLayout;

BodyPictureLayout.Proptype = {
  showCaseImages: PropTypes.object.isRequired
};
