import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import PropTypes from 'prop-types';
import markerImage from './marker.png';

const Marker = ({ text }) => ( // eslint-disable-line
  <div style={{ width: '35px', height: '35px' }}>
    <img src={markerImage} style={{ width: '100%', height: '100%' }} alt="marker" />
  </div>
);

const markers = [
  {
    lat: 33.776660,
    lng: -84.394301,
    text: 'Pi Kapp',
  },
  {
    lat: 33.786500,
    lng: -84.381200,
    text: 'SQ5',
  },
  {
    lat: 33.792626,
    lng: -84.396224,
    text: 'Atlantic Station',
  },
];


class MapView extends Component { // eslint-disable-line
  static defaultProps = {
    center: {
      lat: 33.776660,
      lng: -84.394301
    },
    zoom: 15
  };

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { center, zoom } = this.props;
    const mapOptions = {};
    const markersList = [];

    const createMarkers = i => {
      if (markers[i]) {
        return (
          <Marker
            lat={markers[i].lat}
            lng={markers[i].lng}
            text={markers[i].text}
            key={i}
          />
        );
      }
      return (
        <p key={i}>
            Loading.
        </p>
      );
    };
    for (let i = 0; i < markers.length; i += 1) {
      markersList.push(createMarkers(i));
    }
    return (
    // Important! Always set the container height explicitly
      <div style={{
        height: '100%',
        width: '50%',
        minWidth: '30%',
        marginLeft: '100px',
        marginTop: '0px',
        paddingTop: '50px',
        display: 'inline',
        float: 'left'
      }}
      >
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyCGXQLug5wkJNNL_Jaxj3z_5kJFQoQa8AQ' }}
          defaultCenter={center}
          defaultZoom={zoom}
          options={mapOptions}
        >
          {markersList}
        </GoogleMapReact>
      </div>
    );
  }
}

export default MapView;

MapView.Proptype = {
  Locations: PropTypes.object.isRequired
};
