import React, { Component } from 'react';
import './PropertyNameHeader.less';
import PropTypes from 'prop-types';


class PropertyNameHeader extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { titleOfProperty } = this.props;
    return (
      <div style={{ marginLeft: '150px' }}>
        <header className="headerStyle">
          <h1>
            {titleOfProperty}
          </h1>
        </header>
      </div>
    );
  }
}

export default PropertyNameHeader;

PropertyNameHeader.Proptype = {
  titleOfProperty: PropTypes.string.isRequired
};
