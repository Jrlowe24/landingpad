import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Home.css';
import PropertyAvailability from './components/PropertyAvailability/PropertyAvailabilty';
import PropertyNameHeader from './components/PropertyNameHeader/PropertyNameHeader';
import BodyPictureLayout from './components/BodyPictureLayout/BodyPictureLayout';
import AboutYourHosts from './components/AboutYourHosts/AboutYourHosts';
import AboutYourHomes from './components/AboutYourHomes/AboutYourHomes';
import ListingView from './components/Listings/ListingView';
import MapView from './components/MapView/MapView';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { properties, hostInfo } = this.props;
    return (
      <div className={styles.main}>
        <PropertyNameHeader titleOfProperty={hostInfo.titleOfProperties} />
        <BodyPictureLayout showCaseImages={hostInfo.showCaseImages} />
        <PropertyAvailability />
        <div className={styles.mapAndList}>
          <MapView locations={hostInfo.locations} />
          <ListingView properties={properties} />
        </div>
        <div>
          <AboutYourHomes aboutYourHome={hostInfo.aboutYourHome} />
          <AboutYourHosts aboutYourHosts={hostInfo.aboutYourHosts} />
        </div>
      </div>
    );
  }
}

export default Home;

Home.Proptype = {
  properties: PropTypes.object.isRequired,
  hostInfo: PropTypes.object.isRequired
};
