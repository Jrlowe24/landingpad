import React, { Component } from 'react';
import { Button } from 'antd';
import NumericInput from 'react-numeric-input';
import 'antd/es/date-picker/style';
import moment from 'moment';
import './SearchBar.less';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { formatDate, parseDate } from 'react-day-picker/moment';
import './ReactDayPicker.less';

class SearchBar extends Component {
  static range(start, end) {
    const result = [];
    for (let i = start; i < end; i += 1) {
      result.push(i);
    }
    return result;
  }

  static disabledDate(current) {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  static handleSelect(date) {
    console.log(date); // Momentjs object
  }

  constructor(props) {
    super(props);
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.state = {
      from: undefined,
      to: undefined,
      numOfGuests: 0,
    };
    this.myFormat = this.myFormat.bind(this);
  }

  //
  // handleChange(date, dateString) {
  //   this.state.checkInDate = dateString;
  //   console.log('From: ', dateString, ', to: ', dateString);
  // }


  myFormat(num) {
    this.state.numOfGuests = num;
    return `${num} Guests`;
  }
  //
  // handleSelect(date) {
  //   console.log(date);
  //   this.state.checkInDate = date;
  //   console.log(date); // Momentjs object
  // }

  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState({ from });
  }

  handleToChange(to) {
    this.setState({ to }, this.showFromMonth);
  }


  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    return (
      <div>
        <div className="propertiesSearchBar">
          <div className="InputFromTo">
            <DayPickerInput
              value={from}
              placeholder="Check in"
              format="LL"
              formatDate={formatDate}
              parseDate={parseDate}
              dayPickerProps={{
                selectedDays: [from, { from, to }],
                disabledDays: { after: to },
                toMonth: to,
                modifiers,
                numberOfMonths: 2,
                onDayClick: () => this.to.getInput().focus(),
              }}
              onDayChange={this.handleFromChange}
            />
            <span className="InputFromTo-to">
              <DayPickerInput
                ref={el => { this.to = el; }}
                value={to}
                placeholder="Check out"
                format="LL"
                formatDate={formatDate}
                parseDate={parseDate}
                dayPickerProps={{
                  selectedDays: [from, { from, to }],
                  disabledDays: { before: from },
                  modifiers,
                  month: from,
                  fromMonth: from,
                  numberOfMonths: 2,
                }}
                onDayChange={this.handleToChange}
              />
            </span>
          </div>
          <div>
            <NumericInput
              min={1}
              max={100}
              value="1 Guests"
              className="propertiesNumOfGuests"
              format={this.myFormat}
            />
          </div>
          <div>
            <Button className="propertiesButtonSearch"> Request Booking </Button>
          </div>
        </div>
      </div>

    );
  }
}

export default SearchBar;
