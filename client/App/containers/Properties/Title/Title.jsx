import React, { Component } from 'react';
import './Title.less';

class Title extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    return (
      <div className="propertiesTitleDiv">
        <header className="propertiesTitleText">
          <h2>
            {'Available Spaces'}
          </h2>
        </header>
        <hr />
      </div>
    );
  }
}

export default Title;
