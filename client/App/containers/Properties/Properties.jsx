import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SearchBar from './SearchBar/SearchBar';
import Title from './Title/Title';
import PropertyBox from './PropertyBox/PropertyBox';
import './Properties.less';

class Properties extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { properties } = this.props;
    return (
      <div className="propertiesPage">
        <Title />
        <SearchBar />
        {properties.map(property => (
          <PropertyBox
            key={property.propertyName}
            properties={property}
          />
        ))}
      </div>
    );
  }
}

export default Properties;

Properties.Proptype = {
  properties: PropTypes.object.isRequired
};
