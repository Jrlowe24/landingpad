import React, { Component } from 'react';
import './PropertyBox.less';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';


class PropertyBox extends Component {
  static formatPrice(num) {
    return `$${num}`;
  }

  static mapAmenity(amenity) {
    const amenities = {
      ac: 'https://image.flaticon.com/icons/svg/63/63341.svg',
      wifi: 'https://image.flaticon.com/icons/svg/159/159599.svg',
      tv: 'https://image.flaticon.com/icons/svg/130/130281.svg',
      shower: 'https://image.flaticon.com/icons/svg/1401/1401492.svg',
      kitchen: 'https://image.flaticon.com/icons/svg/45/45266.svg',
      towel: 'https://image.flaticon.com/icons/svg/1948/1948498.svg',
    };
    return amenities[`${amenity}`] || 'https://image.flaticon.com/icons/svg/907/907830.svg';
  }

  constructor(props) {
    super(props);
    this.state = {};
  }


  render() {
    const { properties } = this.props;
    const icons = [];
    for (let i = 0; i < properties.amenities.length; i += 1) {
      if (i < 5) {
        icons.push(<img
          className="propertiesIcon"
          alt="icons"
          src={PropertyBox.mapAmenity(properties.amenities[i])}
          key={i}
        />);
      } else {
        i = properties.amenities.length;
      }
    }
    return (
      <div className="propertyContainer">
        <Link
          to={`/properties/${properties.id}`}
        >
          <img
            alt="Main pic for Property"
            src={properties.img[0]}
            className="propertiesImage"
          />
        </Link>
        <div>
          <div className="descriptionContainer">
            <Link
              to={`/properties/${properties.id}`}
            >
              <h3 className="headerText">
                {properties.propertyName}
              </h3>
            </Link>
            <p className="descriptionText">
              {properties.description}
            </p>
            {icons}
            <div className="vl">
              <span>
                From
                <br />
                {PropertyBox.formatPrice(properties.price)}
              </span>
            </div>
            <br />
            <button type="submit" className="button">
            More Info
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default PropertyBox;

PropertyBox.Proptype = {
  properties: PropTypes.object.isRequired
};
