import React, { Component } from 'react';
import styles from './NoExtraChargesView.css';

export default class NoExtraChargesView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { createNewCharge } = this.props;
    return (
      <div className={styles.noChargesContainer}>
        <div className={styles.emptyCharges} />
        <h2>Create Your Extra Charges</h2>
        <p>
            Add compulsory or optional charges for extras like
          <br />
            laundry, cleaning, Wi-Fi, etc.
        </p>
        <button type="submit" className={styles.addCharge} onClick={createNewCharge}>+ Create New</button>
      </div>
    );
  }
}
