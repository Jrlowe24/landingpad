import React, { Component } from 'react';
import {
  Route,
  Switch
} from 'react-router-dom';
import ExtraCharges from './ExtraCharges';
import EditExtraCharge from './EditExtraCharge/EditExtraCharge';


export default class ExtraChargesRoutes extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { match, owner } = this.props;
    return (
      <Switch>
        <Route
          path={`${match.path}/extraCharge/:id`}
          render={props => (
            <EditExtraCharge owner={owner} {...props} />
          )}
        />
        <Route
          path={`${match.path}`}
          render={props => (
            <ExtraCharges owner={owner} {...props} />
          )}
        />
      </Switch>
    );
  }
}
