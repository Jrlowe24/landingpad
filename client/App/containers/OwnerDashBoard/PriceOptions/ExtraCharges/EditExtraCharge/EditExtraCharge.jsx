import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import Select from 'react-select';
import Input from '../../../Properties/Input/Input';
import Selectt from '../../../Properties/Input/Select';
import styles from './EditExtraCharge.css';

const chargeType = ['Mandatory', 'Optional Charge'];
const chargePer = ['Reservation', 'Room', 'Guest'];
export default class EditExtraCharge extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      propertyOptions: [],
      extraCharge: {}
    };
    this.handleInput = this.handleInput.bind(this);
    this.onPropertySelect = this.onPropertySelect.bind(this);
    this.handlePrice = this.handlePrice.bind(this);
    this.handleNonTaxableCheckBox = this.handleNonTaxableCheckBox.bind(this);
    this.save = this.save.bind(this);
  }

  componentWillMount() {
    // get current coupon
    const { match } = this.props;
    fetch(`http://localhost:3000/charge/${match.params.id}`).then(response => {
      response.json().then(data => {
        this.setState({ extraCharge: data, isLoading: false });
      });
    });
    // Getting Current Properties
    const { owner } = this.props;
    fetch(`http://localhost:3000/properties/${owner._id}`).then(response => {
      response.json().then(data => {
        const propertyOptions = [];
        data.forEach(d => {
          propertyOptions.push({ label: d.name, value: d._id });
        });
        this.setState({ propertyOptions, isLoading: false });
      });
    });
  }

  onPropertySelect(selectedProperties) {
    this.setState(prevState => ({
      extraCharge:
                {
                  ...prevState.extraCharge,
                  selectedProperties,
                }
    }));
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState(prevState => ({
      extraCharge:
            {
              ...prevState.extraCharge,
              [name]: value,
            }
    }));
  }

  handleNonTaxableCheckBox(e) {
    const { checked, name } = e.target;
    this.setState(prevState => ({
      extraCharge:
            {
              ...prevState.extraCharge,
              [name]: checked,
            }
    }));
  }

  handlePrice(e) {
    this.setState(prevState => ({
      extraCharge:
            {
              ...prevState.extraCharge,
              charge: e.floatValue,
            }
    }));
  }

  save() {
    const { match } = this.props;
    const { extraCharge } = this.state;
    // add propeties to the properties array
    const properties = [];
    extraCharge.selectedProperties.forEach(property => {
      properties.push(property.value);
    });
    this.setState(prevState => ({
      extraCharge:
                  {
                    ...prevState.extraCharge,
                    property: properties,
                  }
    }), async () => {
      // eslint-disable-next-line no-shadow
      const { extraCharge } = this.state;
      await fetch(`http://localhost:3000/charge/${match.params.id}`, {
        method: 'PATCH',
        body: JSON.stringify(
          extraCharge
        ),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
      }).then(response => {
        response.json().then(data => {
          this.setState({ extraCharge: data, isLoading: false });
        });
      });
    });
  }


  render() {
    const { isLoading, extraCharge, propertyOptions } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className={styles.bodyContainer}>
        <h1>Create New Extra Charge</h1>
        <button type="submit" onClick={this.save} className={styles.cancelButton}>Cancel</button>
        <button type="submit" onClick={this.save} className={styles.saveButton}>Save</button>
        <div className={styles.chargeContainer}>
          <div className={styles.adjacent}>
            <div className={styles.nameInput}>
              <Input
                type="text"
                value={extraCharge.name || ''}
                title="Name your Charge "
                name="name"
                placeholder="Here you can create extra charges for things like cleaning, Wi-Fi, parking, etc. Guests will see these charges in their booking summary."
                handleChange={this.handleInput}
                required
              />
            </div>
            <div className={styles.chargeType}>
              <Selectt
                title="Set charge type "
                name="chargeType"
                options={chargeType}
                value={extraCharge.chargeType}
                placeholder="Select charge type"
                handleChange={this.handleInput}
              />
            </div>
          </div>
          <div className={styles.adjacent}>
            <div className={styles.chargePer}>
              <Selectt
                title="Add Charge per "
                name="chargePer"
                options={chargePer}
                value={extraCharge.chargePer}
                placeholder="Add charge per"
                handleChange={this.handleInput}
              />
            </div>
            <div className={styles.setPrice}>
              <h4>* Set Price</h4>
              <NumberFormat
                thousandSeparator
                prefix="$"
                value={extraCharge.charge || 0}
                onValueChange={this.handlePrice}
              />
            </div>
            <div className={styles.taxable}>
              <input
                type="checkbox"
                name="taxable"
                checked={extraCharge.taxable || false}
                onChange={this.handleNonTaxableCheckBox}
                className={styles.taxableCheckMark}
              />
              <h4>Non-Taxable</h4>
            </div>
          </div>
          <Select
            value={extraCharge.selectedProperties}
            onChange={this.onPropertySelect}
            options={propertyOptions}
            isMulti
          />
        </div>
      </div>
    );
  }
}
