import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './ExtraChargeViewBox.css';
import Button from '../../../../../components/Button/Button';

export default class ExtraChargeViewBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      extraCharge: props.extraCharge || {},
      isLoading: false
    };
  }

  render() {
    const { isLoading, extraCharge } = this.state;
    const { match, onDeleteClick } = this.props;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }
    const id = extraCharge._id;

    return (
      <div className={styles.extraChargeBoxContainer}>
        <h2 className={styles.title}>{extraCharge.name}</h2>
        <p className={styles.text}>
          {`$${extraCharge.charge}`}
        </p>
        <p className={styles.text}>
          {extraCharge.chargeType}
        </p>
        <p className={styles.text}>
          {extraCharge.chargePer}
        </p>
        <div className={styles.buttons}>
          <Link to={`${match.path}/extraCharge/${id}`}>
            <Button edit />
          </Link>
          <Button onClick={() => onDeleteClick(extraCharge._id)} del />
        </div>
      </div>
    );
  }
}
