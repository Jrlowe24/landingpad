import React, { Component } from 'react';
import ExtraChargeViewBox from './ExtraChargeViewBox/ExtraChargeViewBox';
import NoExtraChargesView from './NoExtraChargesView/NoExtraChargesView';
import styles from './ExtraCharges.css';

export default class ExtraCharges extends Component {
  constructor(props) {
    super(props);
    this.state = {
      extraCharges: [],
      isLoading: false
    };
    this.createNewCharge = this.createNewCharge.bind(this);
    this.updateCharges = this.updateCharges.bind(this);
    this.deleteSelectedCharge = this.deleteSelectedCharge.bind(this);
  }

  componentDidMount() {
    // Getting current charges
    const { owner } = this.props;
    fetch(`http://localhost:3000/charge/owner/${owner._id}`).then(response => {
      response.json().then(data => {
        this.setState({ extraCharges: data, isLoading: false });
      });
    });
  }

  async createNewCharge() {
    const { owner } = this.props;
    await fetch('http://localhost:3000/charge', {
      method: 'POST',
      body: JSON.stringify(
        { owner: owner._id }
      ),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        const { location, history } = this.props;
        history.push(`${location.pathname}/extraCharge/${data}`);
      });
    });
  }

  async deleteSelectedCharge(id) {
    await fetch(`http://localhost:3000/charge/${id}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        console.log(data);
      });
      this.updateCharges();
    });
  }

  updateCharges() {
    // const ownerId = '5d4c4e8d3a41cdd284013454';
    const { owner } = this.props;
    fetch(`http://localhost:3000/charge/owner/${owner._id}`).then(response => {
      response.json().then(data => {
        this.setState({ extraCharges: data, isLoading: false });
      });
    });
  }

  render() {
    const { isLoading, extraCharges } = this.state;
    const { match } = this.props;

    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    if (extraCharges.length === 0) {
      return (
        <NoExtraChargesView createNewCharge={this.createNewCharge} />
      );
    }

    return (
      <div className={styles.bodyContainer}>
        <h1>Extra Charges</h1>
        <button
          type="submit"
          onClick={this.createNewCharge}
          className={styles.createNewButton}
        >
          + Create New
        </button>
        {extraCharges.map(extraCharge => (
          <ExtraChargeViewBox
            key={extraCharge._id}
            extraCharge={extraCharge}
            match={match}
            onDeleteClick={this.deleteSelectedCharge}
          />
        ))}
      </div>
    );
  }
}
