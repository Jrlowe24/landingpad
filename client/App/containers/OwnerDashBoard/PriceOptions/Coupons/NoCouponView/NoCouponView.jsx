import React, { Component } from 'react';
import styles from './NoCouponView.css';

export default class NoCouponView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { createNewCoupon } = this.props;
    return (
      <div className={styles.noCouponContainer}>
        <div className={styles.emptyCoupons} />
        <h2>Create Your Coupons</h2>
        <p>
            Treat your guests with coupon codes! Create coupons and
          <br />
            offer nightly discounts
        </p>
        <button type="submit" className={styles.addCoupon} onClick={createNewCoupon}>+ Create New</button>
      </div>
    );
  }
}
