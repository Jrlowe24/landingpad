import React, { Component } from 'react';
import moment from 'moment';
import NumberFormat from 'react-number-format';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import Select from 'react-select';

import { formatDate, parseDate } from 'react-day-picker/moment';
import Input from '../../../Properties/Input/Input';
import Selectt from '../../../Properties/Input/Select';
import styles from './EditCoupons.css';
import Button from '../../../../../components/Button/Button';


const couponType = ['Payment Discount', '% Discount'];
export default class EditCoupons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      couponInfo: props.couponInfo || {},
      propertyOptions: [],
      isLoading: true
    };
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.onPropertySelect = this.onPropertySelect.bind(this);
    this.save = this.save.bind(this);
    this.handleLimit = this.handleLimit.bind(this);
    this.handleDiscount = this.handleDiscount.bind(this);
  }

  componentWillMount() {
    // get current coupon
    const { match } = this.props;
    fetch(`http://localhost:3000/coupon/${match.params.id}`).then(response => {
      response.json().then(data => {
        const dataWithFormattedDates = data;
        dataWithFormattedDates.validFrom = data.validFrom ? new Date(data.validFrom) : undefined;
        dataWithFormattedDates.validTo = data.validTo ? new Date(data.validTo) : undefined;
        this.setState({ couponInfo: dataWithFormattedDates, isLoading: false });
      });
    });
    // Getting Current Properties
    const { owner } = this.props;
    fetch(`http://localhost:3000/properties/${owner._id}`).then(response => {
      response.json().then(data => {
        const propertyOptions = [];
        data.forEach(d => {
          propertyOptions.push({ label: d.name, value: d._id });
        });
        this.setState({ propertyOptions, isLoading: false });
      });
    });
  }

  onPropertySelect(selectedProperties) {
    this.setState(prevState => ({
      couponInfo:
                {
                  ...prevState.couponInfo,
                  selectedProperties,
                }
    }));
  }

  handleDiscount(e) {
    this.setState(prevState => ({
      couponInfo:
                {
                  ...prevState.couponInfo,
                  paymentDiscount: e.floatValue,
                }
    }));
  }

  handleLimit(e) {
    this.setState(prevState => ({
      couponInfo:
                {
                  ...prevState.couponInfo,
                  couponLimit: e.floatValue,
                }
    }));
  }

  showFromMonth() {
    const { couponInfo } = this.state;
    const { validFrom, validTo } = couponInfo;
    if (!validFrom) {
      return;
    }
    if (moment(validTo).diff(moment(validFrom), 'months') < 1) {
      this.validTo.getDayPicker().showMonth(validFrom);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState(prevState => ({
      couponInfo:
                {
                  ...prevState.couponInfo,
                  validFrom: from,
                }
    }));
  }

  handleToChange(to) {
    this.setState(prevState => ({
      couponInfo:
                {
                  ...prevState.couponInfo,
                  validTo: to,
                }
    }), this.showFromMonth);
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState(prevState => ({
      couponInfo:
                  {
                    ...prevState.couponInfo,
                    [name]: value,
                  }
    }));
  }

  async save() {
    const { match } = this.props;
    const { couponInfo } = this.state;
    // add propeties to the properties array
    const properties = [];
    couponInfo.selectedProperties.forEach(property => {
      properties.push(property.value);
    });
    this.setState(prevState => ({
      couponInfo:
                  {
                    ...prevState.couponInfo,
                    property: properties,
                  }
    }), async () => {
      // eslint-disable-next-line no-shadow
      const { couponInfo } = this.state;
      await fetch(`http://localhost:3000/coupon/${match.params.id}`, {
        method: 'PATCH',
        body: JSON.stringify(
          couponInfo
        ),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
      }).then(response => {
        response.json().then(data => {
          const dataWithFormattedDates = data;
          dataWithFormattedDates.validFrom = data.validFrom ? new Date(data.validFrom) : undefined;
          dataWithFormattedDates.validTo = data.validTo ? new Date(data.validTo) : undefined;
          this.setState({ couponInfo: dataWithFormattedDates, isLoading: false });
        });
      });
    });
  }

  render() {
    const {
      couponInfo, propertyOptions, isLoading
    } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }
    const {
      validFrom, validTo, name, couponLimit, type, paymentDiscount, code, minNightStay, selectedProperties
    } = couponInfo;
    const modifiers = { start: validFrom, end: validTo };
    return (
      <div className={styles.bodyContainer}>
        <div>
          <h1>Create a New Coupon</h1>
          <div className={styles.buttons}>
            <Button cancel text="Cancel" onClick={() => window.location.reload()} />
            <Button onClick={this.save} text="Save" />
          </div>
        </div>
        <div className={styles.couponContainer}>
          <div className={styles.adjacent}>
            <div className={styles.nameInput}>
              <Input
                type="text"
                value={name || ''}
                title="Name your coupon: "
                name="name"
                placeholder="Enter your coupon's name"
                handleChange={this.handleInput}
              />
            </div>
            <div className={styles.spacingBetween}>
              <Selectt
                title="Set coupon type "
                name="type"
                options={couponType}
                value={type}
                placeholder="Select coupon type"
                handleChange={this.handleInput}
              />
            </div>
            <div className={`${styles.onTop} ${styles.spacingBetween}`}>
              <h4>Payment Discount</h4>
              <NumberFormat
                thousandSeparator
                prefix={type === '% Discount' ? '%' : '$'}
                defaultValue={paymentDiscount || 0}
                onValueChange={this.handleDiscount}
              />
            </div>
          </div>
          <div className={styles.adjacent}>
            <div className={`InputFromTo ${styles.adjacentNoSpacing}`}>
              <div className={`${styles.onTop} ${styles.fromDatePicker}`}>
                <h4>Valid from: </h4>
                <DayPickerInput
                  value={validFrom || new Date()}
                  placeholder="From"
                  format="LL"
                  formatDate={formatDate}
                  parseDate={parseDate}
                  dayPickerProps={{
                    selectedDays: [validFrom, { validFrom, validTo }],
                    disabledDays: { after: validTo, before: moment().toDate() },
                    toMonth: validTo,
                    modifiers,
                    numberOfMonths: 1,
                    onDayClick: () => this.validTo.getInput().focus(),
                  }}
                  onDayChange={this.handleFromChange}
                />
              </div>
              <div className={`${styles.onTop} ${styles.spacingBetween}`}>
                <h4>  Valid to: </h4>
                <span className="InputFromTo-to">
                  <DayPickerInput
                    ref={el => { this.validTo = el; }}
                    value={validTo}
                    placeholder="To"
                    format="LL"
                    formatDate={formatDate}
                    parseDate={parseDate}
                    dayPickerProps={{
                      selectedDays: [validFrom, { validFrom, validTo }],
                      disabledDays: { before: validFrom },
                      modifiers,
                      month: validFrom,
                      fromMonth: validFrom,
                      numberOfMonths: 1,
                    }}
                    onDayChange={this.handleToChange}
                  />
                </span>
              </div>
            </div>
            <div className={`${styles.onTop} ${styles.spacingBetween}`}>
              <h4>Limit number of coupons to</h4>
              <NumberFormat
                thousandSeparator
                suffix=" Coupon(s)"
                value={couponLimit || 0}
                onValueChange={this.handleLimit}
              />
            </div>
            <div className={styles.spacingBetween}>
              <Input
                type="number"
                title="Minimum night stay"
                name="minNightStay"
                value={minNightStay || 0}
                placeholder="Enter your Minimum Nights Required"
                handleChange={this.handleInput}
              />
            </div>
          </div>
          <div className={styles.adjacent}>
            <div className={styles.codeInput}>
              <Input
                type="text"
                title="Coupon Code"
                name="code"
                value={code || ''}
                placeholder="Enter your Coupon Id"
                handleChange={this.handleInput}
              />
            </div>
            <div className={styles.selectedProperties}>
              <Select
                value={selectedProperties}
                onChange={this.onPropertySelect}
                options={propertyOptions}
                isMulti
              />
            </div>
          </div>
        </div>
      </div>
      
    );
  }
}
