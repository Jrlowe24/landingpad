
import React, { Component } from 'react';
import {
  Route,
  Switch
} from 'react-router-dom';
import Coupons from './Coupons';
import EditCoupons from './EditCoupon/EditCoupons';


export default class ExtraChargesRoutes extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { match, owner } = this.props;
    return (
      <Switch>
        <Route
          path={`${match.path}/coupon/:id`}
          render={props => (
            <EditCoupons owner={owner} {...props} />
          )}
        />
        <Route
          path={`${match.path}`}
          render={props => (
            <Coupons owner={owner} {...props} />
          )}
        />
      </Switch>
    );
  }
}
