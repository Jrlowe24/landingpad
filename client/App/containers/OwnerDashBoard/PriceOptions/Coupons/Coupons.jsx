import React, { Component } from 'react';
import ViewCouponBox from './ViewCouponBox/ViewCouponBox';
import NoCouponView from './NoCouponView/NoCouponView';
import styles from './Coupons.css';
import Button from '../../../../components/Button/Button';

export default class Coupons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coupons: [],
      isLoading: false
    };
    this.createNewCoupon = this.createNewCoupon.bind(this);
    this.deleteSelectedCoupon = this.deleteSelectedCoupon.bind(this);
    this.updateCoupons = this.updateCoupons.bind(this);
  }

  componentDidMount() {
    // const ownerId = '5d4c4e8d3a41cdd284013454';
    const { owner } = this.props;
    fetch(`http://localhost:3000/coupon/owner/${owner._id}`).then(response => {
      response.json().then(data => {
        this.setState({ coupons: data, isLoading: false });
      });
    });
  }

  async createNewCoupon() {
    const { owner } = this.props;
    await fetch('http://localhost:3000/coupon', {
      method: 'POST',
      body: JSON.stringify(
        { owner: owner._id }
      ),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        const { location, history } = this.props;
        history.push(`${location.pathname}/coupon/${data}`);
      });
    });
  }

  async deleteSelectedCoupon(id) {
    await fetch(`http://localhost:3000/coupon/${id}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        console.log(data);
      });
      this.updateCoupons();
    });
  }

  updateCoupons() {
    const { owner } = this.props;
    fetch(`http://localhost:3000/coupon/owner/${owner._id}`).then(response => {
      response.json().then(data => {
        this.setState({ coupons: data, isLoading: false });
      });
    });
  }

  render() {
    const { isLoading, coupons } = this.state;
    const { match } = this.props;

    if (isLoading) {
      return <h2>Loading...</h2>;
    }


    if (coupons.length === 0) {
      return (
        <NoCouponView createNewCoupon={this.createNewCoupon} />
      );
    }

    return (
      <div className={styles.bodyContainer}>
        <h1>Coupons</h1>
        <Button
          text="+ Create New"
          onClick={this.createNewCoupon}
          className={styles.createNewButton}
        />
        {coupons.map(coupon => (
          <ViewCouponBox
            key={coupon._id}
            coupon={coupon}
            match={match}
            onDeleteClick={this.deleteSelectedCoupon}
          />
        ))}
      </div>
    );
  }
}
