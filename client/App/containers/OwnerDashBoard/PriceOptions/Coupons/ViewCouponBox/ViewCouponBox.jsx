import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Button from '../../../../../components/Button/Button';
import styles from './ViewCouponBox.css';

export default class ViewCouponBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coupon: props.coupon || {},
      isLoading: false
    };
  }

  render() {
    const { isLoading, coupon } = this.state;
    const { match, onDeleteClick } = this.props;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }
    const id = coupon._id;

    return (
      <div className={styles.couponBoxContainer}>
        <h2 className={styles.title}>{coupon.name}</h2>
        <p className={styles.text}>{coupon.code}</p>
        <p className={styles.text}>
          {moment(coupon.startDate).format('MMM Do')}
          {'-'}
          {moment(coupon.endDate).format('MMM Do')}
        </p>
        <p className={styles.text}>
          {coupon.timesRedeemed || 0}
          {'/'}
          {coupon.couponLimit || 0}
        </p>
        <div className={styles.buttons}>
          <Link to={`${match.path}/coupon/${id}`}>
            <Button edit />
          </Link>
          <Button del onClick={() => onDeleteClick(coupon._id)} />
        </div>
      </div>
    );
  }
}
