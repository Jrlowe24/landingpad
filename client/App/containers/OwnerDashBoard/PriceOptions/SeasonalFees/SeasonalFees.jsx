import React, { Component } from 'react';
import SeasonalRuleBox from './SeasonalRuleBox/SeasonalRuleBox';
import NoSeasonalRulesView from './NoSeasonalRulesView/NoSeasonalRuleView';
import './SeasonalRules.less';
import styles from './SeasonalRules.css';

export default class SeasonalFees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seasonalFees: [],
      isLoading: true
    };
    this.createNewSeasonalRule = this.createNewSeasonalRule.bind(this);
    this.updateSeasonalRules = this.updateSeasonalRules.bind(this);
    this.deleteSelectedSeasonalRule = this.deleteSelectedSeasonalRule.bind(this);
  }

  componentDidMount() {
    // Getting current seasonal Charges
    // const id = '5d4c4e8d3a41cdd284013454';
    const { owner } = this.props;
    fetch(`http://localhost:3000/seasonalRules/owner/${owner._id}`).then(response => {
      response.json().then(data => {
        this.setState({ seasonalFees: data, isLoading: false });
      });
    });
    this.setState({ isLoading: false });
  }

  async createNewSeasonalRule() {
    const { owner } = this.props;
    await fetch('http://localhost:3000/seasonalRule', {
      method: 'POST',
      body: JSON.stringify(
        { owner: owner._id }
      ),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        const { location, history } = this.props;
        history.push(`${location.pathname}/seasonalRule/${data}`);
      });
    });
  }

  async deleteSelectedSeasonalRule(id) {
    await fetch(`http://localhost:3000/seasonalRules/${id}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        console.log(data);
      });
      this.updateSeasonalRules();
    });
  }

  updateSeasonalRules() {
    const { owner } = this.props;
    fetch(`http://localhost:3000/seasonalRules/owner/${owner._id}`).then(response => {
      response.json().then(data => {
        this.setState({ seasonalFees: data, isLoading: false });
      });
    });
  }

  render() {
    const { isLoading, seasonalFees } = this.state;
    const { match } = this.props;
    console.log(this.props);
    console.log(match.path);

    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    if (seasonalFees.length === 0) {
      return (
        <NoSeasonalRulesView className={styles.noRulesContainer} createNewSeasonalRule={this.createNewSeasonalRule} />
      );
    }

    return (
      <div className={styles.withRulesContainer}>
        <div className={styles.body}>
          <h1>Seasonal Rules</h1>
          <button
            type="submit"
            onClick={this.createNewSeasonalRule}
            className={styles.addNewRuleButton}
          >
          + Create New
          </button>
          {seasonalFees.map(seasonalFee => (
            <SeasonalRuleBox
              key={seasonalFee._id}
              seasonalRule={seasonalFee}
              match={match}
              onDeleteClick={this.deleteSelectedSeasonalRule}
            />
          ))}
        </div>
      </div>
    );
  }
}
