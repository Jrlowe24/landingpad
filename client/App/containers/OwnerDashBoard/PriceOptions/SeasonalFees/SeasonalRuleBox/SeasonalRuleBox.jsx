import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import styles from './SeasonalRuleBox.css';
import Button from '../../../../../components/Button/Button';

export default class SeasonalFeeBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seasonalRule: props.seasonalRule || {},
      isLoading: false
    };
  }

  render() {
    const { isLoading, seasonalRule } = this.state;
    const { match, onDeleteClick } = this.props;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className={styles.seasonalRuleBoxContainer}>
        <h2 className={styles.title}>{seasonalRule.name}</h2>
        <p className={styles.text}>
          {moment(seasonalRule.startDate).format('MMM Do')}
          {'-'}
          {moment(seasonalRule.endDate).format('MMM Do')}
        </p>
        {(moment().isAfter(moment(seasonalRule.endDate))) ? <p className={styles.text}>InActive</p> : <p className={styles.text}>Active</p>}
        <div className={styles.buttons}>
          <Link to={`${match.path}/seasonalRule/${seasonalRule._id}`}>
            <Button edit />
          </Link>
          <Button del onClick={() => onDeleteClick(seasonalRule._id)} />
        </div>
      </div>
    );
  }
}
