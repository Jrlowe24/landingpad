import React, { Component } from 'react';
import moment from 'moment';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import Select from 'react-select';
import './GeneralSeasonalFeeBox.less';

import { formatDate, parseDate } from 'react-day-picker/moment';
import Input from '../../../Properties/Input/Input';
import PropertySeasonalFeeBox from '../PropertySeasonalFeeBox/PropertySeasonalFeeBox';
import styles from './GeneralSeasonalFeeBox.css';


export default class GeneralSeasonalFeeBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seasonalRule: props.seasonalRule || {},
      propertyOptions: [],
      isLoading: true
    };
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.onPropertySelect = this.onPropertySelect.bind(this);
    this.addPropertyToMap = this.addPropertyToMap.bind(this);
    this.deletePropertyFromMap = this.deletePropertyFromMap.bind(this);
    this.updateMap = this.updateMap.bind(this);
    this.save = this.save.bind(this);
  }

  componentDidMount() {
    const { match } = this.props;
    // Get current seasonal Rule
    fetch(`http://localhost:3000/seasonalRules/${match.params.id}`).then(response => {
      response.json().then(data => {
        const dataWithFormattedDates = data;
        dataWithFormattedDates.startDate = data.startDate ? new Date(data.startDate) : undefined;
        dataWithFormattedDates.endDate = data.endDate ? new Date(data.endDate) : undefined;
        dataWithFormattedDates.propertyInfoMap = data.propertyInfoMap ? new Map(Object.entries(data.propertyInfoMap)) : undefined;
        this.setState({ seasonalRule: dataWithFormattedDates, isLoading: false });
      });
    });
    // Getting Current Properties
    const { owner } = this.props;
    fetch(`http://localhost:3000/properties/${owner._id}`).then(response => {
      response.json().then(data => {
        const propertyOptions = [];
        data.forEach(d => {
          propertyOptions.push({ label: d.name, value: d._id });
        });
        this.setState({ propertyOptions });
      });
    });
  }


  onPropertySelect(selectedProperties) {
    const { seasonalRule } = this.state;
    const prevSelectedProperties = seasonalRule.selectedProperties || [];
    const selectedPropertiesCheck = selectedProperties || [];
    if (selectedPropertiesCheck.length < prevSelectedProperties.length) {
      this.deletePropertyFromMap(selectedProperties);
    } else {
      this.addPropertyToMap(selectedProperties[selectedProperties.length - 1].value);
    }
    this.setState(prevState => ({
      seasonalRule:
                {
                  ...prevState.seasonalRule,
                  selectedProperties: selectedPropertiesCheck,
                }
    }));
  }

  deletePropertyFromMap(selectedProperties) {
    const { seasonalRule } = this.state;
    if (!selectedProperties) {
      const { propertyInfoMap } = seasonalRule;
      propertyInfoMap.clear();
      this.setState(prevState => ({
        seasonalRule:
                {
                  ...prevState.seasonalRule,
                  propertyInfoMap,
                }
      }));
    } else {
      const res = seasonalRule.selectedProperties.filter(item1 => !selectedProperties.some(item2 => (item2.value === item1.value)));
      res.forEach(propertyToDelete => {
        const { propertyInfoMap } = seasonalRule;
        propertyInfoMap.delete(propertyToDelete.value);
        this.setState(prevState => ({
          seasonalRule:
                {
                  ...prevState.seasonalRule,
                  propertyInfoMap,
                }
        }));
      });
    }
  }

  addPropertyToMap(selectedProperty) {
    const { seasonalRule } = this.state;
    let { propertyInfoMap } = seasonalRule;
    if (propertyInfoMap === undefined) {
      propertyInfoMap = new Map();
    }
    const price = {
      weekdayPrice: 0,
      weekendPrice: 0,
      weeklyPrice: undefined,
      monthlyPrice: undefined,
    };
    propertyInfoMap.set(selectedProperty, price);
    this.setState(prevState => ({
      seasonalRule:
                {
                  ...prevState.seasonalRule,
                  propertyInfoMap,
                }
    }));
  }

  showFromMonth() {
    const { seasonalRule } = this.state;
    const { startDate, endDate } = seasonalRule;
    if (!startDate) {
      return;
    }
    if (moment(endDate).diff(moment(startDate), 'months') < 1) {
      this.endDate.getDayPicker().showMonth(startDate);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState(prevState => ({
      seasonalRule:
                {
                  ...prevState.seasonalRule,
                  startDate: from,
                }
    }));
  }

  handleToChange(to) {
    this.setState(prevState => ({
      seasonalRule:
                {
                  ...prevState.seasonalRule,
                  endDate: to,
                }
    }), this.showFromMonth);
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState(prevState => ({
      seasonalRule:
                  {
                    ...prevState.seasonalRule,
                    [name]: value,
                  }
    }));
  }


  save() {
    const { match } = this.props;
    const { seasonalRule } = this.state;
    // add propeties to the properties array
    const properties = [];
    seasonalRule.selectedProperties.forEach(property => {
      properties.push(property.value);
    });
    // add properity info into map
    this.setState({ isLoading: true });

    this.setState(prevState => ({
      seasonalRule:
                  {
                    ...prevState.seasonalRule,
                    property: properties,
                  }
    }), async () => {
      // eslint-disable-next-line no-shadow
      const { seasonalRule } = this.state;
      await fetch(`http://localhost:3000/seasonalRules/prop/${match.params.id}`, {
        method: 'PATCH',
        body: JSON.stringify(
          seasonalRule
        ),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
      }).then(response => {
        response.json().then(data => {
          const dataWithFormattedDates = data;
          dataWithFormattedDates.startDate = data.startDate ? new Date(data.startDate) : undefined;
          dataWithFormattedDates.endDate = data.endDate ? new Date(data.endDate) : undefined;
          dataWithFormattedDates.propertyInfoMap = data.propertyInfoMap ? new Map(Object.entries(data.propertyInfoMap)) : undefined;
          this.setState({ seasonalRule: dataWithFormattedDates, isLoading: false });
        });
      });
    });
  }

  updateMap(id, value) {
    const { seasonalRule } = this.state;
    const { propertyInfoMap } = seasonalRule;
    propertyInfoMap.set(id, value);
    this.setState(prevState => ({
      seasonalRule:
                {
                  ...prevState.seasonalRule,
                  propertyInfoMap,
                }
    }));
  }

  render() {
    const {
      seasonalRule, propertyOptions, isLoading
    } = this.state;
    const {
      startDate, endDate, name, minNights, maxNights
    } = seasonalRule;
    const modifiers = { start: startDate, end: endDate };
    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className={styles.bodyContainer}>
        <div className={styles.infoAbove}>
          <h1>Create New Seasonal Rule</h1>
          <button type="submit" onClick={this.save} className={styles.cancelButton}>Cancel</button>
          <button type="submit" onClick={this.save}>Save</button>
        </div>
        <div className={styles.feeContainer}>
          <div className={styles.adjacent}>
            <div className={styles.nameInput}>
              <Input
                type="text"
                value={name || ''}
                title="Name your season: "
                name="name"
                placeholder="Enter your season's name"
                handleChange={this.handleInput}
              />
            </div>
            <div className={`InputFromTo ${styles.adjacent} ${styles.smallInput}`}>
              <div className={styles.seperateNextTo}>
                <p className={styles.textAboveInput}>* Start date: </p>
                <DayPickerInput
                  value={startDate}
                  placeholder="From"
                  format="LL"
                  formatDate={formatDate}
                  parseDate={parseDate}
                  dayPickerProps={{
                    selectedDays: [startDate, { startDate, endDate }],
                    disabledDays: { after: endDate, before: moment().toDate() },
                    toMonth: endDate,
                    modifiers,
                    numberOfMonths: 1,
                    onDayClick: () => this.endDate.getInput().focus(),
                  }}
                  onDayChange={this.handleFromChange}
                />
              </div>
              <div className={styles.seperateNextTo}>
                <p className={styles.textAboveInput}>  End date: </p>
                <span className="InputFromTo-to">
                  <DayPickerInput
                    ref={el => { this.endDate = el; }}
                    value={endDate}
                    placeholder="To"
                    format="LL"
                    formatDate={formatDate}
                    parseDate={parseDate}
                    dayPickerProps={{
                      selectedDays: [startDate, { startDate, endDate }],
                      disabledDays: { before: startDate },
                      modifiers,
                      month: startDate,
                      fromMonth: startDate,
                      numberOfMonths: 1,
                    }}
                    onDayChange={this.handleToChange}
                  />
                </span>
              </div>
            </div>
          </div>
          <div>
            
            <div className={styles.adjacent}>
              <div className={styles.InputPropertySelect}>
                <p className={styles.textAboveInputPropertySelect}>* Select Properties </p>
                <Select
                  value={seasonalRule.selectedProperties}
                  onChange={this.onPropertySelect}
                  options={propertyOptions}
                  isMulti
                  className={styles.selectProperties}
                />
              </div>
              <div className={styles.nightInput}>
                <Input
                  type="number"
                  title="Min Nights"
                  name="minNights"
                  value={minNights || 0}
                  placeholder="Enter your Minimum Nights Required"
                  handleChange={this.handleInput}
                />
              </div>
              <div className={styles.nightInput}>
                <Input
                  type="number"
                  title="Max Nights"
                  name="maxNights"
                  value={maxNights || 0}
                  placeholder="Enter your Minimum Nights Required"
                  handleChange={this.handleInput}
                />
              </div>
            </div>
          </div>
        </div>
        {seasonalRule.selectedProperties.map(property => (
          <PropertySeasonalFeeBox
            key={property.value}
            currentPropertyId={property.value}
            seasonalRule={seasonalRule.propertyInfoMap.get(property.value)}
            updateMap={this.updateMap}
          />
        ))}
      </div>
      
    );
  }
}
