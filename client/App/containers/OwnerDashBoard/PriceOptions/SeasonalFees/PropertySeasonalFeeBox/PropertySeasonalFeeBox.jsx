import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import styles from './PropertySeasonalFeeBox.css';


export default class PropertySeasonalFeeBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentProperty: {},
      seasonalRule: props.seasonalRule || {},
      isLoading: true
    };

    this.handleWeekdayPrice = this.handleWeekdayPrice.bind(this);
    this.handleWeekendPrice = this.handleWeekendPrice.bind(this);
    this.handleWeeklyPrice = this.handleWeeklyPrice.bind(this);
    this.handleMonthlyPrice = this.handleMonthlyPrice.bind(this);
  }

  componentDidMount() {
    // Getting Current Property
    const { currentPropertyId } = this.props;
    fetch(`http://localhost:3000/property/${currentPropertyId}`).then(response => {
      response.json().then(data => {
        this.setState({ currentProperty: data, isLoading: false });
      });
    });
  }

  handleWeekdayPrice(e) {
    this.setState(prevState => ({
      seasonalRule:
              { ...prevState.seasonalRule, weekdayPrice: e.floatValue }
    }), () => {
      const { seasonalRule } = this.state;
      const { updateMap, currentPropertyId } = this.props;
      updateMap(currentPropertyId, seasonalRule);
    });
  }

  handleWeekendPrice(e) {
    this.setState(prevState => ({
      seasonalRule:
              { ...prevState.seasonalRule, weekendPrice: e.floatValue }
    }), () => {
      const { seasonalRule } = this.state;
      const { updateMap, currentPropertyId } = this.props;
      updateMap(currentPropertyId, seasonalRule);
    });
  }

  handleWeeklyPrice(e) {
    this.setState(prevState => ({
      seasonalRule:
              { ...prevState.seasonalRule, weeklyPrice: e.floatValue }
    }), () => {
      const { seasonalRule } = this.state;
      const { updateMap, currentPropertyId } = this.props;
      updateMap(currentPropertyId, seasonalRule);
    });
  }

  handleMonthlyPrice(e) {
    this.setState(prevState => ({
      seasonalRule:
              { ...prevState.seasonalRule, monthlyPrice: e.floatValue }
    }), () => {
      const { seasonalRule } = this.state;
      const { updateMap, currentPropertyId } = this.props;
      updateMap(currentPropertyId, seasonalRule);
    });
  }

  render() {
    const { currentProperty, isLoading, seasonalRule } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }
  
    return (
      <div className={styles.seasonalFeePropertyContainer}>
        <div className={styles.adjacent}>
          <img
            alt="Main pic for Property"
            src={currentProperty.images[0]}
            className={styles.propertySeasonalImage}
          />
          <div className="descriptionContainer">
            <h1 className="headerText">
              {currentProperty.name || 'no descirption'}
            </h1>
          </div>
        </div>
        <div className={styles.adjacentt}>
          <div>
            <h3>Weekday</h3>
            <NumberFormat
              thousandSeparator
              prefix="$"
              defaultValue={seasonalRule ? seasonalRule.weekdayPrice : 0}
              onValueChange={this.handleWeekdayPrice}
              className={styles.feePriceInput}
            />
          </div>
          <div>
            <h3>Weekend</h3>
            <NumberFormat
              thousandSeparator
              prefix="$"
              defaultValue={seasonalRule ? seasonalRule.weekendPrice : 0}
              onValueChange={this.handleWeekendPrice}
              className={styles.feePriceInput}
            />
          </div>
          <div>
            <h3>Weekly</h3>
            <NumberFormat
              thousandSeparator
              prefix="$"
              defaultValue={seasonalRule ? seasonalRule.weeklyPrice : undefined}
              onValueChange={this.handleWeeklyPrice}
              className={styles.feePriceInput}
            />
          </div>
          <div>
            <h3>Monthly</h3>
            <NumberFormat
              thousandSeparator
              prefix="$"
              defaultValue={seasonalRule ? seasonalRule.monthlyPrice : undefined}
              onValueChange={this.handleMonthlyPrice}
              className={styles.feePriceInput}
            />
          </div>
        </div>
      </div>
    );
  }
}
