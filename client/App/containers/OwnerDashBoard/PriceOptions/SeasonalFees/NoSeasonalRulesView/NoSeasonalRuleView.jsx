import React, { Component } from 'react';
import styles from './NoSeasonalRulesView.css';

export default class SeasonalFees extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { createNewSeasonalRule } = this.props;
    return (
      <div className={styles.noRulesContainer}>
        <div className={styles.emptyRules} />
        <h2>Set Your Seasonal Rules</h2>
        <p translate="rates.empty.MESSAGE">
            Set special rules for Christmas, summer and other peak periods
          <br />
            (even conventions or sporting events!)
        </p>
        <button type="submit" className={styles.addRule} onClick={createNewSeasonalRule}>+ Create New</button>
      </div>
    );
  }
}
