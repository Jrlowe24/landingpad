import React, { Component } from 'react';
import {
  Route,
  Switch
} from 'react-router-dom';
import GeneralSeasonalFeeBox from './SeasonalFees/GeneralSeasonalFeeBox/GeneralSeasonalFeeBox';
import SeasonalFees from './SeasonalFees/SeasonalFees';


export default class PriceOptions extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { match, owner } = this.props;
    return (
      <Switch>
        <Route
          path={`${match.path}/seasonalRule/:id`}
          render={props => (
            <GeneralSeasonalFeeBox owner={owner} {...props} />
          )}
        />
        <Route
          path={`${match.path}`}
          // component={SeasonalFees}
          render={props => (
            <SeasonalFees owner={owner} {...props} />
          )}
          exact
        />
      </Switch>
    );
  }
}
