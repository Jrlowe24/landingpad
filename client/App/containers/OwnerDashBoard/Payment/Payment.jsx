import React, { Component } from 'react';
import styles from './Payment.css';
import stripelogo from './stripe-icon.png';
import paypal from './paypal-logo.png';
import check from './Checkmark.png';

const firebase = require('firebase');

export default class Payment extends Component { // eslint-disable-line
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  logout = () => {
    firebase.auth().signOut()
      .then(() => {
      });
  };

  render() {
    const {
      owner
    } = this.props;
    return (
      <div className={styles.container}>
        <div className={styles.main}>
          <button type="button" onClick={this.logout}>
          logout
          </button>
          <div className={styles.header}>
          Payments
          </div>
          <div className={styles.descript}>
          Choose the online or offline payments that work best for you (more options coming soon).
          </div>
          <div className={styles.paymentDiv}>
            <div className={styles.logo}>
              <img className={styles.stripelogo} src={stripelogo} alt="stripe" />
            </div>
            <div className={styles.description}>
              <ul className={styles.list}>
                <li className={styles.listItem}> Accept Visa and Mastercard online </li>
                <li className={styles.listItem}> High security standards </li>
                <li className={styles.listItem}> Seamless with sites look and feel </li>
                <li className={styles.listItem}> 2.9% + 30 cents per transaction </li>
              </ul>
            </div>
            <div className={styles.connect}>
              {!owner.stripeToken ? (
                <a
                  href={`https://connect.stripe.com/express/oauth/authorize?redirect_uri=http://localhost:3000/payments&client_id=ca_Fg45i9lgIh2wDNyIsHq5Kz0DzbGisTUW&state=${owner._id}`}
                  className={styles.link}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <button type="button" className={styles.button}> Connect</button>
                </a>
              )
                : (
                  <div className={styles.connectedDiv}>
                    <img src={check} className={styles.checkmark} alt="check" />
                    <div className={styles.connected}>Connected</div>
                  </div>
                )
            }
            </div>
          </div>
          <div className={styles.paymentDiv}>
            <div className={styles.logo}>
              <img className={styles.paypal} src={paypal} alt="paypal" />
            </div>
            <div className={styles.description}>
              <ul className={styles.list}>
                <li className={styles.listItem}> Accept PayPal and Credit Cards online </li>
                <li className={styles.listItem}> A world trusted brand </li>
                <li className={styles.listItem}> Quickly sign up and connect </li>
                <li className={styles.listItem}> From 2.9% + 30 cents per transaction </li>
              </ul>
            </div>
            <div className={styles.connect}>
              {!owner.payPal ? (
                <a
                  href="https://www.paypal.com/us/webapps/mpp/referral/paypal-business-account"
                  className={styles.link}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <button type="button" className={styles.button}> Connect</button>
                </a>
              )
                : (
                  <div className={styles.connectedDiv}>
                    <img src={check} className={styles.checkmark} alt="check" />
                    <div className={styles.connected}>Connected</div>
                  </div>
                )
            }
            </div>
          </div>
        </div>
      </div>
    );
  }
}
