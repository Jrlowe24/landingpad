import React, { Component } from 'react';
import Input from '../../../Properties/Input/Input';
import styles from './BusinessInfo.css';

export default class BusinessInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      details: props.details || {}
    };
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState(prevState => ({
      details:
                  {
                    ...prevState.details,
                    [name]: value,
                  }
    }), () => {
      const { updateBusinessInfo } = this.props;
      const { details } = this.state;
      updateBusinessInfo('details', details);
    });
  }


  render() {
    const { isLoading, details } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className={styles.container}>
        <div className={styles.nameDiv}>
          <div className={styles.header}>
            Business Info
          </div>
          <div className={styles.description}>
            Your contact info will appear in guest’s booking confirmation email.
          </div>
        </div>
        <div className={styles.inputDiv}>
          <div className={styles.inputGroupBig}>
            <label htmlFor="none" className={styles.lab}> Business Name </label>
            <Input
              type="text"
              name="customInput"
              value={details.businessName}
              handleChange={this.handleInput}
            />
          </div>
          <div className={styles.inputGroupSmall} style={{ float: 'right' }}>
            <label htmlFor="none" className={styles.lab}> Phone Number </label>
            <Input
              type="tel"
              name="customInput"
              value={details.phoneNumber}
              handleChange={this.handleInput}
              pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
            />
          </div>
          <div className={styles.inputGroupBig} style={{ marginTop: '25px' }}>
            <label htmlFor="none" className={styles.lab}> Email Address </label>
            <Input
              type="email"
              value={details.emailAddress}
              name="customInput"
              handleChange={this.handleInput}
            />
          </div>
        </div>
      </div>
    );
  }
}
