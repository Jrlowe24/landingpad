import React, { Component } from 'react';
import Select from '../../../Properties/Input/Select';
import Checkbox from '../../../Properties/AddProperty/AmentiesAndExtras/CheckBox';
import WeekendList from './WeekendList';
import styles from './RegionalSettings.css';
import timezone from './timezone';


const measurementUnits = ['Square Feet', 'Square Meters'];
const primamryLanguages = ['English', 'Spanish'];

export default class RegionalSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      specifics: props.specifics || {}
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleWeekendSelect = this.handleWeekendSelect.bind(this);
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState(prevState => ({
      specifics:
                  {
                    ...prevState.specifics,
                    [name]: value,
                  }
    }), () => {
      const { updateBusinessInfo } = this.props;
      const { specifics } = this.state;
      updateBusinessInfo('specifics', specifics);
    });
  }

  handleWeekendSelect(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    const { specifics } = this.state;
    if (!specifics.weekendDays) {
      this.setState(prevState => ({
        specifics:
          {
            ...prevState.specifics,
            weekendDays: new Map()
          }
      }), () => {
        //   const { businessInfo } = this.state;
        this.setState(prevState => ({
          specifics:
              {
                ...prevState.specifics,
                weekendDays: prevState.specifics.weekendDays.set(item, isChecked),
              }
        }), () => {
          const { updateBusinessInfo } = this.props;
          // eslint-disable-next-line no-shadow
          const { specifics } = this.state;
          updateBusinessInfo('specifics', specifics);
        });
      });
    } else {
      this.setState(prevState => ({
        specifics:
        {
          ...prevState.specifics,
          weekendDays: specifics.weekendDays.set(item, isChecked),
        }
      }), () => {
        const { updateBusinessInfo } = this.props;
        // eslint-disable-next-line no-shadow
        const { specifics } = this.state;
        updateBusinessInfo('specifics', specifics);
      });
    }
  }


  render() {
    const { isLoading, specifics } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className={styles.container}>
        <div className={styles.nameDiv}>
          <div className={styles.header}>
            Regional Info
          </div>
        </div>
        <div className={styles.inputDiv}>
          <div className={styles.inputGroupBig}>
            <label htmlFor="none" className={styles.lab}> Time Zone </label>
            <Select
              options={timezone}
              value={specifics.timeZones}
              handleChange={this.handleInput}
            />
          </div>
          <div className={styles.inputGroupSmall} style={{ marginBottom: '25px' }}>
            <label htmlFor="none" className={styles.lab}> Measurement Units </label>
            <Select
              options={measurementUnits}
              value={specifics.measurementUnits}
              handleChange={this.handleInput}
            />
          </div>
          <div className={styles.inputGroupBig} style={{ height: '67px' }}>
            <label htmlFor="none" className={styles.lab}> Your Weekend Nights Are: </label>
            {
          WeekendList.map(item => (
            <li key={item.key} className={styles.list}>
              <Checkbox name={item.name} checked={specifics.weekendDays ? specifics.weekendDays.get(item.name) : false} onChange={this.handleWeekendSelect} />
              {item.name}
            </li>
          ))
        }
          </div>
          <div className={styles.inputGroupSmall}>
            <label htmlFor="none" className={styles.lab}> Primary Language </label>
            <Select
              title=""
              name="primaryLanguage"
              options={primamryLanguages}
              value={specifics.primamryLanguages}
              handleChange={this.handleInput}
            />
          </div>
        </div>
      </div>
    );
  }
}
