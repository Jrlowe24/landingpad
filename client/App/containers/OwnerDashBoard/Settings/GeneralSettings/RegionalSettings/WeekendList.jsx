const WeekendList = [
  {
    name: 'Thu',
    key: 'thu',
    label: 'Thu',
  },
  {
    name: 'Fri',
    key: 'fri',
    label: 'Fri',
  },
  {
    name: 'Sat',
    key: 'sat',
    label: 'Sat',
  },
  {
    name: 'Sun',
    key: 'sun',
    label: 'sun',
  },
];
    
export default WeekendList;
