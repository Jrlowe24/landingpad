import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import Input from '../../../Properties/Input/Input';
import styles from './Address.css';

// const AnyReactComponent = ({ text }) => <div>{text}</div>;

class Address extends Component {
  static defaultProps = {
    center: {
      lat: 33.776660,
      lng: -84.394301
    },
    zoom: 15
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      address: props.address || {}
    };
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState(prevState => ({
      address:
                  {
                    ...prevState.address,
                    [name]: value,
                  }
    }), () => {
      const { updateBusinessInfo } = this.props;
      const { address } = this.state;
      updateBusinessInfo('address', address);
    });
  }


  render() {
    const { isLoading, address } = this.state;
    const { center, zoom } = this.props;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className={styles.container}>
        <div className={styles.nameDiv}>
          <div className={styles.header}>
            Address
          </div>
        </div>
        <div className={styles.inputDiv}>
          <div className={styles.inputGroupBiggest} style={{ marginBottom: '20px' }}>
            <label htmlFor="none" className={styles.lab}> City, State, Country </label>
            <Input
              type="text"
              value={address.cityStateCountry || ''}
              title=""
              name="customInput"
              placeholder="Enter your business/home city, state, country"
              handleChange={this.handleInput}
            />
          </div>
          <div className={styles.inputGroupBig}>
            <label htmlFor="none" className={styles.lab}> Street Address </label>
            <Input
              type="text"
              value={address.streetAddress || ''}
              title=""
              name="customInput"
              placeholder="Enter your business/home address"
              handleChange={this.handleInput}
            />
          </div>
          <div className={styles.inputGroupSmall}>
            <label htmlFor="none" className={styles.lab}> Zip Code / Postal Code </label>
            <Input
              type="text"
              title=""
              name="customInput"
              value={address.zipCode || ''}
              placeholder="Enter your Business/home zipcode"
              handleChange={this.handleInput}
              pattern="[0-9]{5}"
            />
          </div>
          <div className={styles.map}>
            <GoogleMapReact
              bootstrapURLKeys={{ key: 'AIzaSyCGXQLug5wkJNNL_Jaxj3z_5kJFQoQa8AQ' }}
              defaultCenter={center}
              defaultZoom={zoom}
              options={{}}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Address;
