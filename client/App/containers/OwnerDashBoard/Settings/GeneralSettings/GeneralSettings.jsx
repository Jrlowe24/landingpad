import React, { Component } from 'react';
import BusinessInfo from './BusinessInfo/BusinessInfo';
import Address from './Address/Address';
import RegionalSettings from './RegionalSettings/RegionalSettings';
import styles from './GeneralSettings.css';


export default class GeneralSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      businessInfo: {}
    };
    this.save = this.save.bind(this);
    this.updateBusinessInfo = this.updateBusinessInfo.bind(this);
  }

  save() {
    // patch owner
    console.log(this.state);
  }

  updateBusinessInfo(name, value) {
    this.setState(prevState => ({
      businessInfo:
                  {
                    ...prevState.businessInfo,
                    [name]: value,
                  }
    }), () => {
      console.log(this.state);
    });
  }

  render() {
    const { isLoading, businessInfo } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className={styles.container}>
        <div className={styles.main}>
          <div className={styles.header}> General Settings </div>
          <button className={styles.saveButton} type="submit" onClick={this.save}>Save</button>
          <button className={styles.cancelButton} type="submit" onClick={this.save}>Cancel</button>
          <BusinessInfo details={businessInfo.details} updateBusinessInfo={this.updateBusinessInfo} />
          <Address address={businessInfo.address} updateBusinessInfo={this.updateBusinessInfo} />
          <RegionalSettings specifics={businessInfo.specifics} updateBusinessInfo={this.updateBusinessInfo} />
          <button className={styles.saveButton} type="submit" onClick={this.save}>Save</button>
          <button className={styles.cancelButton} type="submit" onClick={this.save}>Cancel</button>
        </div>
      </div>
    );
  }
}
