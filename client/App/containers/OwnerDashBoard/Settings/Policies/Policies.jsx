import React, { Component } from 'react';
import ReactQuill from 'react-quill';
import './Policies.less';


export default class Policies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      policyInfo: props.policyInfo || {},
      reservationPolicytext: props.policyInfo ? props.policyInfo.reservationPolicy : '',
      cancellationPolicyText: props.policyInfo ? props.policyInfo.cancellationPolicy : ''
    };
    this.save = this.save.bind(this);
    this.handleChangeCancellationPolicy = this.handleChangeCancellationPolicy.bind(this);
    this.handleChangeReservationPolicy = this.handleChangeReservationPolicy.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  save() {
    // patch owner
    console.log(this.state);
  }

  handleChangeReservationPolicy(value) {
    this.setState({ reservationPolicytext: value }, () => {
      const { reservationPolicytext } = this.state;
      this.setState(prevState => ({
        policyInfo:
                    {
                      ...prevState.policyInfo,
                      reservationPolicy: reservationPolicytext,
                    }
      }), () => {
        console.log(this.state);
      });
    });
  }

  handleChangeCancellationPolicy(value) {
    this.setState({ cancellationPolicyText: value }, () => {
      const { cancellationPolicyText } = this.state;
      this.setState(prevState => ({
        policyInfo:
                    {
                      ...prevState.policyInfo,
                      cancellationPolicy: cancellationPolicyText,
                    }
      }), () => {
        console.log(this.state);
      });
    });
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState(prevState => ({
      policyInfo:
                  {
                    ...prevState.policyInfo,
                    [name]: value,
                  }
    }));
  }


  render() {
    const {
      isLoading, reservationPolicytext, cancellationPolicyText, policyInfo
    } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className="container">
        <div className="main">
          <div className="header">
            Policies
          </div>
          <button type="submit" onClick={this.save}>Save</button>
          <div className="policy-box">
            <div className="policy-type">
              <h3>Reservation Policy</h3>
              <p>Your guests will be asked to accept these terms upon checkout.</p>
            </div>
            <div className="policy-content">
              <div>
                <div style={{ marginBottom: '25px' }}>
                  <h4>Title</h4>
                  <input
                    type="text"
                    name="reservationPolicyTitle"
                    maxLength="40"
                    placeholder="Reservation Policy"
                    autoComplete="off"
                    onChange={this.handleInput}
                    value={policyInfo.reservationPolicyTitle || ''}
                  />
                </div>
              </div>

              <div>
                <div>
                  <span>
                    <h4 style={{ marginBottom: '7px' }} translate="settings.policy.LABEL">Enter your reservation policy</h4>
                    <div className="toolTip">
                        ⓘ
                      <div className="toolTip-text">This is where you should add your property’s terms of service to inform customers about payment, check in/out times and any other details they should be aware of before booking.</div>

                    </div>
                  </span>
                  <ReactQuill
                    style={{ height: '50%' }}
                    value={reservationPolicytext}
                    onChange={this.handleChangeReservationPolicy}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="policy-box">
            <div className="policy-type">
              <h3>Cancellation Policy</h3>
              <p>This is your cancellation policy.</p>
            </div>
            <div className="policy-content">
              <div>
                <div style={{ marginBottom: '25px' }}>
                  <h4>Title</h4>
                  <input
                    type="text"
                    name="cancellationPolicyTitle"
                    maxLength="40"
                    placeholder="Cancellation Policy"
                    autoComplete="off"
                    onChange={this.handleInput}
                    value={policyInfo.cancellationPolicyTitle || ''}
                  />
                </div>
              </div>

              <div>
                <div>
                  <span>
                    <h4 style={{ marginBottom: '7px' }} translate="settings.policy.LABEL">Enter your reservation policy</h4>
                    <div className="toolTip">
                        ⓘ
                      <div className="toolTip-text">Add a clear and concise cancellation policy to help potential guests book quickly and confidently.</div>

                    </div>
                  </span>
                  <ReactQuill
                    style={{ height: '50%' }}
                    value={cancellationPolicyText}
                    onChange={this.handleChangeCancellationPolicy}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
