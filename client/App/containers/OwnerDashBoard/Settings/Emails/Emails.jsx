import React, { Component } from 'react';
import Input from '../../Properties/Input/Input';
import styles from './Emails.css';

export default class Emails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      emailInfo: props.emailInfo || {}
    };
    this.save = this.save.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  save() {
    // patch owner
    console.log(this.state);
  }

  handleInput(e) {
    const { value, name } = e.target;
    this.setState(prevState => ({
      emailInfo:
          {
            ...prevState.emailInfo,
            [name]: value,
          }
    }));
  }

  render() {
    const { isLoading, emailInfo } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }

    return (
      <div className={styles.cont}>
        <div className={styles.main}>
          <h1 className={styles.headerPage}>
              Emails
          </h1>
          <button type="submit" className={styles.saveButton} onClick={this.save}>Save</button>
          <button type="submit" className={styles.cancelButton} onClick={this.save}>Cancel</button>
          <div className={styles.container}>
            <div className={styles.nameDiv}>
              <h4 className={styles.header}>
                  Confirmation Email
              </h4>
              <p className={styles.description}>
                  Guests will receive this confirmation email once they book. Feel free to edit and personalize.
              </p>
            </div>
            <div className={styles.inputDiv}>
              <div className={styles.label}>From</div>
              <div className={styles.email}>youremail@gmail.com</div>
              <label htmlFor="none" className={styles.lab}> Subject </label>
              <Input
                type="text"
                value={emailInfo.subject || ''}
                name="subject"
                handleChange={this.handleInput}
              />
              <label htmlFor="none" className={styles.lab} style={{ marginTop: '30px' }}> Intro </label>
              <textarea
                value={emailInfo.intro}
                name="intro"
                style={{ marginBottom: '30px' }}
              />
              <label id="bookingInfo" className={styles.label} style={{ marginBottom: '5px' }} htmlFor="bookingInfo">
                    Booking Details
                <span className={styles.smallLabel}>(not editable)</span>
              </label>
              <div className={styles.bookingDetails}>
                <span className={styles.bookingDetailsText}>Your Booking Details:</span>
                <br />
                <span className={styles.bookingDetailsText}>Guest(s): Guest Name</span>
                <br />
                <span className={styles.bookingDetailsText}>Reservation Number: #000000</span>
                <br />
                <span className={styles.bookingDetailsText}>Room Type: Room Name</span>
                <br />
                <span className={styles.bookingDetailsText}>Check In: Date</span>
                <br />
                <span className={styles.bookingDetailsText}>Check Out: Date</span>
                <br />
                <span className={styles.bookingDetailsText}>Number of Nights: #</span>
                <br />
                <span className={styles.bookingDetailsText}>Party Size: Adults: # | Kids: #</span>
                <br />
                <span className={styles.bookingDetailsText}>Price: ฿</span>
              </div>
              <label htmlFor="none" className={styles.lab}> Closing </label>
              <textarea
                value={emailInfo.closing}
                name="intro"
              />
            </div>
          </div>
          <button type="submit" className={styles.saveButton} onClick={this.save}>Save</button>
          <button type="submit" className={styles.cancelButton} onClick={this.save}>Cancel</button>
        </div>
      </div>
    );
  }
}
