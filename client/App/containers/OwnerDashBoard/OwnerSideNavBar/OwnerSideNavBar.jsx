import React, { Component } from 'react';
import Sidebar from 'react-sidebar';
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom';
import OwnerProperties from '../Properties/OwnerProperties';
import CalendarRoutes from '../Calendar/CalendarRoutes';
import Payment from '../Payment/Payment';
import Reservations from '../Reservations/Reservations';
import PriceOptions from '../PriceOptions/PriceOptions';
import GeneralSettings from '../Settings/GeneralSettings/GeneralSettings';
import Policies from '../Settings/Policies/Policies';
import Emails from '../Settings/Emails/Emails';
import styles from './OwnerSideNavBar.css';
import ExtraChargesRoutes from '../PriceOptions/ExtraCharges/ExtraChargesRoutes';
import CouponRoutes from '../PriceOptions/Coupons/CouponRoutes';

export default class OwnerSideNavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      priceOptionsOpen: false,
      generalSettingsOpen: false
    };
    this.onPriceOptionClicked = this.onPriceOptionClicked.bind(this);
    this.onGeneralSettingsClicked = this.onGeneralSettingsClicked.bind(this);
  }

  onPriceOptionClicked() {
    this.setState(state => {
      return {
        priceOptionsOpen: !state.priceOptionsOpen,
      };
    });
  }

  onGeneralSettingsClicked() {
    this.setState(state => {
      return {
        generalSettingsOpen: !state.generalSettingsOpen,
      };
    });
  }


  render() {
    const { owner } = this.props;
    const { priceOptionsOpen, generalSettingsOpen } = this.state;
    const routes = [
      {
        path: '/',
        exact: true,
      },
      {
        path: '/MyProperties',
        main: props => {
          return (
            <OwnerProperties owner={owner} {...props} />
          );
        }
      },
      {
        path: '/Calender',
        main: props => {
          return (
            <CalendarRoutes owner={owner} {...props} />
          );
        }
      },
      {
        path: '/Payment',
        main: props => {
          return (
            <Payment owner={owner} {...props} />
          );
        }
      },
      {
        path: '/Inquiries',
        main: props => {
          return (
            <Reservations owner={owner} {...props} />
          );
        }
      },
      {
        path: '/PriceOptions',
        main: props => {
          return (
            <PriceOptions owner={owner} {...props} />
          );
        }
      },
      {
        path: '/PriceOptions/ExtraCharges',
        main: props => {
          return (
            <ExtraChargesRoutes owner={owner} {...props} />
          );
        }
      },
      {
        path: '/PriceOptions/Coupons',
        main: props => {
          return (
            <CouponRoutes owner={owner} {...props} />
          );
        }
      },
      {
        path: '/Settings/GeneralSettings',
        main: props => {
          return (
            <GeneralSettings owner={owner} {...props} />
          );
        }
      },
      {
        path: '/Settings/Emails',
        main: props => {
          return (
            <Emails owner={owner} {...props} />
          );
        }
      },
      {
        path: '/Settings/Policies',
        main: props => {
          return (
            <Policies owner={owner} {...props} />
          );
        }
      }
    ];
    return ( // eslint-disable-line
      <Router>
        <div>
          <Sidebar
            contentClassName={styles.content}
            sidebar={(
              <div>
                <div className={styles.user}>
                  {owner.name}
                </div>
                <ul className={styles.list}>
                  <li>
                    <Link to="/">
                      <button type="button" className={styles.tab}>
                        Home
                      </button>
                    </Link>
                  </li>
                  <li>
                    <Link to="/MyProperties">
                      <button type="button" className={styles.tab}>
                            Properties
                      </button>
                    </Link>
                  </li>
                  <li>
                    <Link to="/Calender">
                      <button type="button" className={styles.tab}>
                        Calender
                      </button>
                    </Link>
                  </li>
                  <li>
                    <Link to="/Payment">
                      <button type="button" className={styles.tab}>
                        Payment
                      </button>
                    </Link>
                  </li>
                  <li>
                    <Link to="/Inquiries">
                      <button type="button" className={styles.tab}>
                        Reservations
                      </button>
                    </Link>
                  </li>
                  <li style={{ textAlign: 'left' }}>
                    <Link to="/PriceOptions">
                      <button type="button" className={styles.tab} onClick={this.onPriceOptionClicked}>
                        Price Options
                      </button>
                    </Link>
                  </li>
                  {priceOptionsOpen && (
                  <div>
                    <ul className={styles.subList}>
                      <li>
                        <Link to="/PriceOptions">
                          <button type="button" className={styles.subTab}>
                                Seasonal Pricing
                          </button>
                        </Link>
                      </li>
                      <li>
                        <Link to="/PriceOptions/ExtraCharges">
                          <button type="button" className={styles.subTab}>
                                Additional Fees
                          </button>
                        </Link>
                      </li>
                      <li>
                        <Link to="/PriceOptions/Coupons">
                          <button type="button" className={styles.subTab}>
                                Guest Discounts
                          </button>
                        </Link>
                      </li>
                    </ul>
                  </div>
                  )}
                  <li>
                    <Link to="/Settings/GeneralSettings">
                      <button
                        type="button"
                        className={styles.tab}
                        onClick={this.onGeneralSettingsClicked}
                      >
                        Settings
                      </button>
                    </Link>
                  </li>
                  {generalSettingsOpen && (
                  <div>
                    <ul className={styles.subList}>
                      <li>
                        <Link to="/Settings/GeneralSettings">
                          <button type="button" className={styles.subTab}>
                            Host Info
                          </button>
                        </Link>
                      </li>
                      <li>
                        <Link to="/Settings/Emails">
                          <button type="button" className={styles.subTab}>
                           Confirmation Email
                          </button>
                        </Link>
                      </li>
                      <li>
                        <Link to="/Settings/Policies">
                          <button type="button" className={styles.subTab}>
                            Guest Policies
                          </button>
                        </Link>
                      </li>
                    </ul>
                  </div>
                  )}
                </ul>
              </div>

              )}
            docked
          >
            {routes.map(route => (
              <Route
                key={route.path}
                path={route.path}
                exact={route.exact}
                render={route.main}
              />
            ))}
          </Sidebar>
        </div>
      </Router>
    );
  }
}
