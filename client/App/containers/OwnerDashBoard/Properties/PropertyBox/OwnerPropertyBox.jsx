import React, { Component } from 'react';
// import './PropertyBox.less';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './OwnerPropertyBox.less';
import sampleImage from './sampleImage.jpg';
import deleteLogo from './trashLogo.png';
import editLogo from './editLogo.png';
import duplicateLogo from './duplicateLogo.png';


class OwnerPropertyBox extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onDeleteClick = this.onDeleteClick.bind(this);
  }

  // functionality for the edit button
  async onDeleteClick() {
    const { id } = this.props;
    await fetch(`http://localhost:3000/property/${id}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        console.log(data);
      });
    });
  }


  render() {
    const {
      id, title, deleteProperty, location
    } = this.props;

    return (
      <div className="ownerPropertyContainer">
        <img
          alt="Main pic for Property"
          src={sampleImage}
          className="ownerPropertiesImage"
        />
        <div className="descriptionContainer">
          <h1 className="headerText">
            {title || 'no description'}
          </h1>
        </div>
        <div className="actions">
          <Link to={`${location.pathname}/property/${id}`}>
            <button type="submit" className="actionButtons">
              <img src={editLogo} alt="edit" className="editLogo" />
            </button>
          </Link>
          <button
            type="button"
            className="actionButtons"
          >
            <img src={duplicateLogo} alt="duplicate" className="duplicateLogo" />
          </button>
          <button
            type="button"
            className="actionButtons"
            onClick={() => deleteProperty(id)}
          >
            <img src={deleteLogo} alt="delete" className="deleteLogo" />
          </button>
        </div>
      </div>
    );
  }
}

export default OwnerPropertyBox;

OwnerPropertyBox.Proptype = {
  properties: PropTypes.object.isRequired
};
