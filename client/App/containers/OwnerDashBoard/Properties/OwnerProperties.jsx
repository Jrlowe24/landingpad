import React, { Component } from 'react';
import {
  Route,
  Switch
} from 'react-router-dom';
import AllProperties from './AllProperties/AllProperties';
import AddProperty from './AddProperty/AddProperty';

export default class OwnerProperties extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { match, owner } = this.props;
    return (
      <Switch>
        <Route
          path={`${match.path}/property/:id`}
          render={props => (
            <AddProperty {...props} />
          )}
        />
        <Route
          path={`${match.path}`}
          render={props => (
            <AllProperties owner={owner} {...props} />
          )}
        />
      </Switch>
    );
  }
}
