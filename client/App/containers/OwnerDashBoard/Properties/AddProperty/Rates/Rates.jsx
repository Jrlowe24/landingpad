import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import Select from '../../Input/Select';

const rateTypes = ['Charge per room', 'Charge per person'];
export default class Rates extends Component {
  constructor(props) {
    super(props);
    const { price } = props;
    this.state = {
      price: {
        rateType: price.rateType || 'Charge per Room',
        weekdayPrice: price.weekdayPrice || 0,
        weekendPrice: price.weekendPrice || undefined,
        weeklyPrice: price.weeklyPrice || undefined,
        monthlyPrice: price.monthlyPrice || undefined,
        forGuestsAbove: price.forGuestsAbove || undefined,
        chargeGuestsAbove: price.chargeGuestsAbove || undefined
      }
    };

    this.handleSelectInput = this.handleSelectInput.bind(this);
    this.handleWeekdayPrice = this.handleWeekdayPrice.bind(this);
    this.handleWeekendPrice = this.handleWeekendPrice.bind(this);
    this.handleWeeklyPrice = this.handleWeeklyPrice.bind(this);
    this.handleMonthlyPrice = this.handleMonthlyPrice.bind(this);
    this.handleForGuestsAbove = this.handleForGuestsAbove.bind(this);
    this.handleChargeGuestsAbove = this.handleChargeGuestsAbove.bind(this);
  }

  handleSelectInput(e) {
    const { value } = e.target;
    const { name } = e.target;
    this.setState(prevState => ({
      price:
            { ...prevState.price, [name]: value }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      const { price } = this.state;
      handleSameNameUpdates('price', price);
    });
  }

  handleWeekdayPrice(e) {
    this.setState(prevState => ({
      price:
              { ...prevState.price, weekdayPrice: e.floatValue }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      const { price } = this.state;
      handleSameNameUpdates('price', price);
    });
  }

  handleWeekendPrice(e) {
    this.setState(prevState => ({
      price:
              { ...prevState.price, weekendPrice: e.floatValue }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      const { price } = this.state;
      handleSameNameUpdates('price', price);
    });
  }

  handleWeeklyPrice(e) {
    this.setState(prevState => ({
      price:
              { ...prevState.price, weeklyPrice: e.floatValue }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      const { price } = this.state;
      handleSameNameUpdates('price', price);
    });
  }

  handleMonthlyPrice(e) {
    this.setState(prevState => ({
      price:
              { ...prevState.price, monthlyPrice: e.floatValue }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      const { price } = this.state;
      handleSameNameUpdates('price', price);
    });
  }
  
  handleForGuestsAbove(e) {
    this.setState(prevState => ({
      price:
              { ...prevState.price, forGuestsAbove: e.floatValue }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      const { price } = this.state;
      handleSameNameUpdates('price', price);
    });
  }

  handleChargeGuestsAbove(e) {
    this.setState(prevState => ({
      price:
              { ...prevState.price, chargeGuestsAbove: e.floatValue }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      const { price } = this.state;
      handleSameNameUpdates('price', price);
    });
  }

  render() {
    const { price } = this.state;
    return (
      <div>
        <Select
          title="Rate Types "
          name="rateType"
          options={rateTypes}
          value={price.rateType}
          placeholder="Select House type"
          handleChange={this.handleSelectInput}
        />
        <h3>Weekday Pricing </h3>
        <NumberFormat
          thousandSeparator
          prefix="$"
          defaultValue={price.weekdayPrice}
          onValueChange={this.handleWeekdayPrice}
        />
        <h3>Weekend Pricing </h3>
        <NumberFormat
          thousandSeparator
          prefix="$"
          defaultValue={price.weekendPrice}
          onValueChange={this.handleWeekendPrice}
        />
        <h3>Weekly Pricing </h3>
        <NumberFormat
          thousandSeparator
          prefix="$"
          defaultValue={price.weeklyPrice}
          onValueChange={this.handleWeeklyPrice}
        />
        <h3>Monthly Pricing </h3>
        <NumberFormat
          thousandSeparator
          prefix="$"
          defaultValue={price.monthlyPrice}
          onValueChange={this.handleMonthlyPrice}
        />
        <h3>For each guest above </h3>
        <NumberFormat
          thousandSeparator
          suffix=" guest(S)"
          defaultValue={price.forGuestsAbove}
          onValueChange={this.handleForGuestsAbove}
        />
        <h3>Charge </h3>
        <NumberFormat
          thousandSeparator
          prefix="$"
          defaultValue={price.chargeGuestsAbove}
          onValueChange={this.handleChargeGuestsAbove}
        />
      </div>
    );
  }
}
