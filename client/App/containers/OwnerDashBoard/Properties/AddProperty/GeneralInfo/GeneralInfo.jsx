import React, { Component } from 'react';
import Geosuggest from 'react-geosuggest';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import Input from '../../Input/Input';
import Select from '../../Input/Select';

import './GeneralInfo.less';


export default class GeneralInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      googleLocation: {
        street: '',
        city: '',
        state: '',
        zipcode: '',
        country: '',
        formattedAddress: ''
      },
      coordinates: {
        lat: 0,
        lng: 0
      },
      homeType: props.homeType || 'Individual House',
      rentalOption: props.rentalOption || 'Private Room',
      numGuests: props.numGuests || 0,
      minNights: props.minNights || 0,
      times: {
        checkIn: moment(props.checkIn),
        checkOut: moment(props.checkOut),
      },
      houseOptions: ['Individual House', 'Townhouse', 'Condo', 'Apartment'],
      rentalOptions: ['Entire Place', 'Private Room', 'Shared Room']
    };

    this.handleGoogleLocation = this.handleGoogleLocation.bind(this);
    this.handleSelectInput = this.handleSelectInput.bind(this);
    this.handleMinNights = this.handleMinNights.bind(this);
    this.handleNumberGuest = this.handleNumberGuest.bind(this);
    this.handleCheckIn = this.handleCheckIn.bind(this);
    this.handleCheckOut = this.handleCheckOut.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  handleGoogleLocation(e) {
    const addressComponent = e.gmaps.address_components;
    const selectedLocation = {
      street: `${addressComponent[0].short_name} ${addressComponent[1].long_name}`,
      city: addressComponent[2].long_name,
      state: addressComponent[4].long_name,
      country: addressComponent[5].long_name,
      zipcode: addressComponent[6].long_name,
      formattedAddress: e.gmaps.formatted_address
    };
    this.setState({ googleLocation: selectedLocation, coordinates: e.location }, () => {
      const { handleSameNameUpdates } = this.props;
      const { googleLocation, coordinates } = this.state;
      handleSameNameUpdates('address', googleLocation);
      handleSameNameUpdates('location', coordinates);
    });
  }

  handleSelectInput(e) {
    const { value } = e.target;
    const { name } = e.target;
    this.setState({ [name]: value }, () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates(name, value);
    });
  }

  handleCheckIn(e) {
    // eslint-disable-next-line react/destructuring-assignment
    this.setState(prevState => {
      return {
        times: {
          ...prevState.times, checkIn: e
        }
      };
    }, () => {
      const { handleSameNameUpdates } = this.props;
      const { times } = this.state;
      handleSameNameUpdates('times', times);
    });
  }

  handleCheckOut(e) {
    this.setState(prevState => {
      return {
        times: {
          ...prevState.times, checkOut: e
        }
      };
    }, () => {
      const { handleSameNameUpdates } = this.props;
      const { times } = this.state;
      handleSameNameUpdates('times', times);
    });
  }


  handleMinNights(e) {
    const { value } = e.target;
    this.setState({ minNights: value }, () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('minNights', value);
    });
  }

  handleNumberGuest(e) {
    const { value } = e.target;
    this.setState({ numGuests: value }, () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('numGuests', value);
    });
  }

  // eslint-disable-next-line react/sort-comp
  handleSave() {
    // add api link to save property
    // eslint-disable-next-line react/destructuring-assignment
    console.log(this.state.property);
  }


  render() {
    const {
      homeType, houseOptions, rentalOptions, rentalOption, numGuests, minNights, times
    } = this.state;
    const format = 'h:mm a';
    const { address } = this.props;
    return (
      <div>
        <span> Enter your location</span>
        <Geosuggest
          className="geosuggest"
          htmlFor="Goe Suggest"
          initialValue={address}
          onChange={this.handleLocation}
          onSuggestSelect={this.handleGoogleLocation}
        />
        <Select
          title="House Options"
          name="homeType"
          options={houseOptions}
          value={homeType}
          placeholder="Select House type"
          handleChange={this.handleSelectInput}
        />
        <Select
          title="Rental Options"
          name="rentalOption"
          options={rentalOptions}
          value={rentalOption}
          placeholder="Select rental option"
          handleChange={this.handleSelectInput}
        />

        <Input
          type="number"
          title="Number of Guests"
          name="numGuests"
          value={numGuests}
          placeholder="Enter your max Occupancy"
          handleChange={this.handleNumberGuest}
        />

        <Input
          type="number"
          title="Minimum night stay"
          name=" minNights"
          value={minNights}
          placeholder="Enter your Minimum Nights Required"
          handleChange={this.handleMinNights}
        />
        <span> Check in</span>
        <TimePicker
          value={times.checkIn}
          htmlFor="checkin"
          showSecond={false}
          className="rc-time-picker"
          format={format}
          onChange={this.handleCheckIn}
          use12Hours
          inputReadOnly
        />
        <br />
        <span> Check out</span>
        <TimePicker
          value={times.checkOut}
          htmlFor="checkOut"
          showSecond={false}
          className="rc-time-picker"
          format={format}
          onChange={this.handleCheckOut}
          use12Hours
          inputReadOnly
        />
        <br />
        {/* <button
          type="submit"
          onClick={this.handleSave}
        >
                    Save
        </button> */}
      </div>


    );
  }
}
