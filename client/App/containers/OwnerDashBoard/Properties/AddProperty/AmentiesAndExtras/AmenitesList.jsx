const AmenitiesList = [
  {
    name: 'Wifi',
    key: 'wifi',
    label: 'Wifi',
  },
  {
    name: 'TV',
    key: 'tv',
    label: 'TV',
  },
  {
    name: 'Air Conditioning',
    key: 'ac',
    label: 'Air Conditioning',
  },
  {
    name: 'Oven',
    key: 'oven',
    label: 'Oven',
  },
];
  
export default AmenitiesList;
