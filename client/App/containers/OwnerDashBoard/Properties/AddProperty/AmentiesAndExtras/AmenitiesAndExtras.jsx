import React, { Component } from 'react';
import AmenitiesList from './AmenitesList';
import SafteyList from './SafetyList';
import Checkbox from './CheckBox';
import ExtrasList from './ExtrasList';
import './AmenitiesAndExtras.less';


export default class AmentiesAndExtras extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedAmenites: props.amenities ? new Map(Object.entries(props.amenities)) : new Map(),
      checkedSaftey: new Map(),
      checkedExtras: new Map()
    };
  
    this.handleChangeAmenities = this.handleChangeAmenities.bind(this);
    this.handleChangeSaftey = this.handleChangeSaftey.bind(this);
    this.handleChangeExtras = this.handleChangeExtras.bind(this);
  }

  handleChangeAmenities(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState(prevState => ({ checkedAmenites: prevState.checkedAmenites.set(item, isChecked) }), () => {
      const { handleSameNameUpdates } = this.props;
      const { checkedAmenites } = this.state;
      handleSameNameUpdates('amenities', checkedAmenites);
    });
  }

  handleChangeSaftey(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState(prevState => ({ checkedSaftey: prevState.checkedSaftey.set(item, isChecked) }));
  }

  handleChangeExtras(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState(prevState => ({ checkedExtras: prevState.checkedExtras.set(item, isChecked) }));
  }

  render() {
    const { checkedAmenites } = this.state;

    return (
      <div>
        <div className="itemList">
          <h1> Choose your Amenities</h1>
          <ul>
            {
          AmenitiesList.map(item => (
            <li key={item.key}>
              <label
                htmlFor={item.name}
                id={item.name}
              >
                <Checkbox name={item.name} checked={checkedAmenites.get(item.name)} onChange={this.handleChangeAmenities} />
                {item.name}
              </label>
            </li>
          ))
        }
          </ul>
        </div>
        <div className="itemList">
          <h1> Choose your Safety Options</h1>
          <ul>
            {
          SafteyList.map(item => (
            <li key={item.key}>
              <label
                htmlFor={item.name}
                id={item.name}
              >
                <Checkbox name={item.name} checked={checkedAmenites.get(item.name)} onChange={this.handleChangeAmenities} />
                {item.name}
              </label>
            </li>
          ))
        }
          </ul>
        </div>
        <div className="itemList">
          <h1> Extras</h1>
          <ul>
            {
          ExtrasList.map(item => (
            <li key={item.key}>
              <label
                htmlFor={item.name}
                id={item.name}
              >
                <Checkbox name={item.name} checked={checkedAmenites.get(item.name)} onChange={this.handleChangeAmenities} />
                {item.name}
              </label>
            </li>
          ))
        }
          </ul>
        </div>
      </div>
    );
  }
}
