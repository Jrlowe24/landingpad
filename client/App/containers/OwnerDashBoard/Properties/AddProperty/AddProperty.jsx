import StepZilla from 'react-stepzilla';
import React, { Component } from 'react';
import GeneralInfo from './GeneralInfo/GeneralInfo';
import BedsAndBathrooms from './BedsAndBathrooms/BedsAndBathrooms';
import AmentiesAndExtras from './AmentiesAndExtras/AmenitiesAndExtras';
import PropertyPicture from './PropertyPictures/PropertyPicture';
import AboutProperty from './AboutProperty/AboutProperty';
import AboutStay from './AboutStay/AboutStay';
import Rates from './Rates/Rates';
import FinalPage from './FinalPage/FinalPage';
import './AddProperty.less';

export default class AddProperty extends Component {
  constructor(props) {
    super(props);
    this.state = {
      property: {},
      loading: true
    };
    this.handleSameName = this.handleSameName.bind(this);
    this.onStepChange = this.onStepChange.bind(this);
    this.doneEditing = this.doneEditing.bind(this);
  }

  componentDidMount() {
    // Getting Current Property
    const { match } = this.props;
    fetch(`http://localhost:3000/property/${match.params.id}`).then(response => {
      response.json().then(data => {
        this.setState({ property: data, loading: false });
      });
    });
  }

  // Save on step change
  async onStepChange(step) {
    if (step !== 7) {
      const { match } = this.props;
      const { property } = this.state;
      await fetch(`http://localhost:3000/property/${match.params.id}`, {
        method: 'PATCH',
        body: JSON.stringify(
          property
        ),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
      }).then(response => {
        response.json().then(data => {
          this.setState({ property: data, loading: false });
        });
      });
    }
  }

  // Call backs for updating properties
  handleSameName(name, value) {
    this.setState(prevState => ({
      property:
      { ...prevState.property, [name]: value }
    }), () => {
      console.log(this.state);
    });
  }

  doneEditing() {
    const { history } = this.props;
    history.push('/MyProperties');
  }


  render() {
    const { property, loading } = this.state;
    console.log(property);
    // add loading screen
    if (loading) {
      return <h2>Loading...</h2>;
    }
    const steps = [
      {
        name: 'Step 1',
        component: <GeneralInfo
          numGuests={property.numGuests}
          homeType={property.homeType}
          rentalOption={property.rentalOption}
          minNights={property.minNights}
          checkIn={property.times ? property.times.checkIn : undefined}
          checkOut={property.times ? property.times.checkOut : undefined}
          address={property.address ? property.address.street : ''}
          handleSameNameUpdates={this.handleSameName}
        />
      },
      {
        name: 'Step 2',
        component: <BedsAndBathrooms
          numBedrooms={property.numBedrooms}
          numBathrooms={property.numBathrooms}
          bedroomInfo={property.bedroomInfo}
          handleSameNameUpdates={this.handleSameName}
        />
      },
      {
        name: 'Step 3',
        component: <AmentiesAndExtras
          amenities={property.amenities}
          handleSameNameUpdates={this.handleSameName}
        />
      },
      {
        name: 'Step 4',
        component: <PropertyPicture
          pictures={property.images}
          handleSameNameUpdates={this.handleSameName}
        />
      },
      {
        name: 'Step 5',
        component: <AboutProperty
          title={property.name}
          description={property.description}
          neighborhoodInfo={property.neighborhoodInfo}
          gettingAround={property.gettingAround}
          handleSameNameUpdates={this.handleSameName}
        />
      },
      {
        name: 'Step 6',
        component: <AboutStay
          parking={property.parking}
          accesibility={property.accesibility}
          thingsToKnow={property.thingsToKnow}
          houseRules={property.houseRules}
          handleSameNameUpdates={this.handleSameName}
        />
      },
      {
        name: 'Step 7',
        component: <Rates
          handleSameNameUpdates={this.handleSameName}
          price={property.price ? property.price : {}}
        />
      },
      {
        name: 'Done',
        component: <FinalPage doneEditing={this.doneEditing} />
      }
    ];

    return (
      <div className="step-progress">
        <StepZilla
          steps={steps}
          dontValidate
          onStepChange={this.onStepChange}
          nextTextOnFinalActionStep="Save"
        />
      </div>
    );
  }
}
