import React, { Component } from 'react';
import Input from '../../Input/Input';


export default class AboutStay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      property: {
        parking: props.parking || '',
        accesibility: props.accesibility || '',
        thingsToKnow: props.thingsToKnow || '',
        houseRules: props.houseRules || ''
      }
    };

    this.handleParking = this.handleParking.bind(this);
    this.handleAccesibility = this.handleAccesibility.bind(this);
    this.handleThingsToKnow = this.handleThingsToKnow.bind(this);
    this.handleHouseRules = this.handleHouseRules.bind(this);
  }


  handleParking(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      property:
          { ...prevState.property, parking: value }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('parking', value);
    });
  }

  handleAccesibility(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      property:
          { ...prevState.property, accesibility: value }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('accesibility', value);
    });
  }

  handleThingsToKnow(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      property:
          { ...prevState.property, thingsToKnow: value }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('thingsToKnow', value);
    });
  }

  handleHouseRules(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      property:
          { ...prevState.property, houseRules: value }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('houseRules', value);
    });
  }

  render() {
    const { property } = this.state;
    return (
      <div>
        <Input
          type="text"
          value={property.parking}
          title="Parking: "
          name="parking"
          placeholder="Enter information about parking"
          handleChange={this.handleParking}
        />
        <Input
          type="text"
          value={property.accesibility}
          title="Accesibility: "
          name="accesibility"
          placeholder="Enter your property accesibility"
          handleChange={this.handleAccesibility}
        />
        <h3> Things To Know</h3>
        <textarea
          id="thingsToKnow"
          name="thingsToKnow"
          value={property.thingsToKnow}
          onChange={this.handleThingsToKnow}
        />
        <h3> House Rules</h3>
        <textarea
          id="houseRules"
          name="houseRules"
          value={property.houseRules}
          onChange={this.handleHouseRules}
        />
      </div>


    );
  }
}
