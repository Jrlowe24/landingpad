import React, { Component } from 'react';
import Input from '../../Input/Input';


export default class AboutProperty extends Component {
  constructor(props) {
    super(props);
    this.state = {
      property: {
        title: props.title || '',
        description: props.description || '',
        neighborhoodInfo: props.neighborhoodInfo || '',
        gettingAround: props.gettingAround || ''
      }
    };

    this.handleTitle = this.handleTitle.bind(this);
    this.handleDescription = this.handleDescription.bind(this);
    this.handleNeighborhoodInfo = this.handleNeighborhoodInfo.bind(this);
    this.handleGettingAround = this.handleGettingAround.bind(this);
  }


  handleTitle(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      property:
          { ...prevState.property, title: value }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('name', value);
    });
  }

  handleDescription(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      property:
          { ...prevState.property, description: value }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('description', value);
    });
  }

  handleNeighborhoodInfo(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      property:
          { ...prevState.property, neighborhoodInfo: value }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('neighborhoodInfo', value);
    });
  }

  handleGettingAround(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      property:
          { ...prevState.property, gettingAround: value }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('gettingAround', value);
    });
  }

  render() {
    const { property } = this.state;
    return (
      <div>
        <Input
          type="text"
          value={property.title}
          title="Property Title: "
          name="propertyTitle"
          placeholder="Enter your Property Name"
          handleChange={this.handleTitle}
        />
        <Input
          type="text"
          value={property.description}
          title="Property Description: "
          name="propertyDescription"
          placeholder="Enter your property description"
          handleChange={this.handleDescription}
        />
        <Input
          type="text"
          value={property.neighborhoodInfo}
          title="Neighborhood Info: "
          name="neighborhoodInfo"
          placeholder="Enter information about the neighborhood"
          handleChange={this.handleNeighborhoodInfo}
        />
        <Input
          type="text"
          value={property.gettingAround}
          title="Getting Around: "
          name="gettingAround"
          placeholder="Enter information about transportation"
          handleChange={this.handleGettingAround}
        />
      </div>


    );
  }
}
