import React, { Component } from 'react';
import ImagesUploader from 'react-images-uploader';
import 'react-images-uploader/styles.css';
import 'react-images-uploader/font.css';
import './PropertyPicture.less';

export default class ProppertyPicture extends Component {
  constructor(props) {
    super(props);
    this.state = { pictures: props.pictures || [] };
    this.onDelete = this.onDelete.bind(this);
    this.onLoadend = this.onLoadend.bind(this);
  }

  async onDelete(e) {
    // call to delete image from database and s3 bucket
    const { pictures } = this.state;
    const imageUrl = pictures[e];
    const key = imageUrl.slice(imageUrl.lastIndexOf('/') + 1);
    const id = '1234';
    await fetch(`http://localhost:3000/deleteImage/${id}/${key}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        console.log(data);
      });
    });
  }

  onLoadend(error, res) {
    if (error) {
      console.error(error);
    } else {
      for (let i = 0; i < res.length; i += 1) {
        this.setState(prevState => ({
          pictures: [...prevState.pictures, res[i]]
        }), () => {
          const { handleSameNameUpdates } = this.props;
          const { pictures } = this.state;
          handleSameNameUpdates('images', pictures);
        });
      }
    }
  }


  render() {
    const { pictures } = this.state;
    return (
      <ImagesUploader
        url="http://localhost:3000/multiple-file-upload/1236?nam=123"
        optimisticPreviews
        onLoadEnd={this.onLoadend}
        label="Upload multiple images"
        max={4}
        deleteImage={this.onDelete}
        images={pictures}
      />
    );
  }
}
