import React, { Component } from 'react';
import Input from '../../../Input/Input';


export default class BedRoomInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bedroomInfo: {
        title: (props.bedRoomInfo !== undefined) ? props.bedRoomInfo.title : '',
        numOfKings: (props.bedRoomInfo !== undefined) ? props.bedRoomInfo.numOfKings : 0,
        numOfQueens: (props.bedRoomInfo !== undefined) ? props.bedRoomInfo.numOfQueens : 0,
        numOfFulls: (props.bedRoomInfo !== undefined) ? props.bedRoomInfo.numOfFulls : 0,
        numOfTwins: (props.bedRoomInfo !== undefined) ? props.bedRoomInfo.numOfTwins : 0,
        numOfCouches: (props.bedRoomInfo !== undefined) ? props.bedRoomInfo.numOfCouches : 0,
      },
    };

    this.handleTitle = this.handleTitle.bind(this);
    this.handleKing = this.handleKing.bind(this);
    this.handleQueen = this.handleQueen.bind(this);
    this.handleFull = this.handleFull.bind(this);
    this.handleTwin = this.handleTwin.bind(this);
    this.handleCouches = this.handleCouches.bind(this);
  }

  componentDidMount() {
    const { addBedroom, bedRoomInfo } = this.props;
    if (bedRoomInfo === undefined) {
      const { bedroomInfo } = this.state;
      addBedroom(bedroomInfo);
    }
  }

  handleTitle(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      bedroomInfo:
          { ...prevState.bedroomInfo, title: value }
    }), () => {
      const { updateBedroomInfo, index } = this.props;
      const { bedroomInfo } = this.state;
      updateBedroomInfo(bedroomInfo, index);
    });
  }

  handleKing(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      bedroomInfo:
          { ...prevState.bedroomInfo, numOfKings: parseInt(value, 10) }
    }), () => {
      const { index, updateBedroomInfo } = this.props;
      const { bedroomInfo } = this.state;
      updateBedroomInfo(bedroomInfo, index);
    });
  }

  handleQueen(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      bedroomInfo:
          { ...prevState.bedroomInfo, numOfQueens: parseInt(value, 10) }
    }), () => {
      const { updateBedroomInfo, index } = this.props;
      const { bedroomInfo } = this.state;
      updateBedroomInfo(bedroomInfo, index);
    });
  }

  handleFull(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      bedroomInfo:
          { ...prevState.bedroomInfo, numOfFulls: parseInt(value, 10) }
    }), () => {
      const { updateBedroomInfo, index } = this.props;
      const { bedroomInfo } = this.state;
      updateBedroomInfo(bedroomInfo, index);
    });
  }

  handleTwin(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      bedroomInfo:
          { ...prevState.bedroomInfo, numOfTwins: parseInt(value, 10) }
    }), () => {
      const { updateBedroomInfo, index } = this.props;
      const { bedroomInfo } = this.state;
      updateBedroomInfo(bedroomInfo, index);
    });
  }

  handleCouches(e) {
    const { value } = e.target;
    this.setState(prevState => ({
      bedroomInfo:
          { ...prevState.bedroomInfo, numOfCouches: parseInt(value, 10) }
    }), () => {
      const { updateBedroomInfo, index } = this.props;
      const { bedroomInfo } = this.state;
      updateBedroomInfo(bedroomInfo, index);
    });
  }


  handleSave() {
    // add api link to save property
    // eslint-disable-next-line react/destructuring-assignment
    console.log(this.state.bedroomInfo);
  }

  render() {
    const { index } = this.props;
    const { bedroomInfo } = this.state;
    return (
      <div>
        {`Bedroom ${index + 1} info:`}
        <Input
          type="text"
          title="Name of Bedroom"
          name="numOfBeds"
          value={bedroomInfo.title}
          placeholder="Enter the name of your Bedroom"
          handleChange={this.handleTitle}
        />
        <Input
          type="number"
          title="King Beds"
          name="numOfBeds"
          value={bedroomInfo.numOfKings}
          placeholder="Enter your number of King Beds"
          handleChange={this.handleKing}
        />
        <Input
          type="number"
          title="Queen Beds"
          name="numOfBeds"
          value={bedroomInfo.numOfQueens}
          handleChange={this.handleQueen}
          placeholder="Enter your number of Queen Beds"
        />
        <Input
          type="number"
          title="Twin Beds"
          name="numOfBeds"
          value={bedroomInfo.numOfTwins}
          handleChange={this.handleTwin}
          placeholder="Enter your number of Twin Beds"
        />
        <Input
          type="number"
          title="Full Beds"
          name="numOfBeds"
          value={bedroomInfo.numOfFulls}
          handleChange={this.handleFull}
          placeholder="Enter your number of Full Beds"
        />
        <Input
          type="number"
          title="Couch"
          name="numOfBeds"
          value={bedroomInfo.numOfCouches}
          handleChange={this.handleCouches}
          placeholder="Enter your number of Couches"
        />
      </div>


    );
  }
}
