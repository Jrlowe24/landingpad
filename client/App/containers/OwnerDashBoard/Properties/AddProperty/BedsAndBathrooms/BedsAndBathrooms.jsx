import React, { Component } from 'react';
import Input from '../../Input/Input';
import BedRoomInfo from './BedRoomInfo/BedRoomInfo';


export default class BedsAndBathrooms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      property: {
        numBedrooms: props.numBedrooms || 0,
        numBathrooms: props.numBathrooms || 0,
        bedroomInfo: props.bedroomInfo || [],
      },


    };

    this.handleNum = this.handleNum.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleBedroomInfo = this.handleBedroomInfo.bind(this);
    this.updateBedroomInfo = this.updateBedroomInfo.bind(this);
    this.handleNumOfBeds = this.handleNumOfBeds.bind(this);
  }


  handleNum(e) {
    const { value, name } = e.target;
    this.setState(prevState => ({
      property:
                { ...prevState.property, [name]: name === 'numBedrooms' ? parseInt(value, 10) : parseFloat(value) }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('numBathrooms', parseFloat(value));
    });
  }

  handleNumOfBeds(e) {
    const { value } = e.target;
    const { property } = this.state;
    const lastIdxBedInfo = property.numBedrooms - 1;
    if (property.numBedrooms < parseInt(value, 10)) {
      this.setState(prevState => ({
        property:
                  { ...prevState.property, numBedrooms: parseInt(value, 10) }
      }), () => {
        const { handleSameNameUpdates } = this.props;
        handleSameNameUpdates('numBedrooms', parseInt(value, 10));
      });
    } else {
      this.setState(prevState => ({
        property:
                  {
                    ...prevState.property,
                    numBedrooms: parseInt(value, 10),
                    bedroomInfo: prevState.property.bedroomInfo.splice(lastIdxBedInfo, 1)
                  }
      }), () => {
        const { handleSameNameUpdates } = this.props;
        handleSameNameUpdates('numBedrooms', parseInt(value, 10));
      });
    }
  }

  handleBedroomInfo(bedroom) {
    this.setState(prevState => ({
      property:
                { ...prevState.property, bedroomInfo: [...prevState.property.bedroomInfo, bedroom] }
    }), () => {
      const { handleSameNameUpdates } = this.props;
      const { property } = this.state;
      handleSameNameUpdates('bedroomInfo', property.bedroomInfo);
    });
  }

  updateBedroomInfo(bedroom, index) {
    const { property } = this.state;
    const { bedroomInfo } = property;
    bedroomInfo[index] = bedroom;
    this.setState(property, () => {
      const { handleSameNameUpdates } = this.props;
      handleSameNameUpdates('bedroomInfo', property.bedroomInfo);
    });
  }

  handleSave() {
    // add api link to save property
    // eslint-disable-next-line react/destructuring-assignment
    console.log(this.state.property);
  }

  render() {
    const { property } = this.state;
    const bedRooms = [];
    for (let i = 0; i < property.numBedrooms; i += 1) {
      bedRooms.push(
        <div key={i}>
          <BedRoomInfo
            updateBedroomInfo={this.updateBedroomInfo}
            index={i}
            addBedroom={this.handleBedroomInfo}
            bedRoomInfo={property.bedroomInfo[i]}
          />
        </div>
      );
    }
    return (
      <div>

        <Input
          type="number"
          title="Bedrooms"
          name="numBedrooms"
          value={property.numBedrooms}
          placeholder="Enter your number of Bedrooms"
          handleChange={this.handleNumOfBeds}
        />

        <Input
          type="number"
          title="Bathrooms"
          name="numBathrooms"
          value={property.numBathrooms}
          placeholder="Enter your number of Bathrooms"
          handleChange={this.handleNum}
        />
        <div>
          {bedRooms}
        </div>
        <button
          type="submit"
          onClick={this.handleSave}
        >
                    Save
        </button>
      </div>


    );
  }
}
