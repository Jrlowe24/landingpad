/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';

import './Modal.less';

const modal = props => {
  const {
    show, close, next, children
  } = props;
  return (
    <div>
      <div
        className="modal-wrapper"
        style={{
          transform: show ? 'translateY(0vh)' : 'translateY(-100vh)',
          opacity: show ? '1' : '0'
        }}
      >
        <div className="modal-header">
          <h3>Modal Header</h3>
          <span className="close-modal-btn" onClick={close} onKeyDown={close}>×</span>
        </div>
        <div className="modal-body">
          {children}
        </div>
        <div className="modal-footer">
          <button className="btn-cancel" onClick={close} type="submit">CLOSE</button>
          <button className="btn-continue" type="submit" onClick={next}>CONTINUE</button>
        </div>
      </div>
    </div>
  );
};

export default modal;
