import React from 'react';

const Select = props => {
  const {
    name, title, value, handleChange, placeholder, options
  } = props;
  return (
    <div className="form-group">
      {/* eslint-disable-next-line jsx-a11y/label-has-for */}
      <label htmlFor={name}>
        {' '}
        {title}
        {' '}
      </label>
      <select
        name={name}
        value={value || placeholder}
        onChange={handleChange}
      >
        <option value="">{placeholder}</option>
        {options.map(option => {
          return (
            <option
              key={option}
              value={option}
              label={option}
            >
              {option}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default Select;
