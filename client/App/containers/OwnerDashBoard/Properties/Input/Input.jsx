import React from 'react';
import styles from './Input.css';

const Input = props => {
  const {
    type, title, name, value, placeHolder, handleChange, pattern, required
  } = props;
  return (
    <div>
      <label htmlFor={name} id={name}>
        <div className={styles.labelDiv}>
          {required ? `*${title}` : title}
        </div>
        <div>
          <input
            id={name}
            name={name}
            type={type}
            value={value}
            onChange={handleChange}
            placeholder={placeHolder}
            pattern={pattern}
            min={0}
          />
        </div>
      </label>
    </div>
  );
};

export default Input;
