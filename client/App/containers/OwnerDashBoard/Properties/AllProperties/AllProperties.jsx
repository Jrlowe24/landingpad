/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Component } from 'react';
import OwnerPropertyBox from '../PropertyBox/OwnerPropertyBox';
import Modal from '../WebScrapperModal/WebScrapperModal';
import Input from '../Input/Input';
// import Listing from '../../../Home/components/Listings/Listing/Listing';
import styles from './AllProperties.css';

export default class AllProperties extends Component {
  constructor(props) {
    super(props);
    this.state = {
      properties: [],
      // newPropertyId: '',
      isShowing: false,
      airbnbUrl: ''
    };
    this.updateProperties = this.updateProperties.bind(this);
    this.deleteSelectedProperty = this.deleteSelectedProperty.bind(this);
    this.onAddPropertyClicked = this.onAddPropertyClicked.bind(this);
    this.handleAirbnbUrl = this.handleAirbnbUrl.bind(this);
  }

  componentWillMount() {
    const { owner } = this.props;
    // Getting Current Properties
    fetch(`http://localhost:3000/properties/${owner._id}`).then(response => {
      response.json().then(data => {
        this.setState({ properties: data });
      });
    });
  }

  // creating new property
  async onAddPropertyClicked() {
    const { owner } = this.props;
    await fetch(`http://localhost:3000/property/${owner._id}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        const { location, history } = this.props;
        history.push(`${location.pathname}/property/${data}`);
      });
    });
  }

  // eslint-disable-next-line react/sort-comp
  async deleteSelectedProperty(id) {
    await fetch(`http://localhost:3000/property/${id}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        console.log(data);
      });
      this.updateProperties();
    });
  }

  updateProperties() {
    const { owner } = this.props;
    fetch(`http://localhost:3000/properties/${owner._id}`).then(response => {
      response.json().then(data => {
        this.setState({ properties: data });
      });
    });
  }

  handleAirbnbUrl(e) {
    const { value } = e.target;
    this.setState({ airbnbUrl: value });
  }


  openModalHandler = () => {
    this.setState({
      isShowing: true
    });
  };

closeModalHandler = () => {
  this.setState({
    isShowing: false
  });
};

continueModalHandler = async () => {
  const { airbnbUrl } = this.state;
  const { owner } = this.props;
  const url = encodeURIComponent(airbnbUrl);
  await fetch(`http://localhost:3000/property/${owner._id}/${url}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
  }).then(response => {
    response.json().then(data => {
      const { location, history } = this.props;
      history.push(`${location.pathname}/property/${data}`);
    });
  });
};


render() {
  const { properties, isShowing, airbnbUrl } = this.state;
  const { location } = this.props;
  return (
    <div className={styles.container}>
      <div className={styles.main}>
        { isShowing ? <div onClick={this.closeModalHandler} className="back-drop" onKeyDown={this.closeModalHandler} /> : (
          <div>
            <div className={styles.header}> Properties</div>
            <button type="submit" className={styles.addProp} onClick={this.onAddPropertyClicked}>+ Create New </button>
            {properties.map(property => (
              <OwnerPropertyBox
                key={property._id}
                id={property._id}
                address={property.address}
                title={property.name}
                image={property.images[0]}
                deleteProperty={this.deleteSelectedProperty}
                location={location}
              />
            ))}
          </div>
        ) }

        {/* <button className="open-modal-btn" onClick={this.openModalHandler} type="submit">Migrate using Airbnb</button> */}

        <Modal
          className="modal"
          show={isShowing}
          close={this.closeModalHandler}
          next={this.continueModalHandler}
        >
          <Input
            type="text"
            value={airbnbUrl}
            title="Property Title: "
            name="propertyTitle"
            placeholder="Enter your Property Name"
            handleChange={this.handleAirbnbUrl}
          />
        </Modal>
      </div>
    </div>
  );
}
}
