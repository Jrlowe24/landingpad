import React, { Component } from 'react';
import styles from './Login.css';
import logo from './LPvNewColor.png';
import passwordLogo from './lockIcon.png';
import emailLogo from './emailicon.png';

const firebase = require('firebase');

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };
    this.login = this.login.bind(this);
  }

  login = () => {
    const { email, password } = this.state;
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => {
        console.log(`logged in user ${user}`);
      }).catch(e => {
        console.log(e);
      });
  };

  passwordReset = () => {

  };

  setEmail = e => {
    this.setState({
      email: e.target.value
    });
  };

  setPassword = e => {
    this.setState({
      password: e.target.value
    });
  };


  render() {
    const { email, password } = this.state;
    return (
      <div className={styles.main}>
        <div className={styles.card}>
          <img src={logo} className={styles.logo} alt="logo" />
          <div className={styles.emailDiv}>
            <img src={emailLogo} className={styles.emailLogo} alt="password" />
            <input className={styles.email} type="email" name="email" onChange={this.setEmail} value={email} />
          </div>
          <div className={styles.emailDiv}>
            <img src={passwordLogo} className={styles.passwordLogo} alt="password" />
            <input className={styles.email} type="password" name="password" onChange={this.setPassword} value={password} />
          </div>
          <div className={styles.rememberDiv}>
            <input className={styles.check} type="checkbox" name="remember" value="" />
            <div className={styles.rememberMe}> Remember me </div>
          </div>
          <button className={styles.button} type="submit" onClick={this.login}> Log in </button>
          <button className={styles.forgot} type="submit" onClick={this.passwordReset}> Forgot Password? </button>
        </div>
      </div>
    );
  }
}
