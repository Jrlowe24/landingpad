
import React, { Component } from 'react';
import {
  Route,
  Switch
} from 'react-router-dom';
import PropertyCalendar from './PropertyCalender';
import AddEvent from './AddEvent/AddEvent';


export default class CalendarRoutes extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const { match, owner } = this.props;
    return (
      <Switch>
        <Route
          path={`${match.path}/booking/:id`}
          render={props => (
            <AddEvent owner={owner} {...props} />
          )}
        />
        <Route
          path={`${match.path}`}
          render={props => (
            <PropertyCalendar owner={owner} {...props} />
          )}
        />
      </Switch>
    );
  }
}
