/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Component } from 'react';
import Select from 'react-select';
import Popover from 'react-tiny-popover';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import './PropertyCalender.less';
import './bigCal.less';
import moment from 'moment';
import Button from '../../../components/Button/Button';
import withDragDropContext from './withDndContext';
import styles from './PropertyCalendar.css';


const localizer = momentLocalizer(moment);
const selectStyles = {
  control: provided => ({ ...provided, minWidth: 240, margin: 8 }),
  menu: () => ({ boxShadow: 'inset 0 1px 0 rgba(0, 0, 0, 0.1)' }),
};

// styled components

const Menu = props => {
  const shadow = 'hsla(218, 50%, 10%, 0.1)';
  return (
    <div
      css={{
        backgroundColor: 'white',
        borderRadius: 4,
        boxShadow: `0 0 0 1px ${shadow}, 0 4px 11px ${shadow}`,
        marginTop: 8,
        position: 'absolute',
        zIndex: 2,
      }}
      {...props}
    />
  );
};
const Blanket = props => (
  <div
    css={{
      bottom: 0,
      left: 0,
      top: 0,
      right: 0,
      position: 'fixed',
      zIndex: 1,
    }}
    {...props}
  />
);
const Dropdown = ({
  children, isOpen, target, onClose
}) => (
  <div css={{ position: 'relative' }}>
    {target}
    {isOpen ? <Menu>{children}</Menu> : null}
    {isOpen ? <Blanket onClick={onClose} /> : null}
  </div>
);
const Svg = p => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    focusable="false"
    role="presentation"
    {...p}
  />
);
const DropdownIndicator = () => (
  <div css={{ color: '#008000', height: 24, width: 32 }}>
    <Svg>
      <path
        d="M16.436 15.085l3.94 4.01a1 1 0 0 1-1.425 1.402l-3.938-4.006a7.5 7.5 0 1 1 1.423-1.406zM10.5 16a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11z"
        fill="currentColor"
        fillRule="evenodd"
      />
    </Svg>
  </div>
);

class PropertyCalender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentProperty: '',
      currentPropertyReservations: [],
      propertyOptions: [],
      isOpen: false,
      value: undefined,
      isPopoverOpen: false,
      currentEvent: {}
    };
    this.generateICal = this.generateICal.bind(this);
    this.getPropertyReservations = this.getPropertyReservations.bind(this);
    this.onPropertySelect = this.onPropertySelect.bind(this);
    this.renderPopover = this.renderPopover.bind(this);
    this.createNewEvent = this.createNewEvent.bind(this);
  }

  componentDidMount() {
    const { owner } = this.props;
    // get owner properties
    fetch(`http://localhost:3000/properties/${owner._id}`).then(response => {
      response.json().then(data => {
        const propertyOptions = [];
        data.forEach(d => {
          propertyOptions.push({ label: d.name, value: d._id });
        });
        this.setState({ propertyOptions, currentProperty: propertyOptions[0] }, () => {
          this.getPropertyReservations();
        });
      });
    });
  }

  onPropertySelect(selectedProperty) {
    this.setState({ currentProperty: selectedProperty, isOpen: false }, () => {
      this.getPropertyReservations();
    });
  }

  getPropertyReservations() {
    const { currentProperty } = this.state;
    const reservations = [];

    // feteching external bookings
    let id = 0;
    fetch(`http://localhost:3000/externalBookings/property/${currentProperty.value}`).then(bookingResponse => {
      bookingResponse.json().then(bookingData => {
        bookingData.forEach(dataSource => {
          dataSource.forEach(externalReservation => {
            reservations.push({
              id: id += 1,
              start: moment(externalReservation.startDate).toDate(),
              end: moment(externalReservation.endDate).toDate(),
              resourceId: externalReservation._id,
              title: externalReservation.source,
              color: '#fd5c63',
              type: 'external'
            });
          });
        });
      });
    });
    // get current property landingPad bookings
    fetch(`http://localhost:3000/bookings/property/${currentProperty.value}`).then(bookingResponse => {
      bookingResponse.json().then(bookingData => {
        bookingData.forEach(element => {
          if (element.dates) {
            reservations.push({
              id: element._id,
              start: new Date(element.dates.starting),
              end: new Date(element.dates.ending),
              resourceId: element.property,
              title: element.bookerinfo ? element.bookerInfo.name : 'Blocked',
              color: element.confirmed ? '#008000' : '#FF0000',
              type: element.type || 'reservation',
              bookerInfo: element.bookerInfo
            });
          }
        });
        this.setState({ currentPropertyReservations: reservations });
      });
    });
  }

  toggleOpen = () => {
    this.setState(state => ({ isOpen: !state.isOpen }));
  };

  onSelectChange = value => {
    this.toggleOpen();
    this.setState({ value });
  };

  generateICal() {
    console.log(this.state);
    // const { owner } = this.props;
    const { currentProperty } = this.state;
    fetch(`http://localhost:3000/calendar/ical/${currentProperty.value}`).then(data => {
      alert(`Your ical link: ${data.url}`);
    }).catch(err => {
      console.log(err);
    });
  }

  async createNewEvent() {
    const { owner } = this.props;
    const { currentProperty } = this.state;
    await fetch('http://localhost:3000/booking', {
      method: 'POST',
      body: JSON.stringify(
        {
          owner: owner._id,
          property: currentProperty.value
        }
      ),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        const { location, history } = this.props;
        history.push(`${location.pathname}/booking/${data}`);
      });
    });
  }

  renderPopover(event) {
    if (event.type !== 'external') {
      const { location, history } = this.props;
      history.push(`${location.pathname}/booking/${event.id}`);
    }
    const { isPopoverOpen } = this.state;
    this.setState({ currentEvent: event }, () => {
      this.setState({ isPopoverOpen: !isPopoverOpen });
    });
  }


  render() {
    const {
      currentProperty, propertyOptions, currentPropertyReservations, isOpen, isPopoverOpen, currentEvent
    } = this.state;
    return (
      <div className={styles.bodyContainer}>
        <div className={styles.main}>
          <div>
            <h1>Reservation Calendar</h1>
            <Button text="+ New Event" className={styles.reservationsButton} onClick={this.createNewEvent} />
          </div>
          <div className={styles.adjacent}>
            <Dropdown
              isOpen={isOpen}
              onClose={this.toggleOpen}
              target={(
                <span
                  onClick={this.toggleOpen}
                  onKeyPress={this.handleKeyPress}
                  className={styles.dropdownButton}
                >
                  {currentProperty.label}
                  <span className={styles.chevron} />
                </span>
            )}
            >
              <Select
                autoFocus
                value={currentProperty}
                onChange={this.onPropertySelect}
                components={{ DropdownIndicator, IndicatorSeparator: null }}
                options={propertyOptions}
                isSearchable
                menuIsOpen
                isClearable={false}
                tabSelectsValue={false}
                styles={selectStyles}
                onSelectEvent={this.renderPopover}
              />
            </Dropdown>

            <Button onClick={this.generateICal} className={styles.iCalButton} text="Export Calendar" noOutline />
          </div>
          <div className={styles.calendarView}>
            <Popover
              isOpen={isPopoverOpen}
              position={['top', 'right']}
              padding={10}
              onClickOutside={() => this.setState({ isPopoverOpen: false })}
              content={() => (
                <div
                  className={styles.eventPopover}
                  onClick={() => {
                    this.setState({ isPopoverOpen: !isPopoverOpen });
                  }}
                >
                  <p>
                    {`${currentEvent.title} is from an external source`}
                  </p>
                </div>
              )}
            >
              <Calendar
                localizer={localizer}
                selectable
                events={currentPropertyReservations}
                startAccessor="start"
                endAccessor="end"
                step={60}
                defaultView={Views.MONTH}
                views={['month']}
                showMultiDayTimes
                eventPropGetter={event => {
                  if (event.color) {
                    return { style: { backgroundColor: event.color } };
                  }
                  return {};
                }}
                onSelectEvent={this.renderPopover}
              />
            </Popover>
          </div>
        </div>
      </div>
    );
  }
}
export default withDragDropContext(PropertyCalender);
