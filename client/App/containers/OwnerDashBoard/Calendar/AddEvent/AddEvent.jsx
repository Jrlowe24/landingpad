import React, { Component } from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { formatDate, parseDate } from 'react-day-picker/moment';
import {
  Link,
} from 'react-router-dom';
import moment from 'moment';
import MultiSelect from 'react-select';
import Select from '../../Properties/Input/Select';
import Input from '../../Properties/Input/Input';
import styles from './AddEvent.css';
import PriceHelper from '../../../../Helpers/PriceHelper';
import Button from '../../../../components/Button/Button';


const bookingType = ['Reservation', 'Set as Not Available', 'Set as Not Available on Airbnb', 'Checked In', 'Checked Out', 'Cancelled'];

class AddEvent extends Component {
  static getPrice(currentPropertyExtraCharges, currentCharge) {
    for (let i = 0; i < currentPropertyExtraCharges.length; i += 1) {
      if (currentPropertyExtraCharges[i]._id === currentCharge) {
        return currentPropertyExtraCharges[i].charge;
      }
    }
    return 0;
  }

  constructor(props) {
    super(props);
    this.state = {
      bookingInfo: {},
      isLoading: true,
      currentPropertyExtraCharges: [],
      extraChargeOptions: [],
      currentProperty: {},
      isNew: false
    };

    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleMessageText = this.handleMessageText.bind(this);
    this.onChargeSelect = this.onChargeSelect.bind(this);
    this.deleteChargeFromMap = this.deleteChargeFromMap.bind(this);
    this.addChargeToMap = this.addChargeToMap.bind(this);
    this.handleQuantity = this.handleQuantity.bind(this);
    this.updatePrice = this.updatePrice.bind(this);
    this.save = this.save.bind(this);
    this.getNumOfDays = this.getNumOfDays.bind(this);
    this.cancel = this.cancel.bind(this);
  }

  componentWillMount() {
    // get current booking
    const { match } = this.props;
    fetch(`http://localhost:3000/booking/${match.params.id}`).then(response => {
      response.json().then(data => {
        const dataWithFormattedDates = data;
        if (data.dates) {
          dataWithFormattedDates.dates.starting = data.dates.starting ? new Date(data.dates.starting) : undefined;
          dataWithFormattedDates.dates.ending = data.dates.ending ? new Date(data.dates.ending) : undefined;
          dataWithFormattedDates.chargesInfoMap = data.chargesInfoMap ? new Map(Object.entries(data.chargesInfoMap)) : undefined;
        }
        this.setState({ bookingInfo: dataWithFormattedDates, isNew: data.dates === undefined }, () => {
          fetch(`http://localhost:3000/charge/prop/${data.property}`).then(response1 => {
            response1.json().then(extraCharges => {
              const extraChargeOptions = [];
              extraCharges.forEach(d => {
                extraChargeOptions.push({ label: d.name, value: d._id });
              });
              this.setState({ extraChargeOptions });
              this.setState({ currentPropertyExtraCharges: extraCharges });
            });
          });
          fetch(`http://localhost:3000/property/${data.property}`).then(response1 => {
            response1.json().then(currentProperty => {
              this.setState({ currentProperty, isLoading: false });
            });
          });
        });
      });
    });
    // get extraChrages
  }

  onChargeSelect(selectedCharges) {
    const { bookingInfo } = this.state;
    const prevSelectedCharges = bookingInfo.selectedCharges || [];
    const selectedChargesCheck = selectedCharges || [];
    if (selectedChargesCheck.length < prevSelectedCharges.length) {
      this.deleteChargeFromMap(selectedCharges);
    } else {
      this.addChargeToMap(selectedCharges[selectedCharges.length - 1].value);
    }
    this.setState(prevState => ({
      bookingInfo:
                {
                  ...prevState.bookingInfo,
                  selectedCharges: selectedChargesCheck,
                }
    }), () => {
      this.updatePrice();
    });
  }

  getNumOfDays() {
    const { bookingInfo } = this.state;
    const {
      dates
    } = bookingInfo;
    console.log(bookingInfo);
    let { starting, ending } = dates;
    if (!starting) {
      starting = new Date();
    }
    if (!ending) {
      ending = new Date();
    }
    const differenceInTime = ending.getTime() - starting.getTime();

    // To calculate the no. of days between two dates
    const numOfDays = Math.round((differenceInTime / (1000 * 3600 * 24)));
    // console.log(`numOfDays:${numOfDays}`);
    return numOfDays;
  }

  deleteChargeFromMap(selectedCharges) {
    const { bookingInfo } = this.state;
    if (!selectedCharges) {
      const { chargesInfoMap } = bookingInfo;
      chargesInfoMap.clear();
      this.setState(prevState => ({
        bookingInfo:
                {
                  ...prevState.bookingInfo,
                  chargesInfoMap,
                }
      }));
    } else {
      const res = bookingInfo.selectedCharges.filter(item1 => !selectedCharges.some(item2 => (item2.value === item1.value)));
      res.forEach(chargeToDelete => {
        const { chargesInfoMap } = bookingInfo;
        chargesInfoMap.delete(chargeToDelete.value);
        this.setState(prevState => ({
          bookingInfo:
                {
                  ...prevState.bookingInfo,
                  chargesInfoMap,
                }
        }));
      });
    }
  }

  addChargeToMap(selectedCharge) {
    const { bookingInfo, currentPropertyExtraCharges } = this.state;
    let { chargesInfoMap } = bookingInfo;
    if (chargesInfoMap === undefined) {
      chargesInfoMap = new Map();
    }
    const chargeInfo = {
      quantity: 1,
      price: AddEvent.getPrice(currentPropertyExtraCharges, selectedCharge)
    };
    chargesInfoMap.set(selectedCharge, chargeInfo);
    this.setState(prevState => ({
      bookingInfo:
                {
                  ...prevState.bookingInfo,
                  chargesInfoMap,
                }
    }));
  }

  showFromMonth() {
    const { bookingInfo } = this.state;
    const { dates } = bookingInfo;
    const { starting, ending } = dates;
    console.log(starting);
    if (!starting) {
      return;
    }
    if (moment(ending).diff(moment(starting), 'months') < 1) {
      this.endDate.getDayPicker().showMonth(starting);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    const { bookingInfo } = this.state;
    let { dates } = bookingInfo;
    if (!dates) {
      dates = {};
    }
    dates.starting = from;
    this.setState(prevState => ({
      bookingInfo:
                {
                  ...prevState.bookingInfo,
                  dates,
                }
    }), () => {
      this.updatePrice();
    });
  }

  handleToChange(to) {
    const { bookingInfo } = this.state;
    const { dates } = bookingInfo;
    dates.ending = to;
    this.setState(prevState => ({
      bookingInfo:
        {
          ...prevState.bookingInfo,
          dates,
        }
    }), () => {
      this.updatePrice();
      this.showFromMonth();
    });
  }

  handleInput(e) {
    const { value, name } = e.target;
    if (name === 'numOfGuests' || name === 'type') {
      this.setState(prevState => ({
        bookingInfo:
                  {
                    ...prevState.bookingInfo,
                    [name]: value,
                  }
      }));
    } else {
      const { bookingInfo } = this.state;
      let { bookerInfo } = bookingInfo;
      if (!bookerInfo) {
        bookerInfo = {};
      }
      const tempBookerInfo = bookerInfo;
      tempBookerInfo[name] = value;
      this.setState(prevState => ({
        bookingInfo:
                      {
                        ...prevState.bookingInfo,
                        bookerInfo: tempBookerInfo,
                      }
      }), () => {
        console.log(this.state);
      });
    }
  }

  handleQuantity(e) {
    const { value, name } = e.target;
    const { bookingInfo } = this.state;
    const { chargesInfoMap } = bookingInfo;
    chargesInfoMap.get(name).quantity = value;
    this.setState(prevState => ({
      bookingInfo:
                  {
                    ...prevState.bookingInfo,
                    chargesInfoMap,
                  }
    }));
  }

  handleMessageText(e) {
    const { value } = e.target;
    const { bookingInfo } = this.state;
    let { bookerInfo } = bookingInfo;
    if (!bookerInfo) {
      bookerInfo = {};
    }
    const tempBookerInfo = bookerInfo;
    tempBookerInfo.message = value;
    this.setState(prevState => ({
      bookingInfo:
                      {
                        ...prevState.bookingInfo,
                        bookerInfo: tempBookerInfo,
                      }
    }));
  }

  updatePrice() {
    let total = 0;
    const { bookingInfo, currentProperty } = this.state;
    const { price } = currentProperty;
    const {
      dates, numOfGuests, chargesInfoMap
    } = bookingInfo;
    const { starting, ending } = dates;
    total += PriceHelper(price, starting, ending);
    if (chargesInfoMap) {
      chargesInfoMap.forEach(charge => {
        if (charge.perNumOfGuests) {
          total += charge.price * numOfGuests;
        } else {
          total += charge.price * charge.quantity;
        }
      });
    }
    this.setState(prevState => ({
      bookingInfo:
                        {
                          ...prevState.bookingInfo,
                          price: total,
                        }
    }));
  }

  async save() {
    const { match } = this.props;
    const { bookingInfo } = this.state;
    await fetch(`http://localhost:3000/booking/${match.params.id}`, {
      method: 'PATCH',
      body: JSON.stringify(
        bookingInfo
      ),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    });
  }

  async cancel() {
    const { isNew } = this.state;
    if (isNew) {
      const { match } = this.props;
      await fetch(`http://localhost:3000/booking/${match.params.id}`, {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
      });
    }
  }

  render() {
    const {
      bookingInfo, isLoading, currentProperty, extraChargeOptions
    } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }
    const {
      type, numOfGuests, selectedCharges, chargesInfoMap
    } = bookingInfo;

    let { dates, bookerInfo } = bookingInfo;
    if (!dates) {
      dates = {};
    }
    
    const { starting, ending } = dates;

    if (!bookerInfo) {
      bookerInfo = {};
    }
    const {
      name, phoneNumber, email, message
    } = bookerInfo;
    const { price } = currentProperty;
    const modifiers = { start: starting, end: ending };
    return (
      <div className={styles.bodyContainer}>
        <div>
          <h1>Create a New Event</h1>
          <div className={styles.buttons}>
            <Link to="/Calender">
              <Button cancel text="Cancel" onClick={this.cancel} />
            </Link>
            <Link to="/Calender">
              <Button onClick={this.save} text="Save" />
            </Link>
          </div>
        </div>
        <div className={styles.eventContainer}>
          <h4 className={styles.subTitle}>Type of Event</h4>
          <div className={styles.contentSide}>
            <Select
              name="type"
              options={bookingType}
              value={type}
              defaultValue="Set as Not Available"
              placeholder="Select event type"
              handleChange={this.handleInput}
            />
          </div>
        </div>
        {(type === 'Set as Not Available' || !type)
          ? (
            <div>
              <div className={styles.detailContainer}>
                <h4 className={styles.subTitle}>Stay Details</h4>
                <div className={`InputFromTo ${styles.adjacentNotAvailable} ${styles.smallInput}`}>
                  <div>
                    <p className={styles.textAboveInput}>* Start date: </p>
                    <DayPickerInput
                      value={starting || undefined}
                      placeholder="From"
                      format="LL"
                      formatDate={formatDate}
                      parseDate={parseDate}
                      dayPickerProps={{
                        selectedDays: [starting, { starting, ending }],
                        disabledDays: { after: ending, before: moment().toDate() },
                        toMonth: ending,
                        modifiers,
                        numberOfMonths: 1,
                        onDayClick: () => this.endDate.getInput().focus(),
                      }}
                      onDayChange={this.handleFromChange}
                    />
                  </div>
                  <div>
                    <p className={styles.textAboveInput}>  End date: </p>
                    <span className="InputFromTo-to">
                      <DayPickerInput
                        ref={el => { this.endDate = el; }}
                        value={ending}
                        placeholder="To"
                        format="LL"
                        formatDate={formatDate}
                        parseDate={parseDate}
                        dayPickerProps={{
                          selectedDays: [starting, { starting, ending }],
                          disabledDays: { before: starting },
                          modifiers,
                          month: starting,
                          fromMonth: starting,
                          numberOfMonths: 1,
                        }}
                        onDayChange={this.handleToChange}
                      />
                    </span>
                  </div>
                </div>
              </div>
            </div>
          )
          : (
            <div>
              <div className={styles.detailContainer}>
                <h4 className={styles.subTitle}>Stay Details</h4>
                <div className={styles.contentSide}>
                  <div className={`InputFromTo ${styles.adjacent} ${styles.smallInput}`}>
                    <div>
                      <p className={styles.textAboveInput}>* Start date: </p>
                      <DayPickerInput
                        value={starting}
                        placeholder="From"
                        format="LL"
                        formatDate={formatDate}
                        parseDate={parseDate}
                        dayPickerProps={{
                          selectedDays: [starting, { starting, ending }],
                          disabledDays: { after: ending, before: moment().toDate() },
                          toMonth: ending,
                          modifiers,
                          numberOfMonths: 1,
                          onDayClick: () => this.endDate.getInput().focus(),
                        }}
                        onDayChange={this.handleFromChange}
                      />
                    </div>
                    <div>
                      <p className={styles.textAboveInput}>  End date: </p>
                      <span className="InputFromTo-to">
                        <DayPickerInput
                          ref={el => { this.endDate = el; }}
                          value={ending}
                          placeholder="To"
                          format="LL"
                          formatDate={formatDate}
                          parseDate={parseDate}
                          dayPickerProps={{
                            selectedDays: [starting, { starting, ending }],
                            disabledDays: { before: starting },
                            modifiers,
                            month: starting,
                            fromMonth: starting,
                            numberOfMonths: 1,
                          }}
                          onDayChange={this.handleToChange}
                        />
                      </span>
                    </div>
                  </div>
                  <Input
                    type="number"
                    title="Num of Guests"
                    name="numOfGuests"
                    value={numOfGuests || 0}
                    placeholder="Enter your Minimum Nights Required"
                    handleChange={this.handleInput}
                  />
                </div>
              </div>
              <div className={styles.guestDetailContainer}>
                <h4 className={styles.subTitle}>Guest Details</h4>
                <div className={styles.contentSide}>
                  <div className={styles.adjacent}>
                    <Input
                      type="text"
                      title="Name of Guest"
                      name="name"
                      value={name || ''}
                      placeholder="Enter the name of the Guest"
                      handleChange={this.handleInput}
                    />
                    <Input
                      type="tel"
                      name="phoneNumber"
                      title="Phone Number"
                      value={phoneNumber || ''}
                      handleChange={this.handleInput}
                      pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                    />
                  </div>
                  <Input
                    type="email"
                    title="Email"
                    value={email || ''}
                    name="email"
                    handleChange={this.handleInput}
                  />
                  <label htmlFor="none" className={styles.lab} style={{ marginTop: '30px' }}> Message </label>
                  <textarea
                    value={message}
                    name="intro"
                    style={{ marginBottom: '30px' }}
                    onChange={this.handleMessageText}
                  />
                </div>
              </div>

              <div className={styles.paymentContainer}>
                <h4 className={styles.subTitle}>Payment Details</h4>
                <table className={styles.paymentTable}>
                  <thead>
                    <tr>
                      <th>Service Name</th>
                      <th>Quantity</th>
                      <th>Price</th>
                      <th>Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{currentProperty.name}</td>
                      <td>{`${dates.starting ? this.getNumOfDays() : 0} night(s)`}</td>
                      <td>
                        <span>{`$${price.weekdayPrice}`}</span>
                      </td>
                      <td><span>{`$${PriceHelper(price, starting, ending)}`}</span></td>
                    </tr>
                    {selectedCharges.map(charge => (
                      <tr key={charge.value}>
                        <td>{charge.label}</td>
                        <td>
                          <Input
                            className={styles.quantityInput}
                            type="number"
                            name={charge.value}
                            value={chargesInfoMap.get(charge.value).quantity || 1}
                            placeholder="Enter quanitity of charges"
                            handleChange={this.handleQuantity}
                          />
                        </td>
                        <td>{chargesInfoMap.get(charge.value).price}</td>
                        <td><span>{`$${chargesInfoMap.get(charge.value).price * chargesInfoMap.get(charge.value).quantity}`}</span></td>
                      </tr>
                    ))}
                  </tbody>
                  <tfoot>
                    <tr className={styles.noBorder}>
                      <th><span>Add Extra Charges:</span></th>
                      <td>
                        <div>
                          {dates.starting
                            ? (
                              <MultiSelect
                                value={selectedCharges}
                                onChange={this.onChargeSelect}
                                options={extraChargeOptions}
                                isMulti
                                className={styles.selectProperties}
                              />
                            )
                            : ''}
                        </div>
                      </td>
                      <th>
                        <span>Subtotal</span>
                            :
                      </th>
                      <td>{`$${bookingInfo.price}`}</td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          )
        }
      </div>
    );
  }
}
export default AddEvent;
