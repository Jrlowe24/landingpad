import React, { Component } from 'react';
import OwnerSideNavBar from './OwnerSideNavBar/OwnerSideNavBar';


export default class OwnerDashBoard extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }


  render() {
    const { owner } = this.props;
    return (
      <div>
        <OwnerSideNavBar owner={owner} />
      </div>
    );
  }
}
