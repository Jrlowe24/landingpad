import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import CircularProgress from '@material-ui/core/CircularProgress';
import './ReservationBox.less';
import dropDown from './dropDown.png';


class ReservationBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      property: {},
      inquiry: props.inquiry,
      isLoading: true,
      infoExpanded: false
    };
    this.onDeleteClick = this.onDeleteClick.bind(this);
    this.onApproveClicked = this.onApproveClicked.bind(this);
    this.onDeclineClicked = this.onDeclineClicked.bind(this);
    this.expandInfo = this.expandInfo.bind(this);
  }

  componentDidMount() {
    // Getting Current Property
    const { inquiry } = this.props;
    fetch(`http://localhost:3000/property/${inquiry.property}`).then(response => {
      response.json().then(data => {
        this.setState({ property: data, isLoading: false });
      });
    });
  }

  // functionality for the edit button
  async onDeleteClick() {
    console.log(this.props);
  }

  onApproveClicked() {
    const { updateInquiry } = this.props;
    const { inquiry } = this.state;
    inquiry.confirmed = 'Approved';
    this.setState({ inquiry }, () => {
      updateInquiry(inquiry);
    });
  }

  onDeclineClicked() {
    const { updateInquiry } = this.props;
    const { inquiry } = this.state;
    inquiry.confirmed = 'Declined';
    console.log(inquiry);
    this.setState({ inquiry }, () => {
      updateInquiry(inquiry);
    });
    this.setState({ infoExpanded: false });
  }

  expandInfo() {
    this.setState(prevState => ({
      infoExpanded: !prevState.infoExpanded
    }));
  }

  render() {
    const {
      property, isLoading, inquiry, infoExpanded
    } = this.state;
    const {
      bookerInfo, dates, numOfGuests, price
    } = inquiry;
    if (isLoading) {
      return (
        <div className="loading">
          <CircularProgress className="loadAnim" />
        </div>
      );
    }
    const boxClassName = infoExpanded ? 'reservationBoxOpen' : 'reservationBoxClosed';
    const arrowClassName = infoExpanded ? 'expandButtonUp' : 'expandButtonDown';
    return (
      <div className={boxClassName}>
        <div className="bookingInfo">
          <div className="bookerName">
            {bookerInfo.name}
          </div>
          <div className="propertyName">
            {' at '}
            {property.name}
          </div>
          <div className="dateRange">
            {moment(dates.starting).format('MMM Do')}
            {' - '}
            {moment(dates.ending).format('MMM Do')}
            {' | '}
            {numOfGuests}
            {' Guests'}
          </div>
          <div className="price">
            {'$'}
            {price}
          </div>
          <div className="expandButtonDiv">
            <button
              className={arrowClassName}
              type="button"
              onClick={this.expandInfo}
            >
              <img className="arrowImage" src={dropDown} alt="arrow" />
            </button>
          </div>
        </div>
        <div className="contactInfoDiv">
          <span className="contactInfo">
            {`Phone: ${bookerInfo.phoneNumber}`}
          </span>
          <span className="contactInfo">
            {`Email: ${bookerInfo.email}`}
          </span>
        </div>
        <div>
          <div className="message">
            {bookerInfo.message}
          </div>
          <div className="buttons">
            <button className="approveButton" type="submit" onClick={this.onApproveClicked}> Approve</button>
            <button type="submit" onClick={this.onDeclineClicked}> Decline</button>
          </div>
        </div>
      </div>

    );
  }
}

export default ReservationBox;

ReservationBox.Proptype = {
  booking: PropTypes.object.isRequired
};
