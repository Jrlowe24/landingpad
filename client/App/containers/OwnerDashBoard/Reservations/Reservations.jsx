import React, { Component } from 'react';
import ReservationBox from './ReservationBox/ReservationBox';
import styles from './Reservations.css';


export default class Reservations extends Component { // eslint-disable-line
  constructor(props) {
    super(props);
    this.state = {
      inquiries: [],
      isLoading: true,
      pendingInquiries: [],
      acceptedInquiries: [],
      declinedInquiries: []
    };
    this.updateInquiry = this.updateInquiry.bind(this);
  }

  componentWillMount() {
    // Getting Current bookings
    const { owner } = this.props;
    fetch(`http://localhost:3000/bookings/owner/${owner._id}`).then(response => {
      response.json().then(data => {
        this.setState({ inquiries: data }, () => {
          this.updateInquiries();
        });
      });
    });
  }

  async updateInquiry(inquiry) {
    await fetch(`http://localhost:3000/booking/${inquiry._id}`, {
      method: 'PATCH',
      body: JSON.stringify(
        inquiry
      ),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(response => {
      response.json().then(data => {
        // this.setState({ property: data, loading: false });
        console.log(data);
        this.updateInquiries();
      });
    });
  }

  updateInquiries() {
    const { inquiries } = this.state;
    let {
      pendingInquiries, declinedInquiries, acceptedInquiries
    } = this.state;
    pendingInquiries = [];
    declinedInquiries = [];
    acceptedInquiries = [];
    for (let i = 0; i < inquiries.length; i += 1) {
      if (inquiries[i].confirmed === 'Pending') {
        pendingInquiries.push(inquiries[i]);
      } else if (inquiries[i].confirmed === 'Approved') {
        acceptedInquiries.push(inquiries[i]);
      } else {
        declinedInquiries.push(inquiries[i]);
      }
    }
    this.setState({
      pendingInquiries, acceptedInquiries, declinedInquiries, isLoading: false
    });
  }

  render() {
    const {
      isLoading, pendingInquiries, acceptedInquiries, declinedInquiries
    } = this.state;
    if (isLoading) {
      return <h2>Loading...</h2>;
    }
    return (
      <div className={styles.container}>
        <div className={styles.main}>
          <div className={styles.header}>
            Reservations
          </div>
          <p>Pending</p>
          {pendingInquiries.map(inquiry => (
            <ReservationBox
              key={inquiry._id}
              id={inquiry._id}
              inquiry={inquiry}
              updateInquiry={this.updateInquiry}
            />
          ))}
          <p>Accepted</p>
          {acceptedInquiries.map(inquiry => (
            <ReservationBox
              key={inquiry._id}
              id={inquiry._id}
              inquiry={inquiry}
              updateInquiry={this.updateInquiry}
            />
          ))}
          <p>Declined</p>
          {declinedInquiries.map(inquiry => (
            <ReservationBox
              key={inquiry._id}
              id={inquiry._id}
              inquiry={inquiry}
              updateInquiry={this.updateInquiry}
            />
          ))}
        </div>
      </div>
    );
  }
}
