import React, { Component } from 'react';
// import Button from '@material-ui/core/Button';
import styles from './AdBanner.css';
import logo from './LPNEW.png';

class AdBanner extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

  }

  render() {
    return (
      <div className={styles.main}>
        <span style={{ paddingLeft: '250px', verticalAlign: 'middle', lineHeight: '50px' }}> Powered By </span>
        <img src={logo} style={{ height: '100%;', width: '125px' }} alt="Logo" />
        <a href="https://www.trylandingpad.com/">
          <button className={styles.button} type="button">Build my LandingPad</button>
        </a>
      </div>


    );
  }
}

export default AdBanner;
