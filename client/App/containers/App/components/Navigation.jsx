import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import styles from './Navigation.css';

class Navigation extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {

  }

  render() {
    return (
      <div className={styles.main}>
        <ul className={styles.navBar} role="navigation">
          <li className={styles.leftButton}>
            <Button color="inherit" style={{ fontSize: '13px' }}>Our Story</Button>
          </li>
          <li>
            <Button color="inherit" className="button" style={{ fontSize: '13px' }}>Neighbors</Button>
          </li>
          <li className={styles.logo}>
            <img src="" alt="Logo" />
          </li>
          <li className={styles.button}>
            <Button color="inherit" style={{ fontSize: '13px' }}>My Suites</Button>
          </li>
          <li className={styles.buttonRight}>
            <Button color="inherit" style={{ fontSize: '13px' }}>Gallery</Button>
          </li>
          <li>
            <Button color="primary" variant="contained" style={{ fontSize: '13px' }}>Book Now</Button>
          </li>
        </ul>
        <hr className={styles.line} />

      </div>

    );
  }
}

export default Navigation;
