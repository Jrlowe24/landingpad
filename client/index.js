/**
 * Client entry point
 */

import React from 'react';
import { render } from 'react-dom';
import App from './App/App';


const firebase = require('firebase');

const firebaseConfig = {
  apiKey: 'AIzaSyBT0B75DXa0yuaS0xCH2Q8Atge6heOvblU',
  authDomain: 'appily-50cdf.firebaseapp.com',
  databaseURL: 'https://appily-50cdf.firebaseio.com',
  projectId: 'appily-50cdf',
  storageBucket: 'appily-50cdf.appspot.com',
  messagingSenderId: '830817027349',
  appId: '1:830817027349:web:8189a1b6f5589d3e'
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
if (firebaseApp != null) {
  console.log('firebase app started successfully');
} else {
  console.log('firebase app failed to start');
}

firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION);

// import { AppContainer } from 'react-hot-loader';
// import configureStore from './store';
// import Root from './Root';

// const store = configureStore();


render(
    <App/>,
    document.getElementById('root')
);
