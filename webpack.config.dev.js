const webpack = require('webpack');
const path = require('path');
// const lessToJs = require('less-vars-to-js');
// const ExternalsPlugin = require('webpack2-externals-plugin');

module.exports = {
  entry: [
    './client/index'
  ],
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.jsx?$/,
        include: [/client/, /server/],
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          configFile: '.eslintrc',
          failOnWarning: false,
          failOnError: false,
          fix: true
        }
      },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.less/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true,
              modifyVars: {
                'body-background': '#ffffff',
                'component-background': '#ffffff',
                'layout-body-background': '#17181c',
                'text-color': '#2F2F30',
                'heading-color': '#2538ab',
                'heading-color-dark': '#001529',
                'background-color-light': '#444444',
                'radio-button-bg': '#203afb',
                'radio-button-color': '#bbbbbb',
                'border-color-base': '#000000',
                'border-color-split': '#000000',
                'radio-button-hover-color': '#bbbbbb',
                'primary-color': '#faad14'
              }
            }
          }
      ]},
      { test: /\.css/, use: [{ loader: 'style-loader' }, { loader: 'css-loader', options: { modules: true } }] },
      { test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf)(\?[a-z0-9=.]+)?$/, loader: 'url-loader?limit=100000' },
      { test: /\.svg$/, loader: 'svg-inline-loader' },
        {//数据
            test: [/\.json$/i],//i不区分大小写
            exclude: /(node_modules|bower_components)/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        outputPath: './static/data/'//图片输出位置
                    }
                }
            ]
        }
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx', '.less', '.css'],
    modules: ['node_modules']
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'build/bundle.js'
  },
  devtool: 'eval-source-map',
  devServer: {
    contentBase: 'public',
    hot: true,
    historyApiFallback: true
  },
  mode: 'development',
  plugins: [
    // new Dotenv({path: './.env'}),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),

    // new ExternalsPlugin({
    //   type: 'commonjs',
    //   include: path.join(__dirname, 'node_modules')
    // })
  ],
  loader: 'css-loader',
  options: {
    modules: true
  }
};



// this makes big schedular work

// const webpack = require('webpack');
// const path = require('path');
// // const lessToJs = require('less-vars-to-js');
// // const ExternalsPlugin = require('webpack2-externals-plugin');

// module.exports = {
//   entry: [
//     './client/index'
//   ],
//   module: {
//     rules: [
//       {
//         enforce: 'pre',
//         test: /\.jsx?$/,
//         include: [/client/, /server/],
//         exclude: /node_modules/,
//         loader: 'eslint-loader',
//         options: {
//           configFile: '.eslintrc',
//           failOnWarning: false,
//           failOnError: false,
//           fix: true
//         }
//       },
//       {
//         test: /\.jsx?$/,
//         loader: 'babel-loader',
//         exclude: /node_modules/,
//       },
//       {
//         test: /\.css$/,
//         use: [
//           { loader: 'style-loader' },
//           { loader: 'css-loader' }
//       ]},
//       { test: /\.less/, use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'less-loader', options: {
//         javascriptEnabled: true,
//         modifyVars: {
//           'body-background': '#ffffff',
//           'component-background': '#ffffff',
//           'layout-body-background': '#17181c',
//           'text-color': '#2F2F30',
//           'heading-color': '#2538ab',
//           'heading-color-dark': '#001529',
//           'background-color-light': '#444444',
//           'radio-button-bg': '#203afb',
//           'radio-button-color': '#bbbbbb',
//           'border-color-base': '#000000',
//           'border-color-split': '#000000',
//           'radio-button-hover-color': '#bbbbbb',
//           'primary-color': '#faad14'
//         }} }] },
//       { test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf)(\?[a-z0-9=.]+)?$/, loader: 'url-loader?limit=100000' },
//       { test: /\.svg$/, loader: 'svg-inline-loader' },
//         {//数据
//             test: [/\.json$/i],//i不区分大小写
//             exclude: /(node_modules|bower_components)/,
//             use: [
//                 {
//                     loader: 'file-loader',
//                     options: {
//                         outputPath: './static/data/'//图片输出位置
//                     }
//                 }
//             ]
//         }
//     ],
//   },
//   resolve: {
//     extensions: ['.js', '.jsx', '.less', '.css'],
//     modules: ['node_modules']
//   },
//   output: {
//     path: path.resolve(__dirname, 'public'),
//     filename: 'build/bundle.js'
//   },
//   devtool: 'eval-source-map',
//   devServer: {
//     contentBase: 'public',
//     hot: true,
//     historyApiFallback: true
//   },
//   mode: 'development',
//   plugins: [
//     // new Dotenv({path: './.env'}),
//     new webpack.optimize.OccurrenceOrderPlugin(),
//     new webpack.HotModuleReplacementPlugin(),
//     new webpack.NoEmitOnErrorsPlugin(),

//     // new ExternalsPlugin({
//     //   type: 'commonjs',
//     //   include: path.join(__dirname, 'node_modules')
//     // })
//   ]
// };
